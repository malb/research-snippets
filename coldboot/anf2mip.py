"""
Convert and solve a Boolean Polynomial System as a MIP Problem.

This implementation follows _[BKS09].

AUTHORS: 

 - 2009-11 Martin Albrecht <M.R.Albrecht@rhul.ac.uk>: initial implementation
 - 2011-08 Martin Albrecht <M.R.Albrecht@rhul.ac.uk>: updated for 4.7.1 (+#10879)

MODIFIED: 
- To run on 4.7.1 (+#10879) 
- Added required import line
- Added MIPSolverException handling
- Changed when timeouts are set
- Works with a modified version of coldboot-experiment.py

EXAMPLE::

     sage: B.<a,b,c,d,e,f> = BooleanPolynomialRing()
     sage: I = Ideal((c*d + d + e + f + 1, b*c + b*e + b*f + e + 1, \
                      a*d + b*e + b*f + b + d*f, a*c + a*f + c + e*f + 1, \
                      a*d + b + d*f + d + 1,  a + b*d + d*e + f + 1))
     sage: I.groebner_basis()
     [a, b + f, c + f + 1, d + 1, e + 1]
     sage: a2m = BooleanPolynomialMIPConverter()
     sage: s = a2m.solve(I.gens()); s
     {f: 0, e: 1, d: 1, b: 0, c: 1, a: 0}
     sage: all(f.subs(s) == 0 for f in I.gens())
     True

REFERENCES:

.. [BKS09] Julia Borghoff , Lars R. Knudsen, Mathias Stolpe *Bivium as
  a Mixed-Integer Linear Programming Problem*; in Cryptography and
  Coding 2009; LNCS 5921; Springer Verlag 2009;

"""
import commands
from copy import copy
import re

from sage.misc.misc import prod

from sage.functions.other import ceil
from sage.misc.cachefunc import cached_method
from sage.misc.misc import get_verbose, cputime, walltime
from sage.modules.free_module import VectorSpace
from sage.numerical.mip import MixedIntegerLinearProgram
from sage.rings.all import ZZ, Integer, RR
from sage.rings.all import GF
from sage.rings.polynomial.pbori import BooleanMonomial, BooleanPolynomial
from sage.rings.real_double import RDF
from sage.structure.sage_object import SageObject
from sage.symbolic.ring import SR
from sage.all import add, random, TermOrder, BooleanPolynomialRing, sage_eval, mq

class BooleanPolynomialMIPConverter(SageObject):
    """
    Convert a Boolean polynomial system to a Mixed Integer Programm.

    EXAMPLE::

        sage: conv = BooleanPolynomialMIPConverter()
        sage: conv
        Boolean Polynomial to MIP Converter (0 variables created so far)

        sage: B.<a,b,c> = BooleanPolynomialRing()
        sage: conv.map_variable(a)
        M0000
        sage: conv
        Boolean Polynomial to MIP Converter (1 variables created so far)
    """
    is_real = -1
    is_integer = 0
    is_binary = 1

    def __init__(self):
        self._phi = {} # ANF -> MIP
        self._rho = {} # MIP -> ANF
        self._var_type = {} # the variable type of our variables (bool, real, int)
        self._var_iter = iter(SR("M%05d"%i) for i in xrange(100000))
        self.mip_variables.clear_cache()
        self.mip_instance.clear_cache()

    def _repr_(self):
        """
        EXAMPLE::

            sage: conv = BooleanPolynomialMIPConverter()
            sage: conv
            Boolean Polynomial to MIP Converter (0 variables created so far)
        """
        return "Boolean Polynomial to MIP Converter (%d variables created so far)"%(len(self._phi))

    def map_variable(self, value, vtype=is_integer):
        r"""
        Return a MIP variable for ``value`` and update the maps `\phi`
        and `\rho`.

        INPUT:

        - ``value`` - a variable in the polynomial ring or in the
          mixed integer programming ring.

        - ``vtype`` - the type (``is_real``,``is_integer``, ``is_binary``)

        EXAMPLE::

            sage: conv = BooleanPolynomialMIPConverter()
            sage: B.<a,b,c> = BooleanPolynomialRing()
            sage: conv.map_variable(a, vtype=BooleanPolynomialMIPConverter.is_real)
            M0000
        """
        if value in self._rho:
            return self._rho[value]
        if value in self._phi:
            return value
        m = self._var_iter.next()
        self._phi[m] = value
        self._rho[value] = m
        self._var_type[m] = vtype
        return m

    def get_type(self, m):
        """
        EXAMPLE::

            sage: conv = BooleanPolynomialMIPConverter()
            sage: B.<a,b,c> = BooleanPolynomialRing()
            sage: m = conv.map_variable(a, vtype=BooleanPolynomialMIPConverter.is_real)
            sage: conv.get_type(m)
            -1
        """
        return self._var_type[m]

    def get_map(self, linear_only=False):
        """
        INPUT:

        - ``linear_only`` - only return those variables which map to 
          linear boolean polynomial variables.

        EXAMPLE::

            sage: conv = BooleanPolynomialMIPConverter()
            sage: B.<a,b,c> = BooleanPolynomialRing()
            sage: m = conv.map_variable(a, vtype=BooleanPolynomialMIPConverter.is_real)
            sage: conv.get_map()
            {M0000: a}

            sage: m = conv.map_variable(a*b, vtype=BooleanPolynomialMIPConverter.is_integer)
            sage: conv.get_map()
            {M0000: a, M0001: a*b}
            sage: conv.get_map(linear_only=True)
            {M0000: a}
        """
        if linear_only:
            ret = {}
            for key,value in self._phi.iteritems():
                if isinstance(value,(BooleanMonomial,BooleanPolynomial)) and value.degree() == 1:
                    ret[key] = value
            return ret
        return copy(self._phi)

    def _linearize_and_build_map_variables(self, F, variable_type_callback=None):
        """
        Linearize the polynomial system ``F`` and build the mapping
        from boolean polynomial ring variables to MIP symbolic
        variables.

        INPUT:

        - ``F`` - a boolean polynomial system of equations

        - ``variable_type_callback`` - a callback which determines the
          type of an original variable in the boolean polynomial ring.

        EXAMPLE::

            sage: conv = BooleanPolynomialMIPConverter()
            sage: B.<a,b,c> = BooleanPolynomialRing()
            sage: conv._linearize_and_build_map_variables([a+a*b,c+1])
            [set([M0000, M0001]), set([1, M0003])]
            sage: conv.get_map()
            {M0003: c, M0000: a*b, M0001: a, M0002: b}
        """
        if variable_type_callback is None:
            variable_type_callback = lambda x: BooleanPolynomialMIPConverter.is_binary

        F = [set(f.monomials()) for f in F]
        if not F:
            return F
        M = reduce(lambda x,y: x.union(y), F)
        for m in M:
            if isinstance(m, Integer) or len(m.variables()) < 1:
                for i,f in enumerate(F):
                    # we map '1'
                    if m in f:
                        f.remove(m)
                        f.add(ZZ(1))
                        F[i] = f
                continue

            # we make sure that we have a MIP variable for each
            # variable in a monomial.
            if len(m.variables()) > 1:
                _m = self.map_variable(m, vtype=BooleanPolynomialMIPConverter.is_real)
                for v in m.variables():
                    _ = self.map_variable(v, vtype=variable_type_callback(v))
            else:
                _m = self.map_variable(m, vtype=variable_type_callback(m))

            # now we replace all m with the linearised variable
            for i,f in enumerate(F):
                if m in f:
                    f.remove(m)
                    f.add(_m)
                F[i] = f
        return F

    def _split_sums(self, F):
        """
        `a + b + c + d + e +f` becomes `a + b + c + X` and 
        `X + d + e + f`.

        INPUT:

        - ``F`` - a list of symbolic polynomials or a list of sets of
          symbolic monomials.

        EXAMPLE::

            sage: conv = BooleanPolynomialMIPConverter()
            sage: B.<a,b,c> = BooleanPolynomialRing()
            sage: Fbar = conv._linearize_and_build_map_variables([a+a*b+c+b*c+1]); sorted(Fbar[0])
            [1, M0003, M0001, M0000, M0004]
            sage: conv._split_sums(Fbar)
            [M0003 + M0005 + 1, M0000 + M0001 + M0005 + M0006, M0004 + M0006]
        """
        list_of_sums = []
        for f in F:
            try:
                f = list(f)
            except TypeError:
                f = list(f.iterator())
            c = 4

            nm = len(f)
            dj =  ceil((c-2)/ZZ(nm) * nm)

            M = []
            new_variable = []
            for j in range(0, nm, dj):
                m =  new_variable + f[j:j+dj]
                if (j + dj) < nm:
                    new_variable = [self._var_iter.next()]
                    self._var_type[new_variable[0]] = BooleanPolynomialMIPConverter.is_real
                    m += new_variable
                M.append(sum(m))
            list_of_sums.extend(M)
        return list_of_sums

    def _to_real_sc(self, F):
        """
        Convert a list of symbolic polynomials or of sets of symbolic
        monomials into a list of symbolic polynomials which hold over
        the reals using the 'standard conversion' method. That is, `x
        + y` becomes `x + y - 2xy`.

        INPUT:

        - ``F`` - a list of symbolic polynomials or a list of sets of
          symbolic monomials.

        EXAMPLE::

            sage: conv = BooleanPolynomialMIPConverter()
            sage: B.<a,b,c> = BooleanPolynomialRing()
            sage: Fbar = conv._linearize_and_build_map_variables([a+a*b+c+b*c+1]); sorted(Fbar[0])
            [1, M0003, M0001, M0000, M0004]
            sage: Fbarbar = conv._split_sums(Fbar); Fbarbar
            [M0003 + M0005 + 1, M0000 + M0001 + M0005 + M0006, M0004 + M0006]
            sage: conv._to_real_sc(Fbarbar)
            [M0003 + M0005 - 2*M0007 - 1, 
             M0000 + M0001 - M0005 - M0006 - 2*M0008 + 2*M0009, 
             M0004 + M0006 - 2*M0010]
        """
        r = []
        for f in F:
            try:
                fl = list(f)
            except TypeError:
                fl = list(f.iterator())
            if len(fl) <= 1:
                r.append( f ) # symbolic expressions with one element have an empty iterator!

            elif len(fl) == 2:
                m = self.map_variable(fl[0]*fl[1], vtype=BooleanPolynomialMIPConverter.is_binary)
                r.append( fl[0] + fl[1] - 2*m )
            
            elif len(fl) == 3:
                m = self.map_variable(fl[0]*fl[1], vtype=BooleanPolynomialMIPConverter.is_binary)
                r.append( fl[0] + fl[1] - 2*m - fl[2] )

            elif len(fl) == 4:
                m = self.map_variable(fl[0]*fl[1], vtype=BooleanPolynomialMIPConverter.is_binary)
                n = self.map_variable(fl[2]*fl[3], vtype=BooleanPolynomialMIPConverter.is_binary)
                r.append( fl[0] + fl[1] - 2*m - (fl[2] + fl[3] - 2*n) )
            else:
                raise NotImplementedError("Conversion for sums of length > 4 not implemented.")
        return r

    @staticmethod
    def _function_abstract_model(f):
        try:
            variables = f.variables()
        except AttributeError:
            f = sum(f)
            variables = f.variables()

        F = []
        for m in f:
            M = []
            for i,v in enumerate(variables):
                if v in m:
                    M.append(i)
            F.append(tuple(M))
        return tuple(F)

    def _to_real_iasc_helper(self, f):
        try:
            return self._mip_cache[BooleanPolynomialMIPConverter._function_abstract_model(f)]
        except KeyError:
            pass
        try:
            variables = f.variables()
        except AttributeError:
            f = sum(f)
            variables = f.variables()

        fs = SR(str(f))
        R = fs.parent()
        S = set()

        if f.degree() == 1:
            S = range(len(list(f))+1)
            if f.constant_coefficient() == 1:
                S = [e+1 for e in S]
            S = [e for e in S if e%2 == 0]
            S = set(S)
        else:
            from sage.rings.polynomial.pbori import BooleSet
            BS = BooleSet(prod([x for i,x in enumerate(variables) if v[i]]) for v in VectorSpace(GF(2),len(variables)))
            for v in f.zeros_in(BS):
                s = {}
                for _v in variables:
                    if _v in v:
                        s[R(str(_v))] = ZZ.one_element()
                    else:
                        s[R(str(_v))] = ZZ.zero_element()
                S.add( fs.subs(s).pyobject() )

        m = self._var_iter.next()
        self._var_type[m] = BooleanPolynomialMIPConverter.is_integer
        assert(all(s%2 == 0 for s in S))
        S = [s/2 for s in S]
        self._mip_cache[BooleanPolynomialMIPConverter._function_abstract_model(f)] = S
        return S

    def _to_real_iasc(self, F):
        """
        Convert a list of symbolic polynomials or of sets of symbolic
        monomials into a list of symbolic polynomials which hold over
        the reals using the 'integer adapted standard conversion'
        method. For example, `x + y` becomes `x + y - 2xy`.

        INPUT:

        - ``F`` - a list of symbolic polynomials or a list of sets of
          symbolic monomials.

        OUTPUT:

            ``F``, ``C`` where is a list of symbolic polynomials and
            ``C`` a list of constraints on newly introduced variables.

        EXAMPLE::

            sage: conv = BooleanPolynomialMIPConverter()
            sage: B.<a,b,c> = BooleanPolynomialRing()
            sage: Fbar = conv._linearize_and_build_map_variables([a+b+c]); sorted(Fbar[0])
            [M0000, M0001, M0002]
            sage: conv._to_real_iasc(Fbar)
            ([M0000 + M0001 + M0002 - 2*M0003], [(M0003, [0, 1])])
        """
        Fy, Fc = [], []
        self._mip_cache = {}
        for f in F:
            fs = SR(str(f))
            S = self._to_real_iasc_helper(f)

            m = self._var_iter.next()
            self._var_type[m] = BooleanPolynomialMIPConverter.is_integer
            Fy.append(fs - 2*m)
            Fc.append((m, S))
        return Fy, Fc

    def standard_conversion(self,  F, objective_set=None, 
                            variable_type_callback=None, modular_additions=None):
        """
        Convert the polynomial system ``F`` to a Mixed Integer Program
        using the 'standard conversion' method.

        INPUT:

        - ``F`` - a boolean polynomial system or a set of boolean
          polynomials
        - ``objective_set`` - a list of tuples boolean polynomial variables
          whichs sum should be minimised.
        - ``variable_type_callback`` - a callback which determines the
          type of an original variable in the boolean polynomial ring.

        EXAMPLE:: 

            sage: conv = BooleanPolynomialMIPConverter()
            sage: B.<a,b,c> = BooleanPolynomialRing()
            sage: p = conv.standard_conversion([a+b+1,a*c+ c + 1]); p
            Mixed Integer Program  ( minimization, 10 variables, 24 constraints )
            sage: p.solve() # optional (requires GLPK or Coin)
            3.0
            sage: va = conv.get_map(linear_only=True); va
            {M0003: b, M0002: c, M0001: a}
            sage: va = list(va.iteritems()); va
            [(M0003, b), (M0002, c), (M0001, a)]
            sage: V = conv.get_typed_mip_variable
            sage: va[0][1], p.get_values(V(va[0][0]))
            (b, 1.0)
            sage: va[1][1], p.get_values(V(va[1][0]))
            (c, 1.0)
            sage: va[2][1], p.get_values(V(va[2][0]))
            (a, 0.0)
        """
        self.__init__()
        # 1. we linearize the system and build maps that map
        # polynomial variables and MIP linear variables.
        F = self._linearize_and_build_map_variables(F, variable_type_callback=variable_type_callback)
        if get_verbose() >= 1: 
            print "After Linearization:"
            for f in F:
                print " ",f
            print
            for k,v in self._phi.iteritems():
                print "%10s => %s"%(k,v)
            print
        # 2. we split all sums into sums of length 4.
        F = self._split_sums(F)
        if get_verbose() >= 1:
            print "After Splitting:"
            for f in F:
                print " ",f
            print
            for k,v in self._phi.iteritems():
                print "%10s => %s"%(k,v)
            print
        # 3. we adjust XOR to additions over the integers
        F = self._to_real_sc(F)
        if get_verbose() >= 1:
            print "After Conversion:"
            for f in F:
                print " ",f
            print
        # 4. we convert to a MIP porgramm
        p = self._mixed_integer_program(F, 
                                        objective_set=objective_set,
                                        modular_additions=modular_additions)
        return p

    def integer_adapted_standard_conversion(self,  F, objective_set=None, 
                                            variable_type_callback=None,
                                            modular_additions = None):
        """
        Convert the polynomial system ``F`` to a Mixed Integer Program
        using the 'standard conversion' method.

        INPUT:

        - ``F`` - a boolean polynomial system or a set of boolean
          polynomials
        - ``objective_set`` - a list of tuples boolean polynomial variables
          whichs sum should be minimised.
        - ``variable_type_callback`` - a callback which determines the
          type of an original variable in the boolean polynomial ring.

        EXAMPLE:: 

            sage: conv = BooleanPolynomialMIPConverter()
            sage: B.<a,b,c> = BooleanPolynomialRing()
            sage: p = conv.integer_adapted_standard_conversion([a+b+1,a*c+ c + 1]); p
            Mixed Integer Program  ( minimization, 6 variables, 8 constraints )
            sage: p.solve() # optional (requires GLPK or Coin)
            2.0
            sage: va = conv.get_map(linear_only=True); va
            {M0003: a, M0004: c, M0005: b}
            sage: va = list(va.iteritems()); va
            [(M0003, a), (M0004, c), (M0005, b)]
            sage: V = conv.get_typed_mip_variable
            sage: va[0][1], p.get_values(V(va[0][0]))
            (a, 0.0)
            sage: va[1][1], p.get_values(V(va[1][0]))
            (c, 1.0)
            sage: va[2][1], p.get_values(V(va[2][0]))
            (b, 1.0)
        """
        self.__init__()
        _, C = self._to_real_iasc(F)
        F = self._linearize_and_build_map_variables(F, variable_type_callback=variable_type_callback)
        for i in range(len(F)):
            if len(C[i][1]) > 1:
                F[i] = sum(F[i]) - 2*C[i][0]
            else:
                F[i] = sum(F[i]) - 2*C[i][1][0]
            
        # 4. we convert to a MIP porgramm
        p = self._mixed_integer_program(F, objective_set=objective_set, modular_additions=modular_additions)
        V = self.get_typed_mip_variable
        for (m,c) in C:
            if min(c) != max(c):
                p.set_max(V(m),max(c))
                p.set_min(V(m),min(c))

        return p

    @cached_method
    def mip_instance(self):
        return MixedIntegerLinearProgram_nonfree(maximization=False)

    @cached_method
    def mip_variables(self):
        p = self.mip_instance()
        pVr = p.new_variable(real=True)
        pVi = p.new_variable(integer=True)
        pVb = p.new_variable(binary=True)
        return pVr,pVi,pVb

    def get_typed_mip_variable(self, x):
        pVr,pVi,pVb = self.mip_variables()
        try:
            if self._var_type[x] == BooleanPolynomialMIPConverter.is_real:
                return pVr[x]
            elif self._var_type[x] == BooleanPolynomialMIPConverter.is_integer:
                return pVi[x]
            elif self._var_type[x] == BooleanPolynomialMIPConverter.is_binary:
                return pVb[x]
            else:
                raise TypeError("Variable '%s' has unknown type ('%s')"%(x,self._var_type[x]))
        except KeyError:
            raise ValueError("Variable '%s' unknown"%(x,))

    def _mixed_integer_program(self, F, objective_set=None, limits=None, modular_additions=None):
        """
        Create a :class:`MixedIntegerProgram` instance from the linear
        system ``F``.

        This method is stateful insofar as it adds constraints a
        monomials in `\phi`.

        INPUT:

        - ``F`` - a boolean polynomial system
        """
        p = self.mip_instance()
        pV = self.get_typed_mip_variable
        
        for i,f in enumerate(F):
            if not f:
                continue
            variables = f.variables()
            f = sum([f.coefficient(m) * pV(m) for m in variables]) + f(dict(zip(variables,[0 for _ in range(len(variables))])))
            if limits:
                p.add_constraint(f, min=limits[i][0], max=limits[i][1])
            else:
                p.add_constraint(f, min=0, max=0)

        if modular_additions is None: modular_additions = []
        for A,B,C in modular_additions:
            N = len(A)
            assert(len(B) == len(C) == len(A))
            for n in range(1, N+1):
                a,b,c = 0,0,0
                for i in range(n):
                    try:
                        a += 2**i * int(A[i])
                    except TypeError:
                        a += 2**i * pV(self.map_variable(A[i], vtype=self.is_binary))

                    try:
                        b += 2**i * int(B[i])
                    except TypeError:
                        b += 2**i * pV(self.map_variable(B[i], vtype=self.is_binary))

                    try:
                        c += 2**i * int(C[i])
                    except TypeError:
                        c += 2**i * pV(self.map_variable(C[i], vtype=self.is_binary))
                m = self._var_iter.next()
                self._var_type[m] = BooleanPolynomialMIPConverter.is_binary

                p.add_constraint(a+b - c - 2**n * pV(m), min=0, max=0)
                p.set_min(pV(m), 0)
                p.set_max(pV(m), 1)

        def gamma(x):
            try:
                return pV(self._rho[x])
            except KeyError:
                return pV(x)

        for k,m in self._phi.iteritems():
            mv = m.variables()
            if len(mv) > 1:
                for v in mv: # y <= x_i
                    p.add_constraint(pV(k) - gamma(v), max=0, min=-1)
                # x_i + x_j - 1 <= y
                p.add_constraint(sum(gamma(w) for w in mv) - pV(k), min=0,max=len(mv)-1)

            if self.get_type(k) in (BooleanPolynomialMIPConverter.is_real,BooleanPolynomialMIPConverter.is_binary):
                p.set_max(pV(k), 1)
                p.set_min(pV(k), 0)
                #p.add_constraint(pV(k), min=0,max=1)

        if objective_set is not None:
            if set(objective_set) == set([0]):
                p.set_objective(0)
            else:
                obj = 0
                for c,m in objective_set:
                    obj += c*gamma(m)
                p.set_objective(obj)
        else:
            p.set_objective(sum(map(gamma,self._rho)))
        return p

    def solve(self, F, objective_set=None, conversion="IASC", 
              solver=None, variable_type_callback=None, timeout=None,
              modular_additions=None):
        """
        Solve the boolean polynomial system ``F`` using Mixed Integer Programming.

        INPUT:
        
        - ``F`` - an iterable of boolean polynomials
        - ``objective_set`` - a list of variables whichs sum ought to
          be minimized (default: ``None``)
        - ``conversion`` - either "SC" or "IASC" (default: "SC")
        - ``solver`` - whatever Sage supports (default: ``None``, i.e. default)
        - ``variable_type_callback`` - override default variable types (default: None)


        EXAMPLE::


            sage: conv = BooleanPolynomialMIPConverter()
            sage: sr = mq.SR(1,1,1,4,gf2=True,polybori=True)
            sage: F,s = sr.polynomial_system()
            sage: s2 = conv.solve(F) # optional
            sage: all([f.subs(s2) == 0 for f in F])
            True
        """
        GF2 = GF(2)
        if conversion == "SC":
            p = self.standard_conversion(F, 
                                         objective_set=objective_set,
                                         variable_type_callback=variable_type_callback,
                                         modular_additions=modular_additions)
        elif conversion == "IASC":
            p = self.integer_adapted_standard_conversion(F, 
                                                         objective_set=objective_set,
                                                         variable_type_callback=variable_type_callback,
                                                         modular_additions=modular_additions)
        else:
            raise ValueError("Conversion '%s' unknown."%(conversion,))
        if get_verbose() >= 1:
            p.show()

        ct, wt = cputime(),walltime()
        S = p.solve(solver=solver, timeout=timeout)

        ct,wt = cputime(ct),walltime(wt)
        V = self.get_typed_mip_variable
        if get_verbose() >= 0:
            print "CPU Time: %.2f  Wall time: %.2f, Obj: %5.2f"%(ct,wt,S[0][1])

        S2 = []
        for s,obj in S:
            s2 = {}
            for var in sorted(self._rho):
                if len(var.variables()) == 1 and not str(var).startswith("M"):
                    try:
                        s2[var]  = GF2(RR(s[V(self._rho[var])]).round())
                    except KeyError:
                        pass
            S2.append( (s2, obj) )
        return S2
                
from sage.rings.polynomial.multi_polynomial_sequence import PolynomialSequence_gf2

class ProbabilisticPolynomialSequence(PolynomialSequence_gf2):
    def __init__(self, ring, hard, soft):
        PolynomialSequence_gf2.__init__(self, [tuple(hard), tuple(soft)], ring)
        self._hard = hard
        self._soft = soft

    def nsoft(self):
        return len(self._soft)

    def soft_generators(self):
        return tuple(self._soft)

    def nhard(self):
        return len(self._hard)

    def hard_generators(self):
        return tuple(self._hard)

    def _gen_mip(self, enforce_boolean=True, weight_callback=None):
        if weight_callback is None:
            weight_callback = lambda f: 1.0

        a2m = BooleanPolynomialMIPConverter()
        ne = self.nsoft()
        R = self.ring()
        order = R.term_order()
        if order != 'lex' and ne:
            order = TermOrder(order.blocks[0].name(),R.ngens()) + TermOrder(order.blocks[0].name(),ne)
        B = BooleanPolynomialRing(R.ngens() + self.nsoft(), R.variable_names() + tuple("indicator%04d"%i for i in range(ne)), order=order)
        e = B.gens()[R.ngens():]
        gens = list(map(lambda f: B(str(f)),self.hard_generators()))
        objective_set = []

        for i,f in enumerate(self.soft_generators()):
            gens.append(B(str(f)) + e[i])
            objective_set.append( (weight_callback(f), e[i]) )

        if enforce_boolean is not True:
            Kvar = [B(str(x)) for x in enforce_boolean] + list(e)
            def variable_type_callback(x):
                if x in Kvar:
                    return BooleanPolynomialMIPConverter.is_binary
                else:
                    return BooleanPolynomialMIPConverter.is_real
        else:
            variable_type_callback = None


        return a2m, objective_set, gens, variable_type_callback, B

    def solve_mip(self, conversion='IASC', solver=None, enforce_boolean=True, weight_callback=None, timeout=None):
        a2m, obj_set, gens, variable_type_callback, R2 = self._gen_mip(enforce_boolean=enforce_boolean,
                                                                       weight_callback=weight_callback)


        R = self.ring()
        S = a2m.solve(gens,
                      variable_type_callback=variable_type_callback,
                      solver=solver,
                      conversion=conversion,
                      objective_set=obj_set,
                      timeout=timeout)

        e  = set([y for (x,y) in obj_set])
        ec = 0

        S2 = []

        for s,obj in S:
            s2 = {}
            ec = sum(int(s[_e]) for _e in e)

            for k in self.variables():
                s2[k] = s[R2(str(k))]
            S2.append((s2, obj))
        return S2

    def solve_scip(self, timeout=None, weight_callback=None, **kwds):
        from sage.numerical.mip import MIPSolverException # added dpm
        from sage.rings.polynomial.multi_polynomial_sequence import Sequence # added dpm
        from sage.libs.scip.scip import SCIP
        scip = SCIP(maximization=False, name='anf2mip', parameters=kwds.get('parameters',None))
        
        ne = self.nsoft()
        R = self.ring()
        order = R.term_order()
        if order != 'lex' and ne:
            order = TermOrder(order.blocks[0].name(),R.ngens()) + TermOrder(order.blocks[0].name(),ne)
        B = BooleanPolynomialRing(R.ngens() + self.nsoft(), 
                                  R.variable_names() + tuple("indicator%04d"%i for i in range(ne)), 
                                  order=order)
        e = B.gens()[R.ngens():]
        t = cputime()
        gens = list(sage_eval(str(self.hard_generators()), B.gens_dict()))

        soft_gens = list(sage_eval(str(self.soft_generators()), B.gens_dict()))

        WD = {}

        for i,f in enumerate(soft_gens):
            gens.append(f + e[i])
            if weight_callback:
                WD[e[i]] = weight_callback(f)
            else:
                WD[e[i]] = 1.0
                
        wcb = lambda x: WD.get(x,0.0)

        gens = Sequence(gens)
        Q1 = scip.read_polynomial_system_mod2(gens, objective_weight_callback=wcb, use_xor=False)

#        if timeout:  % removed dpm (didn't set timeout)
        if timeout!=0.0: # added dpm
            scip['limits/time'] = timeout
        try: # added dpm
            _ = scip.solve()
        except (MIPSolverException): # Exception for SCIP: No Solution Found
            # print('SCIP: No Solution Found (counted as incorrect key)')
            return None		# Reports as Incorrect Solution			
			
        S = scip.get_all_solutions()

        if S is None:
            return None

        gd = R.gens_dict()
        S2 = []
        GF2 = GF(2)
        for s,obj in S:
            s2 = {}
            for k,v in s.iteritems():
                try:
                    s2[gd[str(Q1.up(k))]] = GF2(round(v))
                except KeyError:
                    pass
            S2.append((s2, obj))
        return S2


    def write_mip(self, filename, conversion='IASC', 
                  enforce_boolean=True, filetype='mps', weight_callback=None,
                  modular_additions=None):
        a2m, e, gens, variable_type_callback, R2 = self._gen_mip(enforce_boolean=enforce_boolean,
                                                                 weight_callback=weight_callback)
        if conversion == "SC":
            p = a2m.standard_conversion(gens, 
                                        objective_set=e,
                                        variable_type_callback=variable_type_callback,
                                        modular_additions=modular_additions)
        elif conversion == "IASC":
            p = a2m.integer_adapted_standard_conversion(gens, 
                                                        objective_set=e,
                                                        variable_type_callback=variable_type_callback,
                                                        modular_additions=modular_additions)
        else:
            raise ValueError(conversion)

        if filetype == 'mps':
            p.write_mps(filename)
        elif filetype == 'lp':
            p.write_lp(filename)
        else:
            raise ValueError("Filetype '%s' unknown."%filetype)
