r"""
PRESENT polynomial system generator and algebraic attacks.

AUTHOR: Martin Albrecht <M.R.Albrecht@rhul.ac.uk>

See \code{present_dc} for attack code for

  Martin Albrecht and Carlos Cid; 'Algebraic Techniques in
  Differential Cryptanalysis'; \url{http://eprint.iacr.org/2008/177}

"""

import commands, re

from sage.misc.misc import alarm, walltime

from sage.rings.all import FiniteField as GF
from sage.rings.integer_ring import ZZ

from sage.rings.polynomial.polynomial_ring_constructor import PolynomialRing
from sage.rings.polynomial.polynomial_ring_constructor import BooleanPolynomialRing_constructor as BooleanPolynomialRing
from sage.rings.polynomial.term_order import TermOrder
from sage.crypto.mq.sbox import SBox
from sage.crypto.mq.mpolynomialsystemgenerator import MPolynomialSystemGenerator
from sage.rings.polynomial.multi_polynomial_sequence import PolynomialSequence
from sage.structure.sequence import Sequence

from sage.combinat.permutation import Permutation

from multiprocessing import Process, Queue
from Queue import Empty

from polybori.ll import ll_encode
try:
    from polybori.ll import ll_red_nf
except ImportError:
    from polybori.ll import ll_red_nf_redsb as ll_red_nf

def whoami():
    """
    Print some helpful information about which revisions and date.

    EXAMPLE:
        sage: execfile('present.py') # output random
        ...
        sage: whoami() # output random
        r17 M | road | 2009-02-01 17:18
    """
    import commands, time
    revision = 0
    for line in commands.getoutput("hg tip").splitlines():
        if "changeset" in line:
            revision =  int(line.split(":")[1])
            break

    try:
        status = commands.getoutput("hg status %s"%__file__)[0]
    except (IndexError, NameError):
        status = ""
    hostname = commands.getoutput("uname -n")
    date = time.localtime()
    date = "%04d-%02d-%02d %02d:%02d"%(date[0],date[1],date[2],date[3],date[4])

    print "r%d"%revision, status, "|", hostname, "|", date

whoami()

class PRESENT(MPolynomialSystemGenerator):
    def __init__(self, Ks=80, Nr=1, B=16, **kwds):
        """
        PRESENT is an ultra-lightweight block cipher proposed at CHES
        2007 by A. Bogdanov, L.R. Knudsen , G. Leander , C. Paar,
        A. Poschmann, M.J.B. Robshaw, Y. Seurin, and C. Vikkelsoe

        INPUT:
            B -- blocksize divided by 4 (default: 16)
            Nr -- number of rounds (default: 1)
            Ks -- keysize in (80,128) (default: 80)
            postfix -- (default: '')
            order -- term ordering for the polynomial ring (default: 'degrevlex')
            polybori -- use PolyBoRi as underlying implementation (default: True)

        EXAMPLE:
            sage: execfile('present.py') # output random
            ...
            sage: PRESENT(80,31)
            PRESENT-80-31

            sage: PRESENT(80,31, polybori=False)
            PRESENT-80-31

        REFERENCES:
            \url{http://www.crypto.rub.de/imperia/md/content/texte/publications/conferences/present_ches2007.pdf}
        """
        if Ks not in (80,128):
            raise ValueError("Number of key bits must be either 80 or 128.")

        if Nr < 1:
            raise ValueError("Number of rounds must be >= 1.")

        self.Ks = Ks
        self.Nr = Nr

        self.B = B
        self.Bs = B*4
        self._postfix = kwds.get("postfix","")
        self._order = kwds.get("order","degrevlex")
        self._polybori = kwds.get("polybori",True)
        self._sbox_representation = kwds.get("sbox_representation",2)

        self.s = 4
        self._base = GF(2)
        self._sbox = SBox([12,5,6,11,9,0,10,13,3,14,15,8,4,7,1,2], big_endian=True)

    def new_generator(self, **kwds):
        """
        Return a new instance of the PRESENT polynomial system
        generator.

        INPUT:
            see constructor

        EXAMPLE:
            sage: execfile('present.py') #output random
            ...
            sage: p = PRESENT(80,4)
            sage: R = p.ring()
            sage: R.term_order()
            Degree reverse lexicographic term order
            sage: p = p.new_generator(order='lex')
            sage: R = p.ring()
            sage: R.term_order()
            Lexicographic term order
        """
        Nr = kwds.get("Nr",self.Nr)
        B = kwds.get("B",self.B)
        Ks = kwds.get("Ks",self.Ks)
        
        postfix = kwds.get("postfix",self._postfix)
        order = kwds.get("order", self._order)
        polybori = kwds.get("polybori",self._polybori)
        sbox_representation = kwds.get("sbox_representation",self._sbox_representation)
        
        return PRESENT(Ks=Ks, Nr=Nr, B=B, 
                       postfix=postfix, 
                       order=order, 
                       polybori=polybori,
                       sbox_representation=sbox_representation)
        
    def varformatstr(self, name):
        r"""
        Return format string for variable named \code{name}.

        INPUT:
            name -- a variable name

        EXAMPLE:
            sage: execfile('present.py') #output random
            ...
            sage: p = PRESENT(80,4,postfix='foobar')
            sage: p.varformatstr('K')
            'K%02d%02d'

        Note the interaction with postfix='foobar'

            sage: p.varformatstr('X')
            'Xfoobar%02d%02d'
        """
        l = str(max([len(str(self.Nr)), len(str(self.Bs -1))]))
        if not name.startswith("K"):
            name += self._postfix
        return name + "%0" + l + "d" + "%0" + l + "d"

    def varstrs(self, name, nr):
        r"""
        Return variable strings for variables named \code{name} and
        round \code{nr}.

        INPUT:
            sage: execfile('present.py') #output random
            ...
            sage: p = PRESENT(80,4,postfix='foobar')
            sage: p.varstrs('K',0)
            ('K0000',
             'K0001',
             'K0002',
             'K0003',
            ...
             'K0079')
            sage: p.varstrs('X',1)
            ('Xfoobar0100', 'Xfoobar0101', 'Xfoobar0102', 'Xfoobar0103', ..., 'Xfoobar0163')
        """
        s = self.varformatstr(name)
        if name == "K":
            if nr == 0:
                return tuple([s%(nr,i) for i in range(self.Ks) ])
            if self.Ks == 80:
                return tuple([s%(nr,i) for i in range(self.s) ])
            else:
                return tuple([s%(nr,i) for i in range(2*self.s) ])
        else:
            return tuple([s%(nr,i) for i in range(self.Bs) ])

    def vars(self, name, nr):
        r"""
        Return variables for variables named \code{name} and round
        \code{nr}.

        INPUT:
            sage: execfile('present.py') #output random
            ...
            sage: p = PRESENT(80,4,postfix='foobar')
            sage: p.vars('K',0)
            (K0000, K0001, K0002, K0003, ..., K0079)
            sage: p.vars('X',1)
            (Xfoobar0100, Xfoobar0101, Xfoobar0102, Xfoobar0103, ..., Xfoobar0163)
        """
        
        gd = self.variable_dict()
        return tuple([gd[e] for e in self.varstrs(name,nr)])

    def variable_dict(self):
        """
        Return the dictionary of variables for this PRESENT instance.


        sage: execfile('present.py') #output random
        ...
        sage: p = PRESENT(80,1)
        sage: for k,v in sorted(p.variable_dict().iteritems()):
        ...     k,v
        ('K0000', K0000)
        ('K0001', K0001)
        ('K0002', K0002)
        ('K0003', K0003)
        ('K0004', K0004)
        ('K0005', K0005)
        ('K0006', K0006)
        ...
        ('Y0157', Y0157)
        ('Y0158', Y0158)
        ('Y0159', Y0159)
        ('Y0160', Y0160)
        ('Y0161', Y0161)
        ('Y0162', Y0162)
        ('Y0163', Y0163)
        """
        try:
            R,gd = self._variable_dict
            if R is self.R:
                return gd
            else:
                self._variable_dict = self.R, self.R.gens_dict()
            return gd
        except AttributeError:
            gd = self.R.gens_dict()
            self._variable_dict = self.R,gd
            return gd

    def __repr__(self):
        """
        EXAMPLE:
            sage: execfile('present.py') #output random
            ...
            sage: PRESENT(80,4)
            PRESENT-80-4
        """
        return "PRESENT-%d-%d"%(self.Ks,self.Nr)

    def ring(self, order=None, n=1, reverse_variables=False, boomerang=False, sample_major=True, names=None):
        """
        Return a new ring for this PRESENT equation system generator.

        INPUT:
            n -- number of plaintext ciphertext pairs (default: 1)
            reverse_variables -- arrange variables in reverse order (default: False)

        EXAMPLE:
            sage: execfile('present.py') #output random
            ...
            sage: p = PRESENT(80,2); p
            PRESENT-80-2
            sage: p.ring()
            Boolean PolynomialRing in K0000, K0001, K0002, ..., K0077, K0078, K0079,
            K0100, K0101, K0102, K0103, 
            Y0100, Y0101, ..., Y0162, Y0163, X0100, X0101, ..., X0162, X0163, 
            K0200, K0201, K0202, K0203,
            Y0200, Y0201, ..., Y0262, Y0263, X0200, X0201, ..., X0262, X0263

            sage: p.ring(reverse_variables=True)
            Boolean PolynomialRing in K0200, K0201, K0202, K0203, 
            Y0200, Y0201, ..., Y0262, Y0263, X0200, X0201, ..., X0262, X0263, 
            K0100, K0101, K0102, K0103, 
            Y0100, Y0101, ..., Y0162, Y0163, X0100, X0101, ..., X0162, X0163, 
            K0000, K0001, K0002, ..., K0077, K0078, K0079

            sage: p.ring(n=2)
            Boolean PolynomialRing in K0000, K0001, ..., K0078, K0079, 
            K0100, K0101, K0102, K0103, 
            Y00100, Y00101, ..., Y00162, Y00163, X00100, X00101, ..., X00162, X00163, 
            Y10100, Y10101, ..., Y10162, Y10163, X10100, X10101, ..., X10162, X10163, 
            K0200, K0201, K0202, K0203, 
            Y00200, ...
            
        """
        if order is None:
            order = self._order
        if reverse_variables == False:
            f = lambda x: x
        else:
            f = lambda x: reversed(x)
        Bs = self.Bs
        Ks = self.Ks
        s = self.s

        if Ks == 80:
            ks = s
        elif Ks == 128:
            ks = 2*s

        if n==1:
            
            var_names = []
            if not reverse_variables:
                var_names += [self.varformatstr("K")%(0,j) for j in xrange(Ks)]
            for nr in f(range(1,self.Nr+1)):
                var_names += [self.varformatstr("K")%(nr,j) for j in xrange(ks)]
                var_names += [self.varformatstr("X")%(nr,j) for j in xrange(Bs)]
                var_names += [self.varformatstr("Y")%(nr,j) for j in xrange(Bs)]
            if reverse_variables:
                var_names += [self.varformatstr("K")%(0,j) for j in xrange(Ks)]
            
        elif n > 1:

            if boomerang:
                size = n/2
            else:
                size = n

            if names is None:
                names = map(str,range(size))

            var_names = []
            if not reverse_variables:
                var_names += [self.varformatstr("K")%(0,j) for j in xrange(Ks)]

            for nr in f(range(1,self.Nr+1)):
                var_names += [self.varformatstr("K")%(nr,j) for j in xrange(ks)]
                if sample_major:
                    for sample in names[:size]:
                       var_names += [self.varformatstr("X%s"%sample)%(nr,j) for j in xrange(Bs)]
                    for sample in names[:size]:
                       var_names += [self.varformatstr("Y%s"%sample)%(nr,j) for j in xrange(Bs)]
                else:
                    for j in xrange(Bs):
                        var_names += [self.varformatstr("X%s"%sample)%(nr,j) for sample in names[:size]]
                    for j in xrange(Bs):
                        var_names += [self.varformatstr("Y%s"%sample)%(nr,j) for sample in names[:size]]

            if reverse_variables:
                var_names += [self.varformatstr("K")%(0,j) for j in xrange(Ks)]

            if boomerang:
                for nr in reversed(range(1,self.Nr+1)):
                    for sample in names[size:]:
                        var_names += [self.varformatstr("X%s"%sample)%(nr,j) for j in xrange(Bs)]
                        var_names += [self.varformatstr("Y%s"%sample)%(nr,j) for j in xrange(Bs)]
        else:
            raise TypeError, "parameter n must be >= 1"

        if self._polybori:
            R = BooleanPolynomialRing(len(var_names), var_names, order= order)
        else:
            R = PolynomialRing(self._base, len(var_names), var_names, order=order)

        if n == 1:
            self.R = R

        return R
    
    def random_element(self, length=None):
        r"""
        Return a random list of elements in $GF(2)$ of the given length

        INPUT:
            length -- length (default: \code{self.Bs})

        EXAMPLE:
            sage: execfile('present.py') # output random
            ... 
            sage: p = PRESENT(Nr=1)
            sage: p.random_element()
            [0, 1, 0, 0, 1, 0, 1, ..., 0, 1, 1, 0, 1, 0, 0, 0, 0]
        """
        if length is None:
            return [self._base.random_element() for _ in range(self.Bs)]
        else:
            return [self._base.random_element() for _ in range(length)]

    def __getattr__(self, attr):
        """
        Some syntactical sugar.

        EXAMPLE:
            sage: execfile('present.py') # output random
            ... 
            sage: p = PRESENT(Nr=1)
            sage: p.K0 # variables K0
            (K0000, K0001, K0002, K0003, K0004, ..., K0076, K0077, K0078, K0079)

            sage: p.R # ring
            Boolean PolynomialRing in K0000, K0001, K0002, K0003, ..., X0159, X0160, X0161, X0162, X0163
        """
        import re
        if attr == "R":
            self.R = self.ring()
            return self.R

        s = re.match("([XYK])([0-9]+)",attr)
        if s:
            v,n = s.groups()
            return self.vars(v,int(n))

        raise AttributeError, "'%s' object has no attribute '%s'"%(type(self),attr)
        
    def polynomial_system(self, P=None, K=None, C=None,**kwds):
        """
        Return a polynomial system for self and P,K

        INPUT:
            P -- plaintext (default: random)
            K -- key (default: random)
            C -- ciphertext (default ``self(P,K)``)

        OUTPUT:
            a tuple with ``PolynomialSequence`` and a dictionary of solutions

        EXAMPLE:
            sage: execfile('present.py') # output random
            ... 
            sage: p = PRESENT(Nr=1)
            sage: F,s = p.polynomial_system()
            sage: F
            Polynomial System with 502 Polynomials in 212 Variables
            sage: sorted(s.iteritems())
            [(K0079, 1), (K0078, 1), (K0077, 1), ..., (K0003, 1), (K0002, 0), (K0001, 1), (K0000, 1)]
        """

        if C and not K:
            no_solution = True
        else:
            no_solution = False

        if not P:
            P = self.random_element()
        if not K and not C:
            K = self.random_element(self.Ks)

        if not C:
            Kval = list(K)
            C = self(P, Kval)

        Kvar = list(self.K0)

        if C and not K:
            no_solution = True

        R = self.R

        Zi = P
        rounds = [self.field_polynomials(self.vars("K",0))]

        for i in range(1,self.Nr+1):
            Xi = self.vars("X",i)
            Yi = self.vars("Y",i)

            # key schedule
            Ki, Kvar, key = self.key_schedule_polynomials(Kvar,i)
            key += self.field_polynomials(self.vars("K",i))

            # key addition
            add = self.key_addition_polynomials(Xi, Zi, Ki)
            add += self.field_polynomials(Xi)

            # sbox
            sbox = self.sbox_polynomials(Xi, Yi)
            sbox += self.field_polynomials(Yi)

            # diffusion layer
            Zi = self.pLayer(Yi)

            rounds.append(sbox + add + key)

        # key addition equations
        Ki,Kvar, key = self.key_schedule_polynomials(Kvar, self.Nr+1)
        add = self.key_addition_polynomials(C, Zi, Ki)

        rounds.append(add + key)

        if no_solution:
            solution = None
        else:
            solution = dict(zip(self.K0,Kval))

        F = PolynomialSequence(rounds,R)
        return F, solution

    def sbox_polynomials(self, X, Y):
        """
        Return a list of polynomials describing the sbox/substitution
        layer for the input variables X and the output variables Y.

        INPUT:
            X -- list of input variables of length self.Bs
            Y -- list of output variables of length self.Bs
        """
        s = self.s
        return sum([ self.single_sbox_polynomials(X[j:j+s], Y[j:j+s])
                     for j in range(0,self.Bs,s)],[])

    def single_sbox_polynomials(self, x, y, *args):
        """
        Return a list of polynomials describing one sbox for the
        input variables x and output variables y.

        INPUT:
            x -- list of length 4 with add-able elements
            y -- list of length 4 with add-able elements
            poly_choice -- in (1,2,3), choice which representation to use (default: 2)
        """
        x0,x1,x2,x3 = x
        y0,y1,y2,y3 = y
        
        poly_choice = self._sbox_representation

        if poly_choice == 0:
            l = [
                y3 + x0 + x1*x2 + x1 + x3,
                y1 + y2*x1 + y2*x2 + y3*x2 + y3 + x0*x3 + x2 + x3 + 1,
                y1*x0 + y2*x0 + y2*x1 + y2 + y3*x0 + y3 + x1 + x2 + x3,
                y0 + y1 + y3*x2 + y3*x3 + y3 + x0 + x1*x3 + x2 + x3,
                y0 + y1*x0 + y2*y3 + y3*x2 + y3 + x0 + x1 + x2 + 1,
                y0*x2 + y0 + y1*y3 + y2*x1 + y3*x3 + y3 + x1 + x2 + 1,
                y0*x1 + y0 + y2*x2 + y3*x0 + y3*x3 + y3 + x0 + x3 + 1,
                y0*x1 + y0 + y1*x0 + y1*x1 + y2 + y3*x0 + y3*x1 + x0 + x3 + 1,
                y0*x1 + y0 + y1*x0 + y1*x1 + y1*x3 + y1 + y2*x0 + y2*x2 + y3 + x1 + x3,
                y0*x0 + y1*x0 + y1*x1 + y1 + y2*x0 + y2*x2 + y3*x0 + y3*x2 + x0 + x1 + x2 + 1,
                y0*x0 + y0 + y1 + y2*x0 + y2*x3 + y3*x0 + y3 + x0 + x2,
                y0*x0 + y0 + y1*x1 + y1*x2 + y1 + y2*x0 + x0 + x1 + x3,
                y0*x0 + y0*x3 + y1 + y2*x0 + y3*x0 + x0 + x1 + 1,
                y0*x0 + y0*x2 + y0 + y1*x0 + y1 + y3*x0 + x0 + x1 + x2 + x3,
                y0*y3 + y2*y3 + y2 + y3*x3 + y3 + x0*x1 + x0 + x2,
                y0*y3 + y1 + y2*y3 + y2*x3 + y3*x3 + y3 + x0 + x1 + 1,
                y0*y3 + y1*x3 + y2*x1 + y2*x2 + y2 + x0 + x1 + x3,
                y0*y3 + y1*x1 + y1 + y2*x2 + y3*x3 + y3 + x1 + x2 + 1,
                y0*y3 + y1*y3 + y1 + y2*x0 + y2*x1 + y3*x2 + x3 + 1,
                y0*y3 + y1*y2 + y2 + y3*x3 + x0 + x1 + x3,
                y0*y3 + y0 + y2*y3 + y3*x3 + x0*x2 + x1 + x2 + 1,
                y0*y3 + y0 + y2*y3 + y3*x2 + y3*x3 + x1 + x2*x3 + x2 + 1,
                y0*y3 + y0 + y1 + y2*y3 + y2 + y3*x1 + y3*x2 + y3 + x0,
                y0*y3 + y0 + y1*y3 + y1 + y2*y3 + y2 + y3*x0 + x0,
                y0*y3 + y0 + y1*y3 + y1*x2 + y1 + y2*x2 + y2 + y3 + x1 + x3,
                y0*y3 + y0*x3 + y0 + y1 + y2*y3 + y3*x3 + x0 + x2,
                y0*y3 + y0*x1 + y1*y3 + y1 + y2*y3 + y2*x2 + y2 + y3*x3 + y3 + x3 + 1,
                y0*y3 + y0*x0 + y2*x1 + y2 + y3*x2 + y3*x3 + x0 + x1 + x2 + x3,
                y0*y2 + y1 + y3 + x3 + 1,
                y0*y1 + y2*y3 + y3*x3 + y3 + x0 + x2 + x3 + 1
                ]
        elif poly_choice == 1 or poly_choice == 'lex':
            l = [
                y3 + x0 + x1*x2 + x1 + x3,
                y2 + x0*x1*x3 + x0*x1 + x0*x2*x3 + x0*x2 + x0 + x1*x2*x3 + x2,
                y1 + x0*x1*x3 + x0*x2*x3 + x0*x2 + x0*x3 + x0 + x1 + x2*x3 + 1,
                y0 + x0*x1*x3 + x0*x2*x3 + x0 + x1*x2*x3 + x1*x2 + x2 + x3 + 1
                ]
        elif poly_choice == 2:
            l = [
                y2*x3 + y3*x3 + x1*x3 + x2*x3 + x3,
                y0*x3 + y3*x3 + x1*x3 + x2*x3 + y0 + y3 + x1 + x2 + x3 + 1,
                x1*x2 + y3 + x0 + x1 + x3,
                x0*x2 + y3*x3 + x1*x3 + x2*x3 + y0 + y1 + y3 + x0 + x2 + x3,
                y3*x2 + y3*x3 + x1*x3 + y0 + y1 + y3 + x0 + x2 + x3,
                y0*x2 + y1*x2 + y1*x3 + y3*x3 + y1 + x0 + x1 + x2 + 1,
                x0*x1 + y3*x3 + x1*x3 + x2*x3 + y1 + y2 + x1 + x2 + x3 + 1,
                y3*x1 + y3*x3 + x2*x3 + y1 + y2 + y3 + x0 + x1 + x2 + 1,
                y2*x1 + y2*x2 + y3*x3 + x0*x3 + x1*x3 + y0 + x0 + 1,
                y1*x1 + y2*x2 + y1*x3 + x0*x3 + x1*x3 + y0 + y1 + y2 + y3 + x2 + x3,
                y0*x1 + y1*x2 + y1*x3 + x0*x3 + x2*x3 + y1 + y2 + y3 + x0 + x1 + 1,
                y3*x0 + y1*x2 + y2*x2 + y1*x3 + y3*x3 + x0*x3 + x2*x3 + y0 + y1 + y2 + x1 + x3,
                y2*x0 + y1*x2 + x0*x3 + y0 + y1 + y2 + x1 + x2 + x3,
                y1*x0 + y1*x3 + x0*x3 + x1*x3 + x2*x3 + y0 + y2 + y3 + x0 + x1 + x3 + 1,
                y0*x0 + y2*x2 + y1*x3 + x1*x3 + y0 + y1 + y3 + x0 + x3,
                y2*y3 + y1*x3 + y3*x3 + x0*x3 + x2*x3 + y0 + y1 + y2 + y3 + x0,
                y1*y3 + y1*x2 + y2*x2 + y1*x3 + y3*x3 + x0*x3 + x1*x3 + y1 + y3 + 1,
                y0*y3 + y1*x3 + y3*x3 + x0*x3 + x1*x3 + y0 + y2 + x1 + x3 + 1,
                y1*y2 + y1*x3 + x0*x3 + x1*x3 + y0 + x0 + 1,
                y0*y2 + y1 + y3 + x3 + 1,
                y0*y1 + y1*x3 + x0*x3 + x2*x3 + y0 + y1 + y2 + x2 + x3 + 1,
                y1*x2*x3 + y1*x2 + y3*x3 + x0*x3 + x1*x3 + x2*x3 + y3 + x0 + x1 + x2
                ]
        elif poly_choice == 3:
            l = [x0*x1*x2*x3 + x0*x1*x2*y0*y2 + x0*x1*x2*y1 + x0*x1*x2*y3 + x0*x1*x3*y0*y1*y3 + x0*x1*x3*y0*y1 + x0*x1*x3*y0*y2*y3 + x0*x1*x3*y0*y2 + \
                 x0*x1*x3*y0 + x0*x1*x3*y1*y2*y3 + x0*x1*x3*y1*y3 + x0*x1*x3*y2 + x0*x1*x3*y3 + x0*x1*x3 + x0*x1*y0*y1*y3 + x0*x1*y0*y3 + x0*x1*y1*y2 + \
                 x0*x1*y1*y3 + x0*x1*y1 + x0*x1*y2*y3 + x0*x2*x3*y0*y1*y3 + x0*x2*x3*y0*y2*y3 + x0*x2*x3*y0*y2 + x0*x2*x3*y0*y3 + x0*x2*x3*y0 + \
                 x0*x2*x3*y1*y2*y3 + x0*x2*x3*y1*y2 + x0*x2*x3*y1*y3 + x0*x2*x3*y2*y3 + x0*x2*y0*y1*y3 + x0*x2*y0*y1 + x0*x2*y0*y2*y3 + x0*x2*y1*y2 + \
                 x0*x2*y2*y3 + x0*x3*y0*y1 + x0*x3*y0*y2*y3 + x0*x3*y1*y3 + x0*x3*y2*y3 + x0*y0*y1*y2 + x0*y0*y1*y3 + x0*y0*y1 + x0*y0*y2*y3 + x0*y1*y2*y3 + \
                 x0*y2*y3 + x1*x2*x3*y0*y1*y3 + x1*x2*x3*y0 + x1*x2*x3*y1*y2*y3 + x1*x2*x3*y1 + x1*x2*x3*y2*y3 + x1*x2*x3*y2 + x1*x2*x3*y3 + x1*x2*x3 + \
                 x1*x2*y0*y1*y2 + x1*x2*y0*y1 + x1*x2*y0*y2 + x1*x2*y0*y3 + x1*x2*y1*y2*y3 + x1*x2*y1*y2 + x1*x3*y0*y2 + x1*x3*y0 + x1*x3*y1*y2 + x1*x3*y1 + \
                 x1*x3*y2*y3 + x1*x3*y2 + x1*x3*y3 + x1*x3 + x1*y0*y1*y2 + x1*y0*y1 + x1*y0*y2*y3 + x1*y0*y3 + x2*x3*y0*y1 + x2*x3*y0*y2*y3 + x2*x3*y1*y2 + \
                 x2*x3*y1*y3 + x2*y0*y1*y3 + x2*y0*y1 + x2*y1*y2*y3 + x2*y1*y2 + x3*y0*y1*y2 + x3*y0*y1 + x3*y1*y2*y3 + x3*y1*y3 + y0*y1*y2*y3 + y0*y1*y2 + y0*y1*y3 + y0*y1 + 1]

        else:
            raise TypeError, "poly_choice parameter not understood"
        
        return l

    def key_addition_polynomials(self, X, Z, K):
        """
        Return key addition polynomials.

          X = Z + K

        INPUT:
            X -- output variables
            Z -- input state variables
            K -- key variables
        """
        return [ self.R(X[j]) + self.R(Z[j]) + self.R(K[j]) for j in xrange(self.Bs)]

    def key_schedule_polynomials(self,K,i):
        """
        Return a triple ($K_i$, $K$, $eqns$) with $K_i$ the round keys
        for round $i$, $K$ the updated key register of length self.Ks
        and $eqns$ a list of polynomials describing relationships
        between $K_i$ and $K_{i-1}$.

        INPUT:
            K -- key variable register
            i -- round counter
        
        """
        Bs = self.Bs
        Ks = self.Ks
        s = self.s
        
        Ki = K[:Bs]

        K = K[61:Ks] + K[0:61]

        if i == self.Nr+1:    # we don't need to update the register
            return Ki, K, []  # in the last step

        x = tuple(K[:s])
        y = self.vars("K",i)[:s]
        eqns = self.single_sbox_polynomials(x,y)
        K[:s] = y

        if Ks == 128:
            x = K[s:2*s]
            y = self.vars("K",i)[s:2*s]
            eqns.extend( self.single_sbox_polynomials(x,y) )
            K[s:2*s] = y

        rc = self.round_counter(i)

        if Ks == 80:
            K[Ks-1-19] += rc[0]
            K[Ks-1-18] += rc[1]
            K[Ks-1-17] += rc[2]
            K[Ks-1-16] += rc[3]
            K[Ks-1-15] += rc[4]
        else: # 128
            K[Ks-1-66] += rc[0]
            K[Ks-1-65] += rc[1]
            K[Ks-1-64] += rc[2]
            K[Ks-1-63] += rc[3]
            K[Ks-1-62] += rc[4]
        
        return Ki, K, eqns


    def field_polynomials(self, X):
        """
        Return list $x_i^2 + x_i$ for all $x_i$ in X.

        INPUT:
            X -- list
        """
        if self._polybori:
            return []
        else:
            return [x**2 + x for x in X]

    def sbox(self):
        """
        Return SBox object.
        """
        return self._sbox

    def __call__(self, P, K):
        """
        Encrypt plaintext P with key K.

        INPUT:
            P -- plaintext
            K -- key

        EXAMPLES:
            sage: execfile('present.py') # output random
            ...
            sage: p = PRESENT(80,31)
            sage: P = [0 for _ in xrange(p.Bs)]
            sage: K = [0 for _ in xrange(p.Ks)]
            sage: set_verbose(2)
            sage: C = p(P,K)
            01 00000000 00000000 Key: 00000000 00000000 Reg: 00000000 00000000 0000
            02 ffffffff 00000000 Key: c0000000 00000000 Reg: c0000000 00000000 8000
            03 80ff00ff ff008000 Key: 50001800 00000001 Reg: 50001800 00000001 0000
            04 4036c837 b7c88c09 Key: 60000a00 03000001 Reg: 60000a00 03000001 8000
            05 73c2cd26 b6192359 Key: b0000c00 01400062 Reg: b0000c00 01400062 0000
            06 41d7be58 531e4446 Key: 90001600 0180002a Reg: 90001600 0180002a 800c
            07 182ef861 ad62fd1c Key: 00019200 02c00033 Reg: 00019200 02c00033 0005
            08 0ea0a5b6 7effc5a4 Key: a000a000 3240005b Reg: a000a000 3240005b 8006
            09 bba0b848 a113e080 Key: d000d400 1400064c Reg: d000d400 1400064c 000b
            10 fa943423 a9142338 Key: 30017a00 1a800284 Reg: 30017a00 1a800284 80c9
            11 69f2e22d 63684d54 Key: e0192600 2f400355 Reg: e0192600 2f400355 0050
            12 548a4b63 c330a59d Key: f00a1c03 24c005ed Reg: f00a1c03 24c005ed 806a
            13 d75f955f a228e4ca Key: 800d5e01 4380649e Reg: 800d5e01 4380649e 00bd
            14 44255864 103841f9 Key: 4017b001 abc02876 Reg: 4017b001 abc02876 8c93
            15 e2cc9004 363f6c12 Key: 71926802 f600357f Reg: 71926802 f600357f 050e
            16 c36682c5 cd375421 Key: 10a1ce32 4d005ec7 Reg: 10a1ce32 4d005ec7 86af
            17 597db55c c2a5d9b6 Key: 20d5e214 39c649a8 Reg: 20d5e214 39c649a8 0bd8
            18 e67ce40e 71b8b713 Key: c17b041a bc428730 Reg: c17b041a bc428730 4935
            19 751df6d6 807b5b59 Key: c926b82f 60835781 Reg: c926b82f 60835781 50e6
            20 b948414e 23332c93 Key: 6a1cd924 d705ec19 Reg: 6a1cd924 d705ec19 eaf0
            21 5b75890d cfb3d563 Key: bd5e0d43 9b249aea Reg: bd5e0d43 9b249aea bd83
            22 56792031 68278f5a Key: 07b077ab c1a8736e Reg: 07b077ab c1a8736e 135d
            23 17c377c4 13fa45a3 Key: 426ba0f6 0ef5783e Reg: 426ba0f6 0ef5783e 0e6d
            24 262a2de7 3b5f3ecd Key: 41cda84d 741ec1d5 Reg: 41cda84d 741ec1d5 2f07
            25 d3a05312 8b4d7bb3 Key: f5e0e839 b509ae8f Reg: f5e0e839 b509ae8f d83a
            26 7db29209 c28a20fa Key: 2b075ebc 1d0736ad Reg: 2b075ebc 1d0736ad b5d1
            27 62050c99 40f400b9 Key: 86ba2560 ebd783ad Reg: 86ba2560 ebd783ad e6d5
            28 65d50da2 1fbcc09f Key: 8cdab0d7 44ac1d77 Reg: 8cdab0d7 44ac1d77 7075
            29 6a50663c 540d862f Key: 1e0eb19b 561ae89b Reg: 1e0eb19b 561ae89b 83ae
            30 c79b8ff0 0a48df35 Key: d075c3c1 d6336acd Reg: d075c3c1 d6336acd dd13
            31 4a38c5e0 0283fba1 Key: 8ba27a0e b8783ac9 Reg: 8ba27a0e b8783ac9 6d59
            sage: bitstringtohexstring(C) # official test vector
            '5579c138 7b228445'

            sage: set_verbose(0)
            sage: P = [0 for _ in xrange(p.Bs)]
            sage: K = [1 for _ in xrange(p.Ks)]
            sage: C = p(P,K)
            sage: bitstringtohexstring(C) # official test vector
            'e72c46c0 f5945049'

            sage: P = [1 for _ in xrange(p.Bs)]
            sage: K = [0 for _ in xrange(p.Ks)]
            sage: C = p(P,K)
            sage: bitstringtohexstring(C) # official test vector
            'a112ffc7 2f68417b'

            sage: P = [1 for _ in xrange(p.Bs)]
            sage: K = [1 for _ in xrange(p.Ks)]
            sage: C = p(P,K)
            sage: bitstringtohexstring(C) # official test vector
            '3333dcd3 213210d2'
        """
        Zi = [ self._base(e) for e in P ]

        for i in range(1,self.Nr+1):
            if get_verbose() > 1:
                reg = bitstringtohexstring(K)
            Ki,K = self.keySchedule(K, i)
            if get_verbose() > 1:
                print "%02d"%i, bitstringtohexstring(Zi), "Key:", bitstringtohexstring(Ki), "Reg:", reg

            Xi = self.addRoundKey( Zi, Ki )
            Yi = self.sBoxLayer( Xi )
            Zi = self.pLayer(Yi)
            
        Ki,K = self.keySchedule(K, self.Nr+1)
        Xi = self.addRoundKey(Zi, Ki)
        
        return Xi

    def decrypt(self, C, K):
        """
        Encrypt plaintext P with key K.

        INPUT:
            C -- ciphertext
            K -- key
        """
        Zi = [ self._base(e) for e in C ]

        KK = []

        for i in range(1,self.Nr+1):
            k,K = self.keySchedule(K, i)
            KK.append(k)
        k,K = self.keySchedule(K, self.Nr+1)
        KK.append(k)
        
        for i in reversed(range(1,self.Nr+1)):
            Xi = self.addRoundKey( Zi, KK[i] )
            Yi = self.pLayer_inverse(Xi)
            Zi = self.sBoxLayer_inverse( Yi )
        Xi = self.addRoundKey(Zi, KK[0])
        
        return Xi

    def solution(self, P, K):
        """
        Return a dictionary where the keys are variables and values
        correspond to the solution for P and K.

        The dictionary contains a key,value pair for every variable
        that is used to define the equation system.

        INPUT:
            P -- plaintext
            K -- key

        EXAMPLE:
            sage: execfile('present.py') # output random
            ...
            sage: p = PRESENT(80,2)
            sage: P = p.random_element()
            sage: K = p.random_element(80)
            sage: F,s = p.polynomial_system(P,K)
            sage: len(s) # key variables
            80
            sage: s  = p.solution(P,K)
            sage: len(s) # all variables
            344
            sage: F.ring().ngens()
            344
            sage: F = F.subs(s)
            sage: F.groebner_basis()
            []
        """
        solution = zip(self.vars("K",0),K)
        k = list(self.vars("K",0))
        Zi = [ self._base(e) for e in P ]
        for i in range(1,self.Nr+1):

            Ki,K = self.keySchedule(K, i)
            ki,k,_ = self.key_schedule_polynomials(k,i)
            for j in range(len(Ki)):
                if not ki[j].is_homogeneous():
                    continue
                if (ki[j],Ki[j]) not in solution:
                    solution.append((ki[j],Ki[j]))

            Xi = self.addRoundKey( Zi, Ki )
            solution.extend(zip(self.vars("X",i), Xi))

            Yi = self.sBoxLayer( Xi )
            solution.extend(zip(self.vars("Y",i), Yi))

            Zi = self.pLayer(Yi)
            
        Ki,K = self.keySchedule(K, self.Nr+1)
        ki,k,_ = self.key_schedule_polynomials(k,self.Nr+1)
        for j in range(len(Ki)):
            if not ki[j].is_homogeneous():
                continue
            if (ki[j],Ki[j]) not in solution:
                solution.append((ki[j],Ki[j]))

        Xi = self.addRoundKey(Zi, Ki)
        
        return dict(solution)


    def round_counter(self, i):
        """
        INPUT:
            i -- integer

        EXAMPLE:
            sage: execfile('present.py') # output random
            ... 
            sage: p = PRESENT(80,31)
            sage: for i in xrange(1,31+1):
            ...     p.round_counter(i)
            ...
            [0, 0, 0, 0, 1]
            [0, 0, 0, 1, 0]
            [0, 0, 0, 1, 1]
            [0, 0, 1, 0, 0]
            [0, 0, 1, 0, 1]
            [0, 0, 1, 1, 0]
            [0, 0, 1, 1, 1]
            [0, 1, 0, 0, 0]
            [0, 1, 0, 0, 1]
            [0, 1, 0, 1, 0]
            [0, 1, 0, 1, 1]
            [0, 1, 1, 0, 0]
            [0, 1, 1, 0, 1]
            [0, 1, 1, 1, 0]
            [0, 1, 1, 1, 1]
            [1, 0, 0, 0, 0]
            [1, 0, 0, 0, 1]
            [1, 0, 0, 1, 0]
            [1, 0, 0, 1, 1]
            [1, 0, 1, 0, 0]
            [1, 0, 1, 0, 1]
            [1, 0, 1, 1, 0]
            [1, 0, 1, 1, 1]
            [1, 1, 0, 0, 0]
            [1, 1, 0, 0, 1]
            [1, 1, 0, 1, 0]
            [1, 1, 0, 1, 1]
            [1, 1, 1, 0, 0]
            [1, 1, 1, 0, 1]
            [1, 1, 1, 1, 0]
            [1, 1, 1, 1, 1]
        """
        rc = list(reversed(ZZ(i).digits(base=2)))
        if len(rc) < 5:
            rc = [0]*(5-len(rc)) + rc

        rc = map(self._base, rc[-5:])
        return rc

    def keySchedule(self, K, i):
        """
        INPUT:
            K -- key register of size self.Ks
            i -- round counter
        """
        S = self.sbox()
        Bs = self.Bs
        Ki = [0]*Bs

        # extract key
        for j in xrange(Bs):
            Ki[j] = K[j]

        if self.Ks == 80:
            # update register
            K = K[61:80] + K[0:61]
            K[0:4] = S(K[0:4])

            # add round counter
            rc = self.round_counter(i)

            K[80-1-19] += rc[0]
            K[80-1-18] += rc[1]
            K[80-1-17] += rc[2]
            K[80-1-16] += rc[3]
            K[80-1-15] += rc[4]
            return Ki, K

        elif self.Ks == 128:
            # update register
            K = K[61:128] + K[0:61]
            K[0:4] = S(K[0:4])
            K[4:8] = S(K[4:8])

            # add round constant
            rc = self.round_counter(i)

            K[128-1-66] += rc[0]
            K[128-1-65] += rc[1]
            K[128-1-64] += rc[2]
            K[128-1-63] += rc[3]
            K[128-1-62] += rc[4]
            return Ki, K
            
    def addRoundKey(self, X, Y):
        """
        Return list of pairwise sums of elements in X and Y.

        INPUT:
            X -- list
            Y -- list
        """
        return [ X[j] + Y[j] for j in range(self.Bs) ]

    def sBoxLayer(self, X):
        """
        Apply S-boxes to X
        """
        s = self.s
        sbox = self.sbox()
        return sum([ sbox(X[j:j+s]) for j in range(0,self.Bs,s) ],[])

    def sBoxLayer_inverse(self, X):
        """
        Apply inverse of S-boxes to X
        """
        s = self.s
        sbox = self.sbox()
        inv_sbox = [0 for _ in range(2**s)]
        for i in range(2**s):
            inv_sbox[sbox[i]] = i
        inv_sbox = mq.SBox(inv_sbox)
        return sum([ inv_sbox(X[j:j+s]) for j in range(0,self.Bs,s) ],[])

    def pLayer(self, Y):
        """
        Return a list of length self.Bs with linear combinations of
        the elements in the list y (of length self.Bs) matching the
        permutation layer of PRESENT.

        INPUT:
            Y -- list of length self.Bs
        """
        B = self.B
        s = self.s
        Z = [0]*B*s
        for i in xrange(B):
            for j in xrange(s):
                Z[B*j + i] = Y[s*i + j] 
                
        return Z

    def pLayer_inverse(self, Y):
        """
        Return a list of length self.Bs with linear combinations of
        the elements in the list y (of length self.Bs) matching the
        inverse of the permutation layer of PRESENT.

        INPUT:
            Y -- list of length self.Bs
        """
        B = self.B
        s = self.s
        Z = [0]*B*s
        for i in xrange(B):
            for j in xrange(s):
                Z[s*i + j] = Y[B*j + i] 
                
        return Z

    def pLayer_as_permutation(self):
        """
        """
        return Permutation( self.pLayer(range(1,65)) )

def bitstringtohexstring(l):
    """
    Return a hex string in PRESENT style for l a list of bits.

    INPUT:
        l -- a list with bit entries of length divisible by 4
    """
    r = []
    for i in xrange(0,len(l),4):
        z = list(reversed(map(int, l[i:i+4])))
        r.append(hex(ZZ(z,2)))

    r = sum([r[i:i+8]+[" "] for i in xrange(0,len(r),8) ],[])

    return "".join(r)[:-1]

def hexstringtobitstring(n, length=64):
    """
    Return a hex string in PRESENT style for l a list of bits.

    INPUT:
        l -- a list with bit entries of length divisible by 4
    """
    n = int(n,16)
    l = []
    for i in xrange(length):
        l.append(1 if 2**i & n else 0)
    l = map(GF(2),l)
    return list(reversed(l))


#########################################################
#
#                    Attack Code
#
#########################################################

def differential(self, **kwds):
    """
    Return a bitstring for a given differential in Wang's notation
    
    EXAMPLE:
        sage: execfile('present.py') # output random
        ...
        sage: p = PRESENT(80,2)
        sage: differential(p, x0=1)
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
         0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
         0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1]
            """
    D = map(GF(2),[0,0,0,0]*self.B)
    S = self.sbox()
    for idx, diff in kwds.iteritems():
        idx = self.B-1 - int(idx[1:]) #x1 => 1
        diff = S.to_bits(diff)
        for i in xrange(4):
            D[idx*4+i] = diff[i]
    return D

def get_differences(S, diff):
    r"""
    Return (i1,d1,i2,d2) for the S-Box S and the difference diff if
    diff has the non zero value d1 at index i1 and difference d2 at
    index i2.
    """
    i1 = 0
    d1 = 0
    i2 = 0
    d2 = 0
    for i in reversed(range(0,len(diff),4)):
        d = S.from_bits( diff[i:i+4] )
        if d!=0:
            i1,d1 = 16-1-i/4,d
            break

    for i in range(i-4,0,-4):
        d = S.from_bits( diff[i:i+4] )
        if d!=0:
            i2,d2 = 16-1-i/4,d
            break

    return i1,d1,i2,d2

def check_differential(self, D):
    """
    INPUT:
        p -- PRESENT instance
        D -- difference iterator

    EXAMPLE:
        sage: execfile('present.py') # output random
        ...
        sage: p = PRESENT(Nr=31)
        sage: check_differential(p, Wang6DiffIter(p))
        1 4
        2 8
        3 12
        4 16
        5 22
        6 26
        7 30
        8 34
        9 40
        10 44
        11 48
        12 52
        13 58
        14 62
    """
    r = 1
    p = 1
    S = self.sbox()
    M = S.difference_distribution_matrix()

    it = iter(D)

    while True:
        if r == 15:
            break
        idiff = it.next()
        if r!=1 and idiff != self.pLayer(odiff):
            errstr =  str(get_differences(S,idiff)) + " " + str(get_differences(S,self.pLayer(odiff)))
            raise ValueError, errstr
        
        _,id1,_,id2 = get_differences(S, idiff)

        odiff = it.next()
        _,od1,_,od2 = get_differences(S, odiff)

        p *= M[id1,od1]/16 * M[id2,od2]/16
        print r, len(p.denominator().digits(base=2))-1
        r += 1

def present_dc(self, **kwds):
    """
    Run Attacks A, B, C against PRESENT.

    INPUT:
        timeout -- time in seconds to wait for a contradition
                   (default: 200)
        r -- number of rounds to approximate (default: 1)
        K -- key to use (default: random)
        kwds -- passed on to polynomial system generator
        algorithm -- one from ('polybori', 'singular', 'magma', 'sat')
                     (default: 'polybori')
        characteristic -- use characteristic rather than differential
                          (default: False)
        pre_filter -- use Wang's statistical filter as pre-filter
                       (default: False)
        last_round -- can be None, 'symbolic' or False
        return_system -- can be None, 'success' or True

    To run Attack-A try:
        sage: execfile('present.py') # output random
        ... 
        sage: p = PRESENT(80, 4) # PRESENT-80-1
        sage: present_dc(p, r=2, timeout=0) # not tested

    To run Attack-B try:
        sage: present_dc(PRESENT(80,3),r=1,timeout=20,characteristic=True) # not tested
          0 3.681 4.075
          1 3.613 3.921
         ...
         25 3.626 4.019
         26 3.594 3.907

        candidate found

        (((K0054 + K0055 + 1, K0053 + K0055 + 1), (1, 1, 1, 0)),
         ((K0006 + K0007, K0005 + K0007), (0, 1, 1, 1)))

    To run Attack-C try:
        sage: present_dc(PRESENT(80,3),r=1,timeout=20) # not tested
        0 2.524 2.979
        1 2.557 2.879
        2 2.716 3.092
        3 2.743 3.056
        
        candidate found
        
        (((K0054 + K0055 + 1, K0053 + K0055), (1, 1, 0, 1)),
         ((K0006 + K0007 + 1, K0005 + K0007), (1, 0, 1, 0)))

    SEE ALSO:
        Martin Albrecht and Carlos Cid; 'Algebraic Techniques in
        Differential Cryptanalysis'; \url{http://eprint.iacr.org/2008/177}
    """
    self = copy(self)
    
    timeout = kwds.get("timeout", 200)
    r = kwds.get("r", 1)
    K = kwds.get("K", self.random_element(self.Ks))

    alg = kwds.get("algorithm","polybori")
    characteristic = kwds.get("characteristic", False)
    kwds["K"] = K
    pre_filter = kwds.get("pre_filter", False)
    pre_r = kwds.get("pre_r",0)
    
    diff_iter = Wang6DiffIter

    return_system = kwds.get("return_system", False)
    
    last_round = kwds.get("last_round", True)
    leave_out_last_round = False
    symbolic_last_round = False

    if last_round == False:
        leave_out_last_round = True
    elif last_round == 'symbolic':
        symbolic_last_round = True

    counter = -1

    T = []

    while True:
        counter += 1
        wt = walltime()

        R = self.ring(n=2)
        if symbolic_last_round:
            variable_names = list(R.variable_names()) \
                + ["C000%02d"%i for i in range(self.Bs)] \
                + ["C100%02d"%i for i in range(self.Bs)]
            term_order = R.term_order()
            if "," in term_order.name():
                term_order = term_order.name() + ",degrevlex(128)"
            else:
                term_order = term_order.name() + "(%d)"%(len(variable_names)-128)+ ","+term_order.name()+"(128)"
                
            R = BooleanPolynomialRing(len(variable_names), variable_names, order=term_order)
            Ca = [R("C000%02d"%i) for i in range(self.Bs)]
            Cb = [R("C100%02d"%i) for i in range(self.Bs)]
        # request new plaintexts
        it = diff_iter(self)
        P0 = kwds.get("P0", self.random_element())
        DP = it.next()
        if kwds.get('randomize', False):
            P1 = self.random_element()
        else:
            P1 = map(GF(2), [P0[i] + DP[i] for i in xrange(len(P0))])

        l = []

        # generate polynomial systems
        gen = self.new_generator(postfix=str(0))
        gen.R = R
        C0 = gen(P0, K)
        F,s = gen.polynomial_system(P=P0, **kwds)
        for i in xrange(F.nparts()):
            if characteristic or i > r - pre_r:
                l.append(F.part(i))

        if symbolic_last_round:
            lr = l[-1]
            lr = [lr[i] - lr[i].constant_coefficient() + Ca[i] for i in range(self.Bs)]
            l = l[:-1] + [lr]
        if "C0" in kwds:
            lr = l[-1]
            lr = [lr[i] - lr[i].constant_coefficient() + kwds["C0"][i] for i in range(self.Bs)]
            l = l[:-1] + [lr]

        if leave_out_last_round:
            l = l[:-1]

        gen = self.new_generator(postfix=str(1))
        gen.R = R
        C1 = gen(P1, K)
        F,s = gen.polynomial_system(P=P1, **kwds)
        for i in xrange(F.nparts()):
            if characteristic or i > r - pre_r:
                l.append(F.part(i))

        if symbolic_last_round:
            lr = l[-1]
            lr = [lr[i] - lr[i].constant_coefficient() + Cb[i] for i in range(self.Bs)]
            l = l[:-1] + [lr]
        elif "C1" in kwds:
            lr = l[-1]
            lr = [lr[i] - lr[i].constant_coefficient() + kwds["C1"][i] for i in range(self.Bs)]
            l = l[:-1] + [lr]

        if leave_out_last_round:
            l = l[:-1]

        # test filter function
        if pre_filter:
            if (self.Nr,r) in [(4,2),(8,6),(12,10),(16,14)]:

                C0 = gen(P0, K)
                C1 = gen(P1, K)
                DC = [C0[i] + C1[i] % 2 for i in xrange(len(C0))]

                if filter_wang(self, DC):
                    pass
                else:
                    if return_system:
                        return PolynomialSequence([R(1)])
                    print "% 4d pre-filtered"%counter
                    continue
            elif (self.Nr,r) in [(5,2),(9,6),(13,10),(17,14)]:
                C0 = gen(P0, K)
                C1 = gen(P1, K)
                DC = [C0[i] + C1[i] % 2 for i in xrange(len(C0))]

                if filter_3r(self, DC):
                    pass
                else:
                    if return_system:
                        return PolynomialSequence([R(1)])
                    print "% 4d pre-filtered"%counter
                    continue

        it = diff_iter(self)

        gd = R.gens_dict()
        
        for i in xrange(min(r+1,self.Nr)):
            DX = it.next()
            X0 = self.varformatstr("X0")
            X1 = self.varformatstr("X1")

            if characteristic or i >= r - pre_r:
                l.append([gd[X0%(i+1,b)] + gd[X1%(i+1,b)] + DX[b] for b in range(self.Bs)])

            DY = it.next()
            if i == 0: 
                D1 = DY
            
            Y0 = self.varformatstr("Y0")
            Y1 = self.varformatstr("Y1")

            if characteristic and i<r:
                l.append([gd[Y0%(i+1,b)] + gd[Y1%(i+1,b)] + DY[b] for b in range(self.Bs)])


        if r in (2,6,10,14):
            if self.Nr >= r+1:
                mask = differential(self, x0=14,x8=14)
                filtr = [gd[Y0%(r+1,b)] + gd[Y1%(r+1,b)] + mask[b] for b in range(self.Bs) if mask[b] == 0]
                l.append(filtr)

            if self.Nr >= r+2:
                filtr= [gd[X0%(r+2,b)] + gd[X1%(r+2,b)] for b in range(self.Bs) if b not in (7,15,23,31,39,47)]
                filtr.append((gd[X0%(r+2,23)] + gd[X1%(r+2,23)] + 1)*(gd[X0%(r+2, 7)] + gd[X0%(r+2,39)] + gd[X1%(r+2, 7)] + gd[X1%(r+2,39)] + 1))
                filtr.append((gd[X0%(r+2,31)] + gd[X1%(r+2,31)] + 1)*(gd[X0%(r+2,15)] + gd[X0%(r+2,47)] + gd[X1%(r+2,15)] + gd[X1%(r+2,47)] + 1))

                mask = differential(self, x4=15, x6=15, x8=15, x10=15, x12=15, x14=15)
                filtr.extend([gd[Y0%(r+2,b)] + gd[Y1%(r+2,b)] + mask[b] for b in range(self.Bs) if mask[b] == 0])
                l.append(filtr)
            if self.Nr >= r+3:
                def gen_filtr(C0,C1):
                    l = [(C0[49] + C1[49] + 1)*(C0[33] + C1[33] + 1)*(C0[17] + C1[17]),
                         (C0[51] + C1[51] + 1)*(C0[35] + C1[35] + 1)*(C0[19] + C1[19]),
                         (C0[57] + C1[57])*(C0[53] + C1[53] + 1)*(C0[17] + C1[17]),
                         (C0[57] + C1[57])*(C0[53] + C1[53] + 1)*(C0[33] + C1[33]),
                         (C0[59] + C1[59])*(C0[55] + C1[55] + 1)*(C0[19] + C1[19]),
                         (C0[59] + C1[59])*(C0[55] + C1[55] + 1)*(C0[35] + C1[35]),
                         (C0[49] + C1[49] + 1)*(C0[17] + C1[17]),
                         (C0[49] + C1[49] + 1)*(C0[33] + C1[33]),
                         (C0[51] + C1[51] + 1)*(C0[19] + C1[19]),
                         (C0[51] + C1[51] + 1)*(C0[35] + C1[35]),
                         (C0[53] + C1[53] + 1)*(C0[21] + C1[21]),
                         (C0[53] + C1[53] + 1)*(C0[37] + C1[37]),
                         (C0[53] + C1[53] + 1)*(C0[49] + C0[57] + C1[49] + C1[57] + 1),
                         (C0[55] + C1[55] + 1)*(C0[23] + C1[23]),
                         (C0[55] + C1[55] + 1)*(C0[39] + C1[39]),
                         (C0[55] + C1[55] + 1)*(C0[51] + C0[59] + C1[51] + C1[59] + 1),
                         (C0[57] + C1[57] + 1)*(C0[25] + C1[25]),
                         (C0[57] + C1[57] + 1)*(C0[41] + C1[41]),
                         (C0[59] + C1[59] + 1)*(C0[27] + C1[27]),
                         (C0[59] + C1[59] + 1)*(C0[43] + C1[43]),
                         C0[ 1] + C0[33] + C0[49] + C1[ 1] + C1[33] + C1[49],
                         C0[ 3] + C0[35] + C0[51] + C1[ 3] + C1[35] + C1[51],
                         C0[ 5] + C0[37] + C0[53] + C1[ 5] + C1[37] + C1[53],
                         C0[ 7] + C0[39] + C0[55] + C1[ 7] + C1[39] + C1[55],
                         C0[ 9] + C0[41] + C0[57] + C1[ 9] + C1[41] + C1[57],
                         C0[11] + C0[43] + C0[59] + C1[11] + C1[43] + C1[59]]
                    l.extend([C0[i] + C1[i] for i in (0, 2, 4, 6, 8,10,12,13,14,15,16,18,20,22,24,26,28,29,30,31,32,34,36,38,40,42,44,45,46,47,48,50,52,54,56,58,60,61,62,63)])
                    return l
                    
                filtr = gen_filtr([gd[X0%(r+3,b)] for b in range(self.Bs)],[gd[X1%(r+3,b)] for b in range(self.Bs)])
                l.append(filtr)
            if self.Nr >= r+4:
                def gen_filtr(C0,C1):
                    l = [(C0[36] + C1[36])*(C0[ 4]*C0[20] + C0[ 4]*C0[52] + C0[ 4]*C1[20] + C0[ 4]*C1[52] + C0[20]*C0[52] + C0[20]*C1[ 4] + C0[20]*C1[52] + C0[52]*C1[ 4] + C0[52]*C1[20] + C1[ 4]*C1[20] + C1[ 4]*C1[52] + C1[20]*C1[52] + C0[ 4] + C0[20] + C0[52] + C1[ 4] + C1[20] + C1[52] + 1),
                         (C0[37] + C1[37])*(C0[ 5]*C0[21] + C0[ 5]*C0[53] + C0[ 5]*C1[21] + C0[ 5]*C1[53] + C0[21]*C0[53] + C0[21]*C1[ 5] + C0[21]*C1[53] + C0[53]*C1[ 5] + C0[53]*C1[21] + C1[ 5]*C1[21] + C1[ 5]*C1[53] + C1[21]*C1[53] + C0[ 5] + C0[21] + C0[53] + C1[ 5] + C1[21] + C1[53] + 1),
                         (C0[ 6] + C1[ 6])*(C0[22]*C0[38] + C0[22]*C0[54] + C0[22]*C1[38] + C0[22]*C1[54] + C0[38]*C0[54] + C0[38]*C1[22] + C0[38]*C1[54] + C0[54]*C1[22] + C0[54]*C1[38] + C1[22]*C1[38] + C1[22]*C1[54] + C1[38]*C1[54] + C0[22] + C0[38] + C0[54] + C1[22] + C1[38] + C1[54] + 1),
                         (C0[40] + C1[40])*(C0[ 8]*C0[24] + C0[ 8]*C0[56] + C0[ 8]*C1[24] + C0[ 8]*C1[56] + C0[24]*C0[56] + C0[24]*C1[ 8] + C0[24]*C1[56] + C0[56]*C1[ 8] + C0[56]*C1[24] + C1[ 8]*C1[24] + C1[ 8]*C1[56] + C1[24]*C1[56] + C0[ 8] + C0[24] + C0[56] + C1[ 8] + C1[24] + C1[56] + 1),
                          (C0[41] + C1[41])*(C0[ 9]*C0[25] + C0[ 9]*C0[57] + C0[ 9]*C1[25] + C0[ 9]*C1[57] + C0[25]*C0[57] + C0[25]*C1[ 9] + C0[25]*C1[57] + C0[57]*C1[ 9] + C0[57]*C1[25] + C1[ 9]*C1[25] + C1[ 9]*C1[57] + C1[25]*C1[57] + C0[ 9] + C0[25] + C0[57] + C1[ 9] + C1[25] + C1[57] + 1),
                          (C0[10] + C1[10])*(C0[26]*C0[42] + C0[26]*C0[58] + C0[26]*C1[42] + C0[26]*C1[58] + C0[42]*C0[58] + C0[42]*C1[26] + C0[42]*C1[58] + C0[58]*C1[26] + C0[58]*C1[42] + C1[26]*C1[42] + C1[26]*C1[58] + C1[42]*C1[58] + C0[26] + C0[42] + C0[58] + C1[26] + C1[42] + C1[58] + 1),
                          (C0[12] + C1[12])*(C0[28]*C0[44] + C0[28]*C0[60] + C0[28]*C1[44] + C0[28]*C1[60] + C0[44]*C0[60] + C0[44]*C1[28] + C0[44]*C1[60] + C0[60]*C1[28] + C0[60]*C1[44] + C1[28]*C1[44] + C1[28]*C1[60] + C1[44]*C1[60] + C0[28] + C0[44] + C0[60] + C1[28] + C1[44] + C1[60] + 1),
                          (C0[45] + C1[45])*(C0[13]*C0[29] + C0[13]*C0[61] + C0[13]*C1[29] + C0[13]*C1[61] + C0[29]*C0[61] + C0[29]*C1[13] + C0[29]*C1[61] + C0[61]*C1[13] + C0[61]*C1[29] + C1[13]*C1[29] + C1[13]*C1[61] + C1[29]*C1[61] + C0[13] + C0[29] + C0[61] + C1[13] + C1[29] + C1[61] + 1),
                          (C0[46] + C1[46])*(C0[14]*C0[30] + C0[14]*C0[62] + C0[14]*C1[30] + C0[14]*C1[62] + C0[30]*C0[62] + C0[30]*C1[14] + C0[30]*C1[62] + C0[62]*C1[14] + C0[62]*C1[30] + C1[14]*C1[30] + C1[14]*C1[62] + C1[30]*C1[62] + C0[14] + C0[30] + C0[62] + C1[14] + C1[30] + C1[62] + 1),
                          (C0[52] + C1[52] + 1)*(C0[20] + C1[20] + 1)*(C0[ 4] + C0[36] + C1[ 4] + C1[36]),
                          (C0[60] + C1[60] + 1)*(C0[28] + C1[28] + 1)*(C0[12] + C0[44] + C1[12] + C1[44]),
                          (C0[10] + C0[42] + C0[58] + C1[10] + C1[42] + C1[58])*(C0[ 2] + C0[34] + C0[50] + C1[ 2] + C1[34] + C1[50]),
                          C0[ 3] + C1[ 3],
                          C0[ 7] + C1[ 7],
                          C0[11] + C1[11],
                          C0[15] + C1[15],
                          C0[19] + C1[19],
                          C0[23] + C1[23],
                          C0[27] + C1[27],
                          C0[31] + C1[31],
                          C0[35] + C1[35],
                          C0[39] + C1[39],
                          C0[43] + C1[43],
                          C0[47] + C1[47],
                          C0[51] + C1[51],
                          C0[55] + C1[55],
                          C0[59] + C1[59],
                          C0[63] + C1[63]]
                    return l
                filtr = gen_filtr([gd[X0%(r+4,b)] for b in range(self.Bs)],[gd[X1%(r+4,b)] for b in range(self.Bs)])
                l.append(filtr)


        F = PolynomialSequence(l, R)
        if return_system == True:
            return F

        R = F.ring()
        gd = R.gens_dict()

        try:
            if alg == "magma":
                F = magma(F)
                alarm(timeout) # arm alarm
                gt = magma.cputime()
                gb = F.GroebnerBasis()
                gt = magma.cputime(gt)
                alarm(0) # disable alarm again

                gb = [R(str(e)) for e in gb]
                
            elif alg == "singular":
                singular.option("redTail")
                singular.option("redThrough")
                singular.option("redSB")

                F = F._singular_()

                alarm(timeout) # arm alarm
                gt = singular.cputime()
                gb = F.groebner()
                gt = singular.cputime(gt)
                alarm(0) # disable alarm again

                gb = [R(e) for e in gb]

            elif alg == "polybori":
                def _myfunc(I, q):
                    t = cputime()
                    gb = I.groebner_basis()
                    if gb == [1]:
                        gb = [1] # working around memleak until fixed
                    t = cputime(t)
                    q.put([gb, t])

                I = F.ideal()

                q = Queue()
                p = Process(target=_myfunc, args=(I,q))
                p.start()
                try:
                    gb, gt = q.get(timeout=float(timeout))
                    p.join()
                except Empty:
                    p.terminate()
                    raise KeyboardInterrupt

            elif alg == "sat":
                alarm(timeout) # arm alarm
                gt = cputime()
                
                s,t= minisat2(F)

                if not s:
                    gb = [1]
                
                gt = t
                alarm(0) # disable alarm again

            T.append(walltime(wt))
            print "% 6d, min: %7.3f, avg: %7.3f, max: %7.3f"%(counter, min(T), sum(T)/len(T), max(T))

            if gb != [1]:
                return gb

        except (KeyboardInterrupt), msg:
            print msg
            print "candidate found"
            break

    if return_system == 'success':
        return F,P0,P1,K

    # defining equations for the S-boxes
    S0 = lambda x0,x1,x2,x3: x0*x1*x3 + x0*x2*x3 + x0 + x1*x2*x3 + x1*x2 + x2 + x3 + 1
    S1 = lambda x0,x1,x2,x3: x0*x1*x3 + x0*x2*x3 + x0*x2 + x0*x3 + x0 + x1 + x2*x3 + 1
    S2 = lambda x0,x1,x2,x3: x0*x1*x3 + x0*x1 + x0*x2*x3 + x0*x2 + x0 + x1*x2*x3 + x2
    S3 = lambda x0,x1,x2,x3: x0 + x1*x2 + x1 + x3

    s = self.s

    ### first key batch

    offset = (self.B-1 - it.offset[0])*s # Wang is big endian

    k0,k1,k2,k3 = self.K0[offset:offset+s]
    d0,d1,d2,d3 = D1[offset:offset+s]
    p0,p1,p2,p3 = P0[offset:offset+s]
    q0,q1,q2,q3 = P1[offset:offset+s]

    Fbar0 = [ d0 + S0(p0+k0, p1+k1, p2+k2, p3+k3) + S0(q0+k0, q1+k1, q2+k2, q3+k3),
              d1 + S1(p0+k0, p1+k1, p2+k2, p3+k3) + S1(q0+k0, q1+k1, q2+k2, q3+k3),
              d2 + S2(p0+k0, p1+k1, p2+k2, p3+k3) + S2(q0+k0, q1+k1, q2+k2, q3+k3),
              d3 + S3(p0+k0, p1+k1, p2+k2, p3+k3) + S3(q0+k0, q1+k1, q2+k2, q3+k3)]

    K0 = tuple(K[offset:offset+s])

    ### second key batch

    offset = (self.B-1- it.offset[1] )*s # Wang is big endian

    k0,k1,k2,k3 = self.K0[offset:offset+s]
    d0,d1,d2,d3 = D1[offset:offset+s]
    p0,p1,p2,p3 = P0[offset:offset+s]
    q0,q1,q2,q3 = P1[offset:offset+s]

    Fbar1 = [ d0 + S0(p0+k0, p1+k1, p2+k2, p3+k3) + S0(q0+k0, q1+k1, q2+k2, q3+k3),
              d1 + S1(p0+k0, p1+k1, p2+k2, p3+k3) + S1(q0+k0, q1+k1, q2+k2, q3+k3),
              d2 + S2(p0+k0, p1+k1, p2+k2, p3+k3) + S2(q0+k0, q1+k1, q2+k2, q3+k3),
              d3 + S3(p0+k0, p1+k1, p2+k2, p3+k3) + S3(q0+k0, q1+k1, q2+k2, q3+k3)]

    K1 = tuple(K[offset:offset+s])

    # return learned relations AND real key values for verification
    return (tuple(self.R.ideal(Fbar0).groebner_basis()), K0), \
           (tuple(self.R.ideal(Fbar1).groebner_basis()), K1)



def present_dc_implications(self, **kwds):
    """
    """
    r = kwds.get("r", 1)
    Bs = self.Bs

    R = self.ring(n=2,reverse_variables=True)
    variable_names = ["C0000%03d"%i for i in range(Bs)] + ["C1000%03d"%i for i in range(Bs)] \
        + list(R.variable_names()) + \
        ["P0000%03d"%i for i in range(Bs)] + ["P1000%03d"%i for i in range(Bs)]
    term_order = 'deglex(%d),deglex(%d)'%(len(variable_names)-2*Bs-self.Ks,2*Bs+self.Ks)
                
    R = BooleanPolynomialRing(len(variable_names), variable_names, order=term_order)
    self.R = R
    P0 = [R("P0000%03d"%i) for i in range(Bs)]
    P1 = [R("P1000%03d"%i) for i in range(Bs)]
    C0 = [R("C0000%03d"%i) for i in range(Bs)]
    C1 = [R("C1000%03d"%i) for i in range(Bs)]

    diff_iter = Wang6DiffIter
    it = diff_iter(self)
    DP = it.next()
    P1 = [P0[i] + DP[i] for i in xrange(len(P0))]

    l = []

    l = []

    # generate polynomial systems
    gen = self.new_generator(postfix='0')
    gen.R = R
    F,s = gen.polynomial_system(P=P0, C=C0, **kwds)
    for i in xrange(r+1):
        l.append(F.part(i))

    gen = self.new_generator(postfix='1')
    gen.R = R
    F,s = gen.polynomial_system(P=P1, C=C1, **kwds)
    for i in xrange(r+1):
        l.append(F.part(i))

    gd = R.gens_dict()
    it = diff_iter(self)

    for i in xrange(min(r,self.Nr)):
        DX = it.next()
        X0 = self.varformatstr("X0")
        X1 = self.varformatstr("X1")

        l.append([gd[X0%(i+1,b)] + gd[X1%(i+1,b)] + DX[b] for b in range(self.Bs)])

        DY = it.next()
        if i == 0: 
            D1 = DY

        Y0 = self.varformatstr("Y0")
        Y1 = self.varformatstr("Y1")

        l.append([gd[Y0%(i+1,b)] + gd[Y1%(i+1,b)] + DY[b] for b in range(self.Bs)])

    F =  PolynomialSequence(l)
    return F


def present_dc_wrong_pair(self, **kwds):
    """
    """
    Bs = self.Bs

    self = copy(self)
    r = kwds.get("r", 1)
    K = kwds.get("K", self.random_element(80))

    R = self.ring(n=2)
    variable_names = list(R.variable_names()) + ["C0000%03d"%i for i in range(Bs)] + ["C1000%03d"%i for i in range(Bs)]

    term_order = R.term_order()
                

    it = Wang6DiffIter(self)

    R = BooleanPolynomialRing(len(variable_names), variable_names, order=term_order)
    self.R = R
    C0 = [R("C0000%03d"%i) for i in range(Bs)]
    C1 = [R("C1000%03d"%i) for i in range(Bs)]
    # request new plaintexts
    DP = it[0]
    P0 = self.random_element()
    P1 = [P0[i] + DP[i] for i in range(Bs)]

    l = []

    # generate polynomial systems
    gen0 = self.new_generator(postfix='0')
    gen0.R = R
    F,s = gen0.polynomial_system(P=P0, C=C0, **kwds)
    for i in xrange(r+1):
        l.append(F.part(i))

    gen1 = self.new_generator(postfix='1')
    gen1.R = R
    F,s = gen1.polynomial_system(P=P1, C=C1, **kwds)
    for i in xrange(r+1):
        l.append(F.part(i))

    gd = R.gens_dict()
        
    Y0 = self.varformatstr("Y0")
    Y1 = self.varformatstr("Y1")
    DY = it[2*r-1]

    diff = []
    for b in range(self.Bs):
        diff.append(gd[Y0%(r,b)] + gd[Y1%(r,b)] + DY[b])
    l.append(diff)

    #l.append(map(add,zip(self.vars("K",0),K))[:40])

    F =  PolynomialSequence(l)
    F.ring().set()
    s,t = minisat2(F)

    print "%5s %8.3f"%(bool(s),t)


def present_dc_right_pair(self, fixed=None,**kwds):
    """
    """
    Bs = self.Bs

    self = copy(self)
    r = kwds.get("r", 1)
    K = kwds.get("K", self.random_element(self.Ks))

    if "K" in kwds:
        del kwds["K"]

    R = self.ring(n=2)
    variable_names = ["P0000%03d"%i for i in range(Bs)] + ["P1000%03d"%i for i in range(Bs)] + list(R.variable_names()) + ["C0000%03d"%i for i in range(Bs)] + ["C1000%03d"%i for i in range(Bs)]

    term_order = R.term_order()

    it = Wang6DiffIter(self)

    R = BooleanPolynomialRing(len(variable_names), variable_names, order=term_order)
    self.R = R
    gd = R.gens_dict()
    P0 = [gd["P0000%03d"%i] for i in range(Bs)]
    P1 = [gd["P1000%03d"%i] for i in range(Bs)]
    C0 = [gd["C0000%03d"%i] for i in range(Bs)]
    C1 = [gd["C1000%03d"%i] for i in range(Bs)]
    # request new plaintexts
    DP = it[0]
    P1 = [P0[i] + DP[i] for i in range(Bs)]

    l = []

    # generate polynomial systems
    gen0 = self.new_generator(postfix='0')
    gen0.R = R
    F,s = gen0.polynomial_system(P=P0, C=C0, **kwds)
    for i in xrange(r+1):
        l.append(F.part(i))

    gen1 = self.new_generator(postfix='1')
    gen1.R = R
    F,s = gen1.polynomial_system(P=P1, C=C1, **kwds)
    for i in xrange(r+1):
        l.append(F.part(i))

    gd = R.gens_dict()

    for i in xrange(min(r+1,self.Nr)):
        DX = it.next()
        X0 = self.varformatstr("X0")
        X1 = self.varformatstr("X1")

        l.append([gd[X0%(i+1,b)] + gd[X1%(i+1,b)] + DX[b] for b in range(self.Bs)])

        DY = it.next()
        Y0 = self.varformatstr("Y0")
        Y1 = self.varformatstr("Y1")

        if i<r:
            l.append([gd[Y0%(i+1,b)] + gd[Y1%(i+1,b)] + DY[b] for b in range(self.Bs)])
            
    l.append(map(add,zip(self.vars("K",0),K)[:80]))


    F =  PolynomialSequence(l)
    F.ring().set()
    if fixed:
        s = dict([(P0[i],0) for i in fixed])
        F = F.subs(s)
    F = F.eliminate_linear_variables(maxlength=1)
    s,t = minisat2(F,randomize=True)
    return [s.get(P0[i],0) for i in range(Bs)], [s.get(k,K[i]) for i,k in enumerate(self.vars("K",0))]


def present_dc_filter_pair(self, r):
    F = present_dc(self,r=r,characteristic=False,last_round='symbolic',return_system=True)
    R = F.ring()
    gd = R.gens_dict()
    Bs = self.Bs
    C0 = [gd["C000%02d"%i] for i in range(Bs)]
    C1 = [gd["C100%02d"%i] for i in range(Bs)]
    K = [gd["K00%02d"%i] for i in range(80)]
    l = [C1[i] + GF(2).random_element() for i in range(Bs)]
    l += [K[i] + GF(2).random_element() for i in range(80)]

    s,t = minisat2(F2,randomize=True)
    return [s[C0[i]] for i in range(Bs)],[s[C1[i]] for i in range(Bs)]

def present_dc_filter_pair_alt(self, r):
    GF2 = GF(2)
    B = BooleanPolynomialRing(64,["C%02d"%i for i in range(64)])

    C0 = [GF2.random_element() for _ in range(64)]
    C1 = B.gens()

    if r not in (2,6,10,14):
        raise ValueError

    if self.Nr - r == 2:
        l = [(C0[57] + C1[57] + 1)*(C0[41] + C1[41] + 1)*(C0[25] + C1[25]),
             (C0[57] + C1[57])*(C0[53] + C1[53] + 1)*(C0[17] + C1[17]),
             (C0[57] + C1[57])*(C0[53] + C1[53] + 1)*(C0[33] + C1[33]),
             (C0[59] + C1[59])*(C0[55] + C1[55] + 1)*(C0[19] + C1[19]),
             (C0[59] + C1[59])*(C0[55] + C1[55] + 1)*(C0[35] + C1[35]),
             (C0[49] + C1[49] + 1)*(C0[17] + C1[17]),
             (C0[49] + C1[49] + 1)*(C0[33] + C1[33]),
             (C0[51] + C1[51] + 1)*(C0[19] + C1[19]),
             (C0[51] + C1[51] + 1)*(C0[35] + C1[35]),
             (C0[53] + C1[53] + 1)*(C0[21] + C1[21]),
             (C0[53] + C1[53] + 1)*(C0[37] + C1[37]),
             (C0[53] + C1[53] + 1)*(C0[49] + C0[57] + C1[49] + C1[57] + 1),
             (C0[55] + C1[55] + 1)*(C0[23] + C1[23]),
             (C0[55] + C1[55] + 1)*(C0[39] + C1[39]),
             (C0[55] + C1[55] + 1)*(C0[51] + C0[59] + C1[51] + C1[59] + 1),
             (C0[57] + C1[57] + 1)*(C0[25] + C1[25]),
             (C0[57] + C1[57] + 1)*(C0[41] + C1[41]),
             C0[ 0] + C1[ 0],
             C0[ 1] + C0[33] + C0[49] + C1[ 1] + C1[33] + C1[49],
             C0[ 2] + C1[ 2],
             C0[ 3] + C0[35] + C0[51] + C1[ 3] + C1[35] + C1[51],
             C0[ 4] + C1[ 4],
             C0[ 5] + C0[37] + C0[53] + C1[ 5] + C1[37] + C1[53],
             C0[ 6] + C1[ 6],
             C0[ 7] + C0[39] + C0[55] + C1[ 7] + C1[39] + C1[55],
             C0[ 8] + C1[ 8],
             C0[ 9] + C0[41] + C0[57] + C1[ 9] + C1[41] + C1[57],
             C0[10] + C1[10],
             C0[11] + C0[43] + C0[59] + C1[11] + C1[43] + C1[59],
             C0[12] + C1[12],
             C0[13] + C1[13],
             C0[14] + C1[14],
             C0[15] + C1[15],
             C0[16] + C1[16],
             C0[18] + C1[18],
             C0[20] + C1[20],
             C0[22] + C1[22],
             C0[24] + C1[24],
             C0[26] + C1[26],
             C0[28] + C1[28],
             C0[29] + C1[29],
             C0[30] + C1[30],
             C0[31] + C1[31],
             C0[32] + C1[32],
             C0[34] + C1[34],
             C0[36] + C1[36],
             C0[38] + C1[38],
             C0[40] + C1[40],
             C0[42] + C1[42],
             C0[44] + C1[44],
             C0[45] + C1[45],
             C0[46] + C1[46],
             C0[47] + C1[47],
             C0[48] + C1[48],
             C0[50] + C1[50],
             C0[52] + C1[52],
             C0[54] + C1[54],
             C0[56] + C1[56],
             C0[58] + C1[58],
             C0[60] + C1[60],
             C0[61] + C1[61],
             C0[62] + C1[62],
             C0[63] + C1[63]]

    elif self.Nr - r == 3:
        l = [(C0[36] + C1[36])*(C0[ 4]*C0[20] + C0[ 4]*C0[52] + C0[ 4]*C1[20] + C0[ 4]*C1[52] + C0[20]*C0[52] + C0[20]*C1[ 4] + C0[20]*C1[52] + C0[52]*C1[ 4] + C0[52]*C1[20] + C1[ 4]*C1[20] + C1[ 4]*C1[52] + C1[20]*C1[52] + C0[ 4] + C0[20] + C0[52] + C1[ 4] + C1[20] + C1[52] + 1),
             (C0[37] + C1[37])*(C0[ 5]*C0[21] + C0[ 5]*C0[53] + C0[ 5]*C1[21] + C0[ 5]*C1[53] + C0[21]*C0[53] + C0[21]*C1[ 5] + C0[21]*C1[53] + C0[53]*C1[ 5] + C0[53]*C1[21] + C1[ 5]*C1[21] + C1[ 5]*C1[53] + C1[21]*C1[53] + C0[ 5] + C0[21] + C0[53] + C1[ 5] + C1[21] + C1[53] + 1),
             (C0[ 6] + C1[ 6])*(C0[22]*C0[38] + C0[22]*C0[54] + C0[22]*C1[38] + C0[22]*C1[54] + C0[38]*C0[54] + C0[38]*C1[22] + C0[38]*C1[54] + C0[54]*C1[22] + C0[54]*C1[38] + C1[22]*C1[38] + C1[22]*C1[54] + C1[38]*C1[54] + C0[22] + C0[38] + C0[54] + C1[22] + C1[38] + C1[54] + 1),
             (C0[40] + C1[40])*(C0[ 8]*C0[24] + C0[ 8]*C0[56] + C0[ 8]*C1[24] + C0[ 8]*C1[56] + C0[24]*C0[56] + C0[24]*C1[ 8] + C0[24]*C1[56] + C0[56]*C1[ 8] + C0[56]*C1[24] + C1[ 8]*C1[24] + C1[ 8]*C1[56] + C1[24]*C1[56] + C0[ 8] + C0[24] + C0[56] + C1[ 8] + C1[24] + C1[56] + 1),
             (C0[41] + C1[41])*(C0[ 9]*C0[25] + C0[ 9]*C0[57] + C0[ 9]*C1[25] + C0[ 9]*C1[57] + C0[25]*C0[57] + C0[25]*C1[ 9] + C0[25]*C1[57] + C0[57]*C1[ 9] + C0[57]*C1[25] + C1[ 9]*C1[25] + C1[ 9]*C1[57] + C1[25]*C1[57] + C0[ 9] + C0[25] + C0[57] + C1[ 9] + C1[25] + C1[57] + 1),
             (C0[10] + C1[10])*(C0[26]*C0[42] + C0[26]*C0[58] + C0[26]*C1[42] + C0[26]*C1[58] + C0[42]*C0[58] + C0[42]*C1[26] + C0[42]*C1[58] + C0[58]*C1[26] + C0[58]*C1[42] + C1[26]*C1[42] + C1[26]*C1[58] + C1[42]*C1[58] + C0[26] + C0[42] + C0[58] + C1[26] + C1[42] + C1[58] + 1),
             (C0[12] + C1[12])*(C0[28]*C0[44] + C0[28]*C0[60] + C0[28]*C1[44] + C0[28]*C1[60] + C0[44]*C0[60] + C0[44]*C1[28] + C0[44]*C1[60] + C0[60]*C1[28] + C0[60]*C1[44] + C1[28]*C1[44] + C1[28]*C1[60] + C1[44]*C1[60] + C0[28] + C0[44] + C0[60] + C1[28] + C1[44] + C1[60] + 1),
             (C0[45] + C1[45])*(C0[13]*C0[29] + C0[13]*C0[61] + C0[13]*C1[29] + C0[13]*C1[61] + C0[29]*C0[61] + C0[29]*C1[13] + C0[29]*C1[61] + C0[61]*C1[13] + C0[61]*C1[29] + C1[13]*C1[29] + C1[13]*C1[61] + C1[29]*C1[61] + C0[13] + C0[29] + C0[61] + C1[13] + C1[29] + C1[61] + 1),
             (C0[46] + C1[46])*(C0[14]*C0[30] + C0[14]*C0[62] + C0[14]*C1[30] + C0[14]*C1[62] + C0[30]*C0[62] + C0[30]*C1[14] + C0[30]*C1[62] + C0[62]*C1[14] + C0[62]*C1[30] + C1[14]*C1[30] + C1[14]*C1[62] + C1[30]*C1[62] + C0[14] + C0[30] + C0[62] + C1[14] + C1[30] + C1[62] + 1),
             (C0[52] + C1[52] + 1)*(C0[20] + C1[20] + 1)*(C0[ 4] + C0[36] + C1[ 4] + C1[36]),
             (C0[60] + C1[60] + 1)*(C0[28] + C1[28] + 1)*(C0[12] + C0[44] + C1[12] + C1[44]),
             (C0[10] + C0[42] + C0[58] + C1[10] + C1[42] + C1[58])*(C0[ 2] + C0[34] + C0[50] + C1[ 2] + C1[34] + C1[50]),
             C0[ 3] + C1[ 3],
             C0[ 7] + C1[ 7],
             C0[11] + C1[11],
             C0[15] + C1[15],
             C0[19] + C1[19],
             C0[23] + C1[23],
             C0[27] + C1[27],
             C0[31] + C1[31],
             C0[35] + C1[35],
             C0[39] + C1[39],
             C0[43] + C1[43],
             C0[47] + C1[47],
             C0[51] + C1[51],
             C0[55] + C1[55],
             C0[59] + C1[59],
             C0[63] + C1[63]]

    elif self.Nr - r == 4:
        l = [(C0[32] + C1[32] + 1)*(C0[ 0] + C1[ 0] + 1)*(C0[16] + C0[48] + C1[16] + C1[48]),
             (C0[33] + C1[33] + 1)*(C0[ 1] + C1[ 1] + 1)*(C0[17] + C0[49] + C1[17] + C1[49]),
             (C0[34] + C1[34] + 1)*(C0[ 2] + C1[ 2] + 1)*(C0[18] + C0[50] + C1[18] + C1[50]),
             (C0[35] + C1[35] + 1)*(C0[ 3] + C1[ 3] + 1)*(C0[19] + C0[51] + C1[19] + C1[51]),
             (C0[36] + C1[36] + 1)*(C0[ 4] + C1[ 4] + 1)*(C0[20] + C0[52] + C1[20] + C1[52]),
             (C0[37] + C1[37] + 1)*(C0[ 5] + C1[ 5] + 1)*(C0[21] + C0[53] + C1[21] + C1[53]),
             (C0[38] + C1[38] + 1)*(C0[ 6] + C1[ 6] + 1)*(C0[22] + C0[54] + C1[22] + C1[54]),
             (C0[39] + C1[39] + 1)*(C0[ 7] + C1[ 7] + 1)*(C0[23] + C0[55] + C1[23] + C1[55]),
             (C0[40] + C1[40] + 1)*(C0[ 8] + C1[ 8] + 1)*(C0[24] + C0[56] + C1[24] + C1[56]),
             (C0[41] + C1[41] + 1)*(C0[ 9] + C1[ 9] + 1)*(C0[25] + C0[57] + C1[25] + C1[57]),
             (C0[42] + C1[42] + 1)*(C0[10] + C1[10] + 1)*(C0[26] + C0[58] + C1[26] + C1[58]),
             (C0[43] + C1[43] + 1)*(C0[11] + C1[11] + 1)*(C0[27] + C0[59] + C1[27] + C1[59]),
             (C0[44] + C1[44] + 1)*(C0[12] + C1[12] + 1)*(C0[28] + C0[60] + C1[28] + C1[60]),
             (C0[45] + C1[45] + 1)*(C0[13] + C1[13] + 1)*(C0[29] + C0[61] + C1[29] + C1[61]),
             (C0[46] + C1[46] + 1)*(C0[14] + C1[14] + 1)*(C0[30] + C0[62] + C1[30] + C1[62]),
             (C0[47] + C1[47] + 1)*(C0[15] + C1[15] + 1)*(C0[31] + C0[63] + C1[31] + C1[63])]
            

    s,t = minisat2(PolynomialSequence(l), randomize=True)

    C0, C1 = C0, [s.get(c,GF2.random_element()) for c in C1]

    F = present_dc(self,r=r,characteristic=False,return_system=True,C0=C0,C1=C1)
    #F = F.eliminate_linear_variables(maxlength=1)
    return F

class DiffIter:
    def __init__(self, p ):
        """
        Abstract class for difference characteristic iterator.

        INPUT:
            p -- PRESENT instance
        """
        self._p = p
        self._d = []
        self._it = 0
    def __iter__(self):
        return self

    def next(self):
        if self._it < len(self._d):
            it = self._it
            self._it += 1
        else:
            it = 0
            self._it = 1

        return self._d[it]

    def __getitem__(self,i):
        return self._d[i]

    def get(self, r, after_player=False):
        if r == 0:
            return self._d[0]
        else:
            if not after_player:
                return self._d[2*r-1]
            else:
                return self._d[2*r]

class PRESENTDiffIter(DiffIter):
    def __init__(self, p):
        """
        Difference characteristic as provided in the original PRESENT
        paper.
        """
        DiffIter.__init__(self, p)
        self.offset = [0,3]
        self._d = [differential(p,x0=4,x3=4),
                   differential(p,x0=5,x3=5),
                   
                   differential(p,x0=9,x8=9),
                   differential(p,x0=4,x8=4),
                   
                   differential(p,x8=1,x10=1),
                   differential(p,x8=9,x10=9),
                   
                   differential(p,x2=5,x14=5),
                   differential(p,x2=1,x14=1)]

class Wang6DiffIter(DiffIter):
    def __init__(self, p):
        """
        Difference characteristic as provided in Wang's ePrint paper
        and via private communication.
        """
        DiffIter.__init__(self, p)
        self.offset = [2,14]
        self._d = [differential(p, x2=7, x14=7),

                   # R1
                   differential(p, x2=1, x14=1),
                   differential(p, x0=4, x3=4),
                   
                   # R2
                   differential(p, x0=5, x3=5),
                   differential(p, x0=9, x8=9),

                   # R3
                   differential(p, x0=4, x8=4),
                   differential(p, x8=1, x10=1),

                   # R4
                   differential(p, x8=9, x10=9),
                   differential(p, x2=5, x14=5),

                   # R5            
                   differential(p, x2=1, x14=1),
                   differential(p, x0=4, x3=4),

                   # R6
                   differential(p, x0=5, x3=5),
                   differential(p, x0=9, x8=9),

                   # R7
                   differential(p, x0=4, x8=4),
                   differential(p, x8=1, x10=1),

                   # R8
                   differential(p, x8=9, x10=9),
                   differential(p, x2=5, x14=5),
                   
                   # R9
                   differential(p, x2=1, x14=1),
                   differential(p, x0=4, x3=4),
                   
                   # R10
                   differential(p, x0=5, x3=5),
                   differential(p, x0=9, x8=9),

                   # R11
                   differential(p, x0=4, x8=4),
                   differential(p, x8=1, x10=1),
        
                   # R12
                   differential(p, x8=9, x10=9),
                   differential(p, x2=5, x14=5),
        
                   # R13
                   differential(p, x2=1, x14=1),
                   differential(p, x0=4, x3=4),

                   # R14
                   differential(p, x0=5, x3=5),
                   differential(p, x0=9, x8=9),

                   # cutoff
                   # R15
                   differential(p, x0=4, x8=4),
                   differential(p, x8=1, x10=1),
        
                   # R16
                   differential(p, x8=9, x10=9),
                   differential(p, x2=5, x14=5),
        
                   # R17
                   differential(p, x2=1, x14=1),
                   differential(p, x0=4, x3=4),

                   # R18
                   differential(p, x0=5, x3=5),
                   differential(p, x0=9, x8=9),

                   # R19
                   differential(p, x0=4, x8=4),
                   differential(p, x8=1, x10=1),
        
                   # R20
                   differential(p, x8=9, x10=9),
                   differential(p, x2=5, x14=5),
        
                   # R21
                   differential(p, x2=1, x14=1),
                   differential(p, x0=4, x3=4),

                   # R22
                   differential(p, x0=5, x3=5),
                   differential(p, x0=9, x8=9),

                   # R23
                   differential(p, x0=4, x8=4),
                   differential(p, x8=1, x10=1),
        
                   # R24
                   differential(p, x8=9, x10=9),
                   differential(p, x2=5, x14=5),
        
                   # R25
                   differential(p, x2=1, x14=1),
                   differential(p, x0=4, x3=4),

                   # R26
                   differential(p, x0=5, x3=5),
                   differential(p, x0=9, x8=9),

                   # R27
                   differential(p, x0=4, x8=4),
                   differential(p, x8=1, x10=1),
        
                   # R28
                   differential(p, x8=9, x10=9),
                   differential(p, x2=5, x14=5),
        
                   # R29
                   differential(p, x2=1, x14=1),
                   differential(p, x0=4, x3=4),

                   # R30
                   differential(p, x0=5, x3=5),
                   differential(p, x0=9, x8=9),
                   ]
        
    def __len__(self):
        return len(self._d)


def filter_3r(self, D):
    """
    INPUT:
        self -- PRESENT polynomial system generator
        D -- list of length 64
    """
    nonzeros = [3, 7, 11, 15, 19, 23, 27, 31, 35, 39, 43, 47, 51, 55, 59, 63]

    for p in nonzeros:
        if D[p] != 0:
            return False
    return True
    
def filter_wang(self, D):
    """
    INPUT:
        self -- PRESENT polynomial system generator
        D -- list of length 64
    """
    S = self.sbox()
    s = self.s

    nonzeros = map(lambda x: x*s, [15-4, 15-6, 15-8, 15-10, 15-12, 15-14])

    # strip last permutation layer
    D = self.pLayer_inverse(D)
    
    for i in range(0,64,4):
        v = S.from_bits( D[i:i+4] )

        if i in nonzeros:
            if v not in [3,7,9,13,0]:
                return False
        if i not in nonzeros:
            if v != 0:
                return False
    return True

##################### Integral Attack ##########################

def present_ia(self, **kwds):
    """
    REFERENCES:
        Muhammad Reza Z'Aba, Havard Raddum, Matt Henricksen, Ed
        Dawson; Bit-Pattern Based Integral Attack Lecture Notes In
        Computer Science; Fast Software Encryption: 15th International
        Workshop, FSE 2008, Lausanne, Switzerland, February 10-13,
        2008, Revised Selected Papers book contents; 2008
        available at: http://www.springerlink.com/content/q681724233m41264/
    """
    reverse_variables= kwds.get("reverse_variables", False)
    n = kwds.get("n", 1)
    explicit = kwds.get("explicit", True)
    leave_out_last_round = kwds.get("leave_out_last_round", False)
    min_round = kwds.get("min_round",0)
    sample_major = kwds.get("sample_major",False)


    size = 2**4
    total = size*n
    generators = [self.new_generator(postfix="%d"%i) for i in range(total)]

    R = self.ring(sample_major=sample_major, n=total, reverse_variables=reverse_variables, order=kwds.get('order','degrevlex'))

    for gen in generators:
        gen.R = R

    oldR = self.R
    self.R = R

    gd = R.gens_dict()
    varformatstr = self.varformatstr
    var = lambda name,i,j,k: gd[varformatstr("%s%d"%(name,i))%(j,k)]

    K = kwds.get("K", self.random_element(self.Ks))

    kwds["K"] = K
    sbox = self.sbox()

    L = []
    L2 = []

    for structure in range(n):
        offset = structure*16
        P = kwds.get("P", self.random_element())
        C = self(P,K)
        for i in range(size):
            v = sbox.to_bits(i)
            P[51] = v[0] # paper uses bit-slice notation
            P[55] = v[1]
            P[59] = v[2]
            P[63] = v[3]
            kwds["P"] = P
            C = self(P,K)
            F,s = generators[offset+i].polynomial_system(**kwds)

            if not leave_out_last_round:
                rounds = F.rounds()
            else:
                rounds = F.rounds()[:-1]

            for j in range(min_round,len(rounds)):
                if i == 0:
                    L.append(rounds[j].gens())
                else:
                    L.append(map(add,zip(L[j],rounds[j])))
    
        if explicit and self.Nr >= 1:
            l = []
            for sample in xrange(offset+1,offset+size):
                for i in xrange(48): # the first S-boxes are all identical
                    l.append( var("X",offset+0,1,i) + var("X",sample,1,i) ) 
                    l.append( var("Y",offset+0,1,i) + var("Y",sample,1,i) )
                for i in (48,49,50, 52,53,54, 56,57,58, 60,61,62): # these inputs are all identical
                    l.append( var("X",offset+0,1,i) + var("X",sample,1,i) ) 

            for sample in xrange(offset+0,offset+size,2): # a_0
                l.append( var("X",sample,1,63) + var("X",sample+1,1,63) + 1)
                l.append( var("Y",sample,1,63) + var("Y",sample+1,1,63) + 1)

            for sample in xrange(offset+0,offset+size,2): # a_1
                l.append( var("X",sample,1,59) + var("X",sample+1,1,59))
                for i in (56,57,58,59):
                    l.append( var("Y",sample,1,i) + var("Y",sample+1,1,i))

            for sample in xrange(offset+0,offset+size,4): # a_2
                l.append( var("X",sample,1,55) + var("X",sample+1,1,55) )
                l.append( var("X",sample,1,55) + var("X",sample+2,1,55) )
                l.append( var("X",sample,1,55) + var("X",sample+3,1,55) )
                for i in (52,53,54,55):
                    l.append( var("Y",sample,1,i) + var("Y",sample+1,1,i) )
                    l.append( var("Y",sample,1,i) + var("Y",sample+2,1,i) )
                    l.append( var("Y",sample,1,i) + var("Y",sample+3,1,i) )

            for sample in xrange(offset+0,offset+size,8): # a_3
                l.append( var("X",sample,1,51) + var("X",sample+1,1,51) )
                l.append( var("X",sample,1,51) + var("X",sample+2,1,51) )
                l.append( var("X",sample,1,51) + var("X",sample+3,1,51) )
                l.append( var("X",sample,1,51) + var("X",sample+4,1,51) )
                l.append( var("X",sample,1,51) + var("X",sample+5,1,51) )
                l.append( var("X",sample,1,51) + var("X",sample+6,1,51) )
                l.append( var("X",sample,1,51) + var("X",sample+7,1,51) )

                for i in (48,49,50,51):
                    l.append( var("Y",sample,1,i) + var("Y",sample+2,1,i) )
                    l.append( var("Y",sample,1,i) + var("Y",sample+1,1,i) )
                    l.append( var("Y",sample,1,i) + var("Y",sample+3,1,i) )
                    l.append( var("Y",sample,1,i) + var("Y",sample+4,1,i) )
                    l.append( var("Y",sample,1,i) + var("Y",sample+5,1,i) )
                    l.append( var("Y",sample,1,i) + var("Y",sample+6,1,i) )
                    l.append( var("Y",sample,1,i) + var("Y",sample+7,1,i) )
            L.append(l)

        if explicit and self.Nr >= 2:
            l = []
            for sample in xrange(offset+1,offset+size):
                for i in xrange(self.Bs):
                    if i not in (12,13,14,15, 28,29,30,31, 44,45,46,47, 60,61,62,63): 
                        l.append( var("X",offset+0,2,i) + var("X",sample,2,i) ) 
                        l.append( var("Y",offset+0,2,i) + var("Y",sample,2,i) )

            # todo: add better equations for X_2
            for i in xrange(self.Bs):
                if i in (12,13,14,15, 28,29,30,31, 44,45,46,47, 60,61,62,63):
                    l.append( sum([ var("X",sample,2,i) for sample in range(size) ]) )
                    l.append( sum([ var("Y",sample,2,i) for sample in range(size) ]) )

            L.append(l)

        if explicit and self.Nr >= 3:
            l = []
            for sample in xrange(offset+1,offset+size):
                for i in xrange(self.Bs):
                    if i%4 != 3:
                        l.append( var("X",offset+0,3,i) + var("X",sample,3,i) ) 

            l.extend( [ sum([ var("Y",j,3,i) for j in range(offset,offset+size) ]) for i in range(self.Bs) ] )
            L.append(l)

        if explicit and self.Nr >= 4:
            l =  [ sum([ var("X",j,4,i) for j in range(offset,offset+size) ]) for i in range(self.Bs) ]

            variable_map = {}
            for sample in range(16):
                for j in range(64):
                    variable_map["X%d%02d%02d"%(sample,4,j)] = var("X",offset+sample,4,j)

            for line in open("../data/present-80-4-ia.txt"):
                l.append(eval(line,{},variable_map))
            L.append(l)

        if explicit and self.Nr >= 5:
            l = []

            variable_map = {}
            for sample in range(16):
                for j in range(64):
                    variable_map["X%d%02d%02d"%(sample,5,j)] = var("X",offset+sample,5,j)

            for line in open("../data/present-80-5-ia.txt"):
                l.append(eval(line,{},variable_map))
            L.append(l)


    s = dict(zip(self.vars("K",0),K))
    self.R = oldR
    return PolynomialSequence(L), s

def filter_equations(equations, reject, pair_check=True):
    """
    Filters the provided set of equations by rejecting any equation
    for which reject returns True for any variable. Also rejects any
    equation which does not involve multiple trials, i.e. an equation
    that is local.

    INPUT:
        equations -- a list/set of equations
        reject -- anything which returns True or False and takes four
                  arguments.
        pair_check -- default:False

        sage: execfile('present.py') # output random
        ... 
        sage: p = PRESENT(80,1)
        sage: l = ia_find_relations(p,nalg=1,neval=2) # runtimes are random-ish
        0   2220   19.876
        <BLANKLINE>
        0   2138    3.795
        1   2119    3.129

        sage: len(filter_equations(l, reject=lambda n,t,r,b: n!='X'))
        960
    """
    L = []
    for f in equations:
        V = f.variables()
        Vbar = []
        for v in V:
            v = str(v)
            if v[0] == "K":
                Vbar.append( (v[0],0,int(v[-4:-2]),int(v[-2:])) )
            else:
                Vbar.append( (v[0],int(v[1:-4]),int(v[-4:-2]),int(v[-2:])) )
        V = Vbar
        if pair_check and len(set([t for (n,t,r,b) in V])) <= 1:
            continue
        if any(reject(n,t,r,b) for (n,t,r,b) in V):
            continue

        L.append(f)
            
    return L


def present_hodc_factory(self, offset=0):
    size = 2
    names = "ABCD"
    Bs, Ks = 64, self.Ks
    
    P = [["p%s%d" % (i,j) for j in range(Bs)] for i in names[:size]]
    C = [["q%s%d" % (i,j) for j in range(Bs)] for i in names[:size]]
    K = [ "K%04d" % i for i in range(Ks)]

    R = self.ring(n=size,names=names)

    order = TermOrder("deglex",R.ngens()+size*Bs) + TermOrder("deglex",size*Bs)
    R = BooleanPolynomialRing( size*Bs + R.ngens() + size*Bs , sum(P,[]) + list(R.variable_names()) + sum(C,[]), order=order)
    Kv = [ R(k) for k in K]
    Pv = [[R(p) for p in P[i]] for i in range(size)]
    Cv = [[R(c) for c in C[i]] for i in range(size)]

    # Generate 1-round forward equations
    L = []
    for i in range(2):
        gen = self.new_generator(postfix=names[i],sbox_representation='lex')
        gen.R = R
        F,s = gen.polynomial_system(P=Pv[i],C=Cv[i])

        l = []
        for r in F.rounds()[1:]:
            l.append(list(Ideal(r.gens()).groebner_basis()))
        L.append(sum(l,[])) # first four are key schedule polynomials

    L.append([])
    for i in range(Bs):
        L[-1].append(Pv[0][i] + Pv[1][i])
        if i == offset:
            L[-1][-1] +=1
    return Ideal(sum(L,[]))#.groebner_basis(deg_bound=2,faugere=False,linear_algebra_in_last_block=False)


def ia_find_relations(self, deg_bound=2, nalg=1, neval=20, R=None, leave_out_last_round=True, relations=None, reject=None, **kwds):
    r"""
    INPUT:
        nalg -- number of algebraic trials
        neval -- number of evalution trials
        R -- polynomial ring to do computations in (optional)
        leave_out_last_round -- don't consider the last key addition
                                when computing algebraically (default:
                                True)
        relations -- a precomputed set of relations to refine further
                     using this function (default: None)
        reject -- see \code{filter_equations}
    """
    if reject is None:
        reject = lambda n,t,r,b : False

    if relations is None:
        relations = []

    relations = filter_equations(relations, reject)

    for i in range(nalg):
        F,s = present_ia(self, explicit=True, boomerang=False, leave_out_last_round=leave_out_last_round, **kwds)

        t = cputime()
        gb = F.groebner_basis(faugere=False, linear_algebra_in_last_block=False, deg_bound=deg_bound, prot=True)
        gb = filter_equations(gb, reject)
        t = cputime(t)
        if i == 0 and relations == []:
            relations = set(gb)
        else:
            # TODO: this might not be optimal?
            relations = relations.union(gb)
        print "% 3d % 6d %8.3f"%(i, len(relations), t)
        sys.stdout.flush()

    # evaluation step
    generators = [self.new_generator(postfix="%d"%i) for i in range(2**4)]

    if R is None:
        R = self.ring(n=2**4)

    sage.rings.polynomial.pbori.set_cring(R)

    for gen in generators:
        gen.R = R

    oldR = self.R
    self.R = R

    print 

    relations = filter_equations(relations, reject)

    for i in xrange(neval):
        P = self.random_element()
        K = self.random_element(self.Ks)
        sbox = self.sbox()

        t = cputime()
        solution = {}
        for step in xrange(2**4):
            v = sbox.to_bits(step)
            P[51] = v[0] # paper uses bit-slice notation
            P[55] = v[1]
            P[59] = v[2]
            P[63] = v[3]
            solution.update( generators[step].solution(P,K) )

        linear = [k+v for k,v in solution.iteritems()]
        rb = ll_encode(linear)

        survivors = set()
        for f in relations:
            fbar = ll_red_nf(f, rb)
            if fbar == 0:
                survivors.add(f)
        relations = survivors
        t = cputime(t)
        print "% 3d % 6d %8.3f"%(i, len(relations), t)
        sys.stdout.flush()
    return relations

def differential_holds(P0,P1,K,DC,r):
    p = PRESENT(80,r)
    C0 = p(P0,K)
    C1 = p(P1,K)
    s = [C0[i] + C1[i] + DC[i] for i in range(p.Bs)]
    ret = all([e == 0 for e in s])
    return ret
    
def prob_nonzero(F, V=None, limit=15):
    if not isinstance(F, (list,tuple)):
        F = [F]
    if V is None:
        V = reduce(union,map(lambda f: f.variables(), F))
    V = sorted(V)
    VS = VectorSpace(GF(2),len(V))

    i = ZZ(0)
    if len(V) <= limit:
        for v in VS:
            rb = ll_encode(map(sum,zip(V,v)))
            for f in F:
                if ll_red_nf(f, rb):
                    i+=1
                    break            
        return i/len(VS)
    else:
        for j in range(2**limit):
            v = VS.random_element()
            rb = ll_encode(map(sum,zip(V,v)))
            for f in F:
                if ll_red_nf(f, rb):
                    i+=1
                    break            
        return i/(2**limit)

def connection_graph(F):
    V = sorted(reduce(union,map(lambda f: f.variables(), F)))
    g = Graph()
    g.add_vertices(sorted(V))
    for f in F:
        v = f.variables()
        a,tail = v[0],v[1:]
        for b in tail:
            g.add_edge((a,b))
    return g

def connected_sets(F, g=None):
    if g is None:
        g = connection_graph(F)
    R = F[0].ring()
    C = g.connected_components()

    P = [[] for _ in range(len(C))]
    for f in F:
        for i,c in enumerate(C):
            if set(f.variables()).difference(c) == set():
                P[i].append(f)
                break
    return C,P

def prob_zero_approx(F, limit=16, quite=False):
    g = connection_graph(F)
    C,P = connected_sets(F, g)
    if not quite:
        print "%d connected sets."%len(C)
    
    res = ZZ(1)

    for i in range(len(C)):
        c,p = C[i],P[i]
        prob = (1-prob_nonzero(p,c,limit=limit))
        res *= prob
        if not quite:
            try:
                print i, len(c), len(p), log_b(1/prob,2), log_b(1/res,2)
            except ZeroDivisionError:
                print i, len(c), len(p), ">",limit, "inf"
    return log_b(1/res,2)


def filter_passes(X0,X1,Y0,Y1):
    if X0[0] != X1[0] or X0[1] != X1[1] or X0[2] != X1[2]:
        return False
    return True

def alg_filter(C0, C1):
    for i in range(16):
        if C0[i] + C0[32+i] + C0[48 + i] +  C1[i] + C1[32+i] + C1[48 + i]:
            return False
    for i in range(16):
        if (C0[i+48] + C1[i+48] + 1)*(C0[i+32] + C1[i+32]):
            return False
        if (C0[i+48] + C1[i+48] + 1)*(C0[i+16] + C1[i+16]):
            return False
    
    return True

def present_dc_check(self, r, ntrials=100):
  diff_iter = Wang6DiffIter
  it = diff_iter(self)
  DP = it[0]
  DC = it.get(r=r, after_player=True)
  j = 0
  while True:
      if j>=ntrials:
          break
      F,P0,P1,K = present_dc(self, r=1, return_system='success')
      if not differential_holds(P0,P1, K, DC, r=r):
          print P0
          print P1
          print K
          VS = VectorSpace(GF(2),8)
          self.R = F.ring()
          Kvar = list(self.K0)
          for i in range(1,r+2):
              Ki, Kvar, _ = self.key_schedule_polynomials(Kvar, i)
          
          Vr = Ki[28:32] + Ki[60:64]
          for v in VS:
              print v
              sys.stdout.flush()
              rb = ll_encode(map(add,zip(Vr,v)))
              Fbar = Ideal([ll_red_nf(f, rb) for f in F])
              if len(Fbar.groebner_basis(faugere=False, linear_algebra_in_last_block=False)) > 1:
                  return Fbar
          print "success"
          return
      else:
          print "holds"
        
def minisat_experiment(p, r, trials, **kwds):
    T = []
    passes = 0
    timeout = kwds.get("timeout",0)
    timedout = 0
    for i in range(trials): 
        wt = walltime()
        if "factory" in kwds:
            F = kwds['factory'](p, r)
        else:
            F = present_dc(p, r=r, return_system=True, **kwds)
        try:
            alarm(timeout) # arm alarm
            s,t =minisat2(F)
            alarm(0)
            if s:
                passes += 1
            T.append(t)
            print "i: %5d   passes: %3d   timeout: %3d   min:   %.3f   avg: %.3f   med: %.3f   max: %.3f"%(i+1,passes,timedout,min(T),sum(T)/len(T),sorted(T)[len(T)/2],max(T))
        except KeyboardInterrupt:
            print "i: %5d   TIMEOUT"%(i+1)
            timedout += 1
        sys.stdout.flush()
    print
    print "min: %9.3f\navg: %9.3f\nmed: %9.3f\nmax: %9.3f"%(min(T),sum(T)/len(T),sorted(T)[len(T)/2],max(T))

def minisat_experiment1(p, r, trials, timeout=120.0, **kwds):
    while True:
        F = present_dc(p, r=r, return_system=True, **kwds)
        F = F.eliminate_linear_variables(maxlength=1)
        alarm(timeout)
        try:
            s,t =minisat2(F, randomize=False)
            alarm(0)
            print "t: %.3f"%(t,)
            sys.stdout.flush()
        except KeyboardInterrupt:
            break
    print F
    T = []
    gens = list(F.variables())
    for i in range(trials): 
        shuffle(gens)

        F2 = F + [v + GF(2).random_element() for v in gens[:40]]
        F2 = F2.eliminate_linear_variables(maxlength=1)
        if len(F2.variables()) > len(F.variables()) - 80:
            continue

        print F2

        alarm(timeout)
        try:
            s,t =minisat2(F2)
            alarm(0)
            T.append(t)
            print "i: %5d   min:   %.3f   avg: %.3f   med: %.3f   max: %.3f"%(i+1,min(T),sum(T)/len(T),sorted(T)[len(T)/2],max(T))
            sys.stdout.flush()
        except KeyboardInterrupt:
            print "TIMEOUT"
            sys.stdout.flush()
            pass


def minisat_experiment_precomputed(fn):
    for i,line in enumerate(open(fn).readlines()):
        if line.startswith("#"):
            line = line[1:].split(";")
            Nr = int(line[0].strip()[2:])
            r = int(line[1].strip()[2:])
            K = hexstringtobitstring(line[2].strip()[2:].replace(" ","0"),80)
            print bitstringtohexstring(K)
            continue
        line = line.split(";")
        P0,P1,C0,C1 = map(hexstringtobitstring,line)
        p = PRESENT(80,Nr, sbox_representation=3)
        assert(p(P0,K) == C0)
        assert(p(P1,K) == C1)
        F = present_dc(p,r=r,P0=P0,C0=C0,C1=C1,return_system=True,characteristic=True)
        # K0 = F.ring().gens()[:80]
        # F2 = F.subs(dict(zip(K0,K)))
        #s,t = minisat2(F)
        ce = CNFEncoder(F.ring())
        open("/home/malb/present_%d_%d_%02d.cnf"%(Nr,r,i),"w").write(ce.dimacs_cnf(F))
        #print "%3d"%i, bool(s), "%7.3f"%(t,)
        sys.stdout.flush()


def estimate_running_time(fn):
    for i,line in enumerate(open(fn).readlines()):
        if line.startswith("#"):
            line = line[1:].split(";")
            Nr = int(line[0].strip()[2:])
            r = int(line[1].strip()[2:])
            continue
        line = line.split(";")
        print i
        P0,P1,C0,C1 = map(hexstringtobitstring,line)
        F = present_dc(PRESENT(80,Nr),r=r,P0=P0,C0=C0,C1=C1,return_system=True,characteristic=True)
        
        for vi in range(32)[::-1]:
            T = []
            for j in range(32):
                F2 = copy(F)
                gens = list(F.ring().gens()[:80])
                shuffle(gens)
                for v in gens[:vi+1]:
                    F2 += [v + GF(2).random_element()]
                s,t = minisat2(F2)
                T.append(t)
                print "%3.1f"%(t,),
                sys.stdout.flush()
            print
            print vi,"%7.3f"%(sum(T)/len(T))
        print 



def prob_zero_approx_k(self, F, limit=16, quite=False):
    for j in range(16):
        P0 = self.random_element()
        it = Wang6DiffIter(self)
        DP = it.next()
        P1 = map(GF(2), [P0[i] + DP[i] for i in xrange(len(P0))])
        F = PolynomialSequence([f for f in F.subs(dict(zip(F.ring().gens()[-128:],P0+P1))) if f])
        print F

        g = connection_graph(F)
        C,P = connected_sets(F, g)
        if not quite:
            print "%d connected sets."%len(C)

        res = ZZ(1)

        for i in range(len(C)):
            c,p = C[i],P[i]
            prob = (1-prob_nonzero(p,c,limit=limit))
            res *= prob
            if not quite:
                print i, len(c), len(p), log_b(1/prob,2), log_b(1/res,2)
        print


def b_experiment(self,r):
    Kval = self.random_element(self.Ks)
    B = BooleanPolynomialRing(self.Ks,["K%03d"%i for i in range(self.Ks)])
    K = B.gens()
    s = set()
    for i in range(32):
        P,Kval = present_dc_right_pair(self,r=r,K=Kval,fixed=[0,1,2,3, 8,9,10,11, 48,49,50,51, 56,57,58,59])
        print P[:32]
        l = [

            (K[1] + P[1] + 1) * (K[0] + K[3] + K[29] + P[0] + P[3]),
            (K[2] + P[2]) * (K[0] + K[3] + K[29] + P[0] + P[3]),
            K[1]*K[2] + K[1]*P[2] + K[2]*P[1] + P[1]*P[2] + K[0] + K[1] + K[3] + K[29] + P[0] + P[1] + P[3],

            (K[9] + P[9] + 1)*(K[8] + K[11] + K[31] + P[8] + P[11]),
            (K[10] + P[10])*(K[8] + K[11] + K[31] + P[8] + P[11]),
            K[9]*K[10] + K[9]*P[10] + K[10]*P[9] + P[9]*P[10] + K[8] + K[9] + K[11] + K[31] + P[8] + P[9] + P[11],

            (K[49] + P[49] + 1)*(K[41] + K[48] + K[51] + P[48] + P[51]),
            (K[50] + P[50])*(K[41] + K[48] + K[51] + P[48] + P[51]),
            K[49]*K[50] + K[49]*P[50] + K[50]*P[49] + P[49]*P[50] + K[41] + K[48] + K[49] + K[51] + P[48] + P[49] + P[51],
            
            (K[57] + P[57] + 1)*(K[43] + K[56] + K[59] + P[56] + P[59]),
            (K[58] + P[58])*(K[43] + K[56] + K[59] + P[56] + P[59]),
            K[57]*K[58] + K[57]*P[58] + K[58]*P[57] + P[57]*P[58] + K[43] + K[56] + K[57] + K[59] + P[56] + P[57] + P[59],

            K[5] + K[7] + P[5] + P[7],
            K[6] + K[7] + P[6] + P[7],
            K[53] + K[55] + P[53] + P[55],
            K[54] + K[55] + P[54] + P[55]]
        
        print
        for f in l:
            print f
        print


        s.add(tuple(l))
        print i, len(s)


def p(t, delta, alpha):
    if delta == 0.5:
        return 2**(-alpha*t)
    elif delta < 0.5:
        beta = 2**alpha * (0.5 + delta) - 1
        return ((e**beta)/(1+beta)**(1+beta)) ** (t/2**alpha)
    else:
        raise ValueError

def mbound_assurance(l, delta, alpha):
    for i in range(min(l),max(l)+1):
        print "Pr(2^%d) == %10.9f"%(i-alpha,1 - p(len([c for c in l if c >= i]), delta, alpha))

def mbound(F, s, k=7, t=20, delta=0.5, alpha=1):
    assert(delta <= 0.5)
    assert(alpha >= 1)
    V = list(F.variables())
    R = F.ring()

    numSat = 0


    for i in range(t):
        Qs = []
        for si in range(s):
            shuffle(V)
            f = sum([V[j] for j in range(k)]) + GF(2).random_element()
            Qs.append(f)

        Fs = F + PolynomialSequence(Qs,R)
        sat,t = minisat2(Fs)

        #t = cputime()
        #gb = Fs.groebner_basis(faugere=False,linear_algebra_in_last_block=False,prot=True)
        #t = cputime(t)
        #if gb != [1]:
        #    sat = False
        #else:
        #    sat = True
        sat_str = "-"
        if sat:
            numSat +=1
            sat_str = "+"
        print "%.0f,%s"%(t,sat_str),
        sys.stdout.flush()
            
    print
    print numSat
    if numSat >= t * (0.5 + delta):
        return "LO", s-alpha
    elif numSat <= t * (0.5 - delta):
        return "UP", s+alpha
    else:
        raise ValueError

def c_experiment(self,r, trials=10, precision=5, bound=24, source='rightpairs', bound_only=False, k=7):
    """
    INPUT:

    - ``source`` - either ``rightpairs'', ``present``, ``random'' or ``file:filename''
    """
    if bound_only:
        start = bound -1
    else:
        start = 0

    if source.startswith("file:"):
        fn = source[5:]
        candidates = open(fn).readlines()
        source = 'file'
        line = candidates[0]
        line = line[1:].split(";")
        Nr = int(line[0].strip()[2:])
        r = int(line[1].strip()[2:])
        K = hexstringtobitstring(line[2].strip()[2:].replace(" ","0"),80)
        print bitstringtohexstring(K)
        candidates = candidates[1:]
        
    for trial in range(trials):
        print "%3d"%(trial),

        if source == 'rightpairs': # right pairs for the characteristic
            P, Kval = present_dc_right_pair(self, r=r)
            print bitstringtohexstring(P), bitstringtohexstring(Kval),
            F = present_dc(self, r=r, P0=P, K=Kval, return_system=True,characteristic=False)

        elif source == 'present': # a pair which satisfies our Attack-C filter
            while True:
                P = self.random_element()
                Kval = self.random_element(self.Ks)
                F = present_dc(self, r=r, P0=P, K=Kval, return_system=True,characteristic=False)
                s,t = minisat2(F)
                if s:
                    break
        elif source == 'present2':
            C0,C1 = present_dc_filter_pair(self, r)
            print bitstringtohexstring(C0), bitstringtohexstring(C1),
            F = present_dc(self, r=r, C0=C0, C1=C1, return_system=True, characteristic=False)

        elif source == 'present3': # a pair which satisfies our Attack-C filter
            while True:
                F = present_dc_filter_pair_alt(self, r=r)
                s,t = minisat2(F)
                if s:
                    break

        elif source == 'random': # truly random C and C'
            while True:
                C0 = self.random_element()
                C1 = self.random_element()
                Kval = self.random_element(self.Ks)
                F = present_dc(self, r=r, C0=C0, C1=C1, return_system=True,characteristic=False)
                s,t = minisat2(F)
                if s:
                    break

        elif source == 'file': # use a file as source
            line = candidates[trial]
            line = line.split(";")
            P0,P1, C0,C1 = map(hexstringtobitstring,line)
            assert(self(P0,K) == C0)
            assert(self(P1,K) == C1)
            F = present_dc(self, r=r, C0=C0, C1=C1, return_system=True, characteristic=False)
            s,t = minisat2(F)
            assert(s)
            sys.stdout.flush()

        elif source == 'fixed':
            assert(self.Nr == 18)
            assert(r == 14)
            P = hexstringtobitstring('0538a885efcc1610')
            Kval = hexstringtobitstring('7d22778508f409025443',80)
            F = present_dc(self, r=r, P0=P, K=Kval, return_system=True,characteristic=False)
            sys.stdout.flush()
        else:
            raise ValueError("'%s' is not a valid source of pairs."%(source,))

        F = F.eliminate_linear_variables(maxlength=1)
        V = list(F.variables())
        F_old = copy(F)

        print "--",
        sys.stdout.flush()

        size = []
        for prec in range(precision):
            for i in range(start,bound)[::-1]:
                l = []
                for j in range(i):
                    shuffle(V)
                    f = sum([V[j] for j in range(k)]) + GF(2).random_element()
                    l.append(f)
                F = F_old + l
                s,t = minisat2(F)
                sys.stdout.flush()
                if s:
                    size.append(i+1)
                    break
            if not bound_only:
                print "%2d"%(size[-1],),
            sys.stdout.flush()
        if not bound_only:
            print "min: %3d avg: %3d max: %3d"%(min(size),int(sum(size)/float(len(size))),max(size))
        else:
            print "%5s %7.2f"%(True if s else False, t)
        sys.stdout.flush()
            


def c_experiment_alt(self,r, trials=10, precision=10, source='rightpairs'):
    """
    INPUT:

    - ``source`` - either ``rightpairs'', ``present``, ``random'' or ``file:filename''
    """
    start = 0

    output = open("/home/malb/scratch.txt","w")
    output.close()

    re_l = []
    re_l.append(re.compile(".*restarts              : ([0-9]+) .*$"))
    re_l.append(re.compile(".*conflicts             : ([0-9]+) .*"))
    re_l.append(re.compile(".*decisions             : ([0-9]+) .*"))
    re_l.append(re.compile(".*propagations          : ([0-9]+) .*"))
    re_l.append(re.compile(".*CPU time              : ([0-9\.]+) s$"))
    re_l.append(re.compile(".*conflict literals     : ([0-9]+) \(([0-9\.]+)\% deleted\)$"))
    re_l.append(re.compile(".*Memory used           : ([0-9\.]+) MB$"))


    S = ["c61bf05c2a39a5e4;11ec9f165edf22062eca",
         "0538a885efcc1610;7d22778508f409025443",
         "7ff2f485d5a60d21;e0d9bb128807920c5c08",
         "cf9237a359636e00;076561d04cbd06753ee3",
         "e9cb5753e695ef32;4e7cacde64f8a0997656",
         "adfaf8c6df732dcf;b53f55860b51658567e8",
         "66396df4366faa43;69b319cb18b56d0d2d97",
         "25fd60089c1bdbfc;05f0e912e477c4572bb5",
         "544841b2ce90aa14;1bd3681ceee2ea8dd2e0",
         "530c32754d0d666f;d403c6141c074ef5a629",
         "a283ce93eab76c9d;0d6b63e5dd806b006ef8",
         "a1637f3f6a497c75;b60055368fccfbccff6f",
         "76c6fc0db9e541ac;2e1f1d8b46ee79863c59",
         "7d6f403611cfe536;9544bc1c16dfaddca8ca",
          "0e1fc0e143c74365;f952e6dbc3c89b4764a4"]

    S = ["1eea7d4337962d04;0eb932aeae36e58d1f57",
         "26e3ed68a0f4a62d;218027b0d3579e800321",
         "8f5c5ca1ee230995;c808951cc403fefc016e",
         "a9e16caa327d0361;f6cd9ff87224946ea4db",
         "2c795566739e1b06;bc05d9938ea6e4f7f8fb"]

    S = [s.split(";") for s in S]
    S = [(hexstringtobitstring(a),hexstringtobitstring(b,80)) for (a,b) in S]
        
    for trial in range(trials):
        print "%3d"%(trial),

        if source == 'present': # a pair which satisfies our Attack-C filter
            while True:
                P = self.random_element()
                Kval = self.random_element(self.Ks)
                F = present_dc(self, r=r, P0=P, K=Kval, return_system=True,characteristic=False)
                s,t = minisat2(F)
                if s:
                    break
        elif source == 'file': # use a file as source
            line = candidates[trial]
            line = line.split(";")
            P0,P1, C0,C1 = map(hexstringtobitstring,line)
            assert(self(P0,K) == C0)
            assert(self(P1,K) == C1)
            F = present_dc(self, r=r, C0=C0, C1=C1, return_system=True, characteristic=False)
            s,t = minisat2(F)
            assert(s)
            sys.stdout.flush()
        elif source == 'fixed':
            P0,K = S[trial]
            F = present_dc(self, r=r, P0=P0,K=K, return_system=True, characteristic=False)


        else:
            raise ValueError("'%s' is not a valid source of pairs."%(source,))

        F = F.eliminate_linear_variables(maxlength=1)

        V = list(F.variables())
        F_old = copy(F)

        print "--",
        sys.stdout.flush()
        wt = walltime()

        L = []
        fn = tmp_filename()
        fh = open(fn,"w")
        F.ring().set()
        CE = CNFEncoder(F.ring())
        fh.write(CE.dimacs_cnf(F))
        fh.close()
        on = tmp_filename()

        for prec in range(precision):
            L.append([])
            # call MiniSat
            rands = "-randomize=%d"%randint(0,2**32-1)
            s =  commands.getoutput("/home/malb/Uni/polybori-scripts/cryptominisat -verbosity=1 -polarity-mode=rnd %s %s %s"%(rands,fn,on))

            s = s.splitlines()
            t,cl,c = 0.0, 0, 0

            for line in s:
                for _re in re_l:
                    m = re.match(_re, line)
                    if m:
                        L[-1].append(float(m.groups()[0]))

        os.unlink(on)
        os.unlink(fn)

        wt = walltime(wt)
        output = open("/home/malb/scratch.txt","a")
        output.write("%d "%(-1 if source=='present' else 1))
        for i in range(len(L[0])):
            output.write("%d:%10f "%(i+1,sum([l[i] for l in L])/len(L)))
        output.write("\n")
        output.close()
        sys.stdout.flush()
            


def print_right_pair(P0,K, Nr=18, r=14):
    p = PRESENT(80,18)
    if isinstance(P0,str):
        P0 = hexstringtobitstring(P0.replace(" ",""))
    if isinstance(K,str):
        K = hexstringtobitstring(K.replace(" ",""),80)
        assert(len(K) == 80)
    VS = VectorSpace(GF(2),64)
    P0 = VS(P0)
    D = VS(differential(p, x2=7, x14=7))

    a = lambda x: bitstringtohexstring(x).replace(" ","")

    print "#n: %2d; r: %2d; K: %s; D: %s; log_trials: --"%(Nr,r,a(K),a(D))
    print "%s; %s; %s; %s"%(a(P0), a(P0+D), a(p(P0,K)),  a(p(P0+D,K)))
