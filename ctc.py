"""
Courtois Toy Cipher (CTC) polynomial system generator.

AUTHOR: Martin Albrecht <malb@informatik.uni-bremen.de>
"""

from sage.rings.integer_ring import ZZ
from sage.rings.polynomial.polynomial_ring_constructor import BooleanPolynomialRing_constructor as BooleanPolynomialRing
from sage.crypto.mq.sbox import SBox

from sage.crypto.mq.mpolynomialsystemgenerator import MPolynomialSystemGenerator

class CTC(MPolynomialSystemGenerator):
    """
    Any CTC object may construct several CTC polynomial systems for a
    given B and Nr combination.

    """
    def __init__(self,B=1,Nr=1, **kwargs):
        """
        Return a new MPolynomialSystem constructor for CTC polynomial
        systems.

        INPUT:
            B -- blocksize divided by 3
            Nr -- number of rounds
            postfix -- append string to variable names
                       except key variables (default: '')
            order -- term ordering of ring (default: 'degrevlex')

        EXAMPLE:
            sage: ctc = CTC(B=1,Nr=1)
        """
        self.s = 3
        self.B = B
        self.Bs = int(self.B*self.s)

        if Nr < 1: raise TypeError, "number of rounds must be >= 1"
        self.Nr = int(Nr)

        # we number bits from left to right
        self._sbox = SBox([7,6,0,4,2,5,1,3], big_endian=False)
        self._postfix = kwargs.get("postfix", "")
        self._order = kwargs.get("order", "degrevlex")
        self._base = GF(2)

    def varformatstr(self, name):
        l = str(max([len(str(self.Nr)), len(str(self.Bs -1))]))
        if not name.startswith("K"):
            name += self._postfix
        return name + "%0" + l + "d" + "%0" + l + "d"

    def varstrs(self, name, round):
        s = self.varformatstr(name)
        return [s%(round,i) for i in range(self.Bs) ]

    def vars(self, name, round):
        return [self.R(e) for e in self.varstrs(name,round)]

    def block_order(self):
        """
        Return a blockorder/product order for self where each
        round gets its own block.

        EXAMPLE:
            sage: ctc = CTC(B=1,Nr=2)
            sage: ctc.block_order()
        """
        B = self.B
        Nr = self.Nr
        
        if Nr==1:
            return TermOrder('degrevlex')

        bo = []

        for nr in range(Nr-1):
            bo.append( TermOrder("degrevlex",B*3*4))
        return sum(bo, TermOrder("degrevlex",B*3*4+B*3))
        
    def __repr__(self):
        return "CTC_3,%d,%d"%(self.B,self.Nr)

    def ring(self, order=None, n=1):
        """
        Return a ring which holds all variables of a CTC instance.

        Variables are sorted in reverse order as they appear during
        the encryption process of CTC.

        This ordering of variables was found to be the fastest to
        compute a Groebner basis.

        INPUT:
            order -- term ordering, overrides constructor parameter
                     if not None (default: None)
        """
        if order is None:
            order = self._order
        if order == "block":
            order = self.block_order()
        
        Nr = self.Nr
        Bs = self.Bs
        
        if n == 1:

            var_names = []
            for nr in reversed(range(1,self.Nr+1)):
                var_names += [self.varformatstr("K")%(nr,j) for j in xrange(Bs)]
                var_names += [self.varformatstr("Z")%(nr,j) for j in xrange(Bs)]
                var_names += [self.varformatstr("Y")%(nr,j) for j in xrange(Bs)]
                var_names += [self.varformatstr("X")%(nr,j) for j in xrange(Bs)]
            var_names += [self.varformatstr("K")%(0,j) for j in xrange(Bs)]

        elif n > 1:

            var_names = []
            for nr in reversed(xrange(1,self.Nr+1)):
                var_names += [self.varformatstr("K")%(nr,j) for j in xrange(Bs)]
                for sample in xrange(n):
                    var_names += [self.varformatstr("Z%d"%sample)%(nr,j) for j in xrange(Bs)]
                    var_names += [self.varformatstr("Y%d"%sample)%(nr,j) for j in xrange(Bs)]
                    var_names += [self.varformatstr("X%d"%sample)%(nr,j) for j in xrange(Bs)]
            var_names += [self.varformatstr("K")%(0,j) for j in xrange(Bs)]

        else:
            raise TypeError, "parameter n must be >= 1"

        
        R = BooleanPolynomialRing(len(var_names), var_names, order= order)

        if n == 1:
            self.R = R

        return R

    def random_element(self):
        """
        Return a random list of elements in GF(2) of length self.Bs
        """
        return [self._base.random_element() for _ in range(self.Bs)]

    def __getattr__(self, attr):
        if attr == "R":
            self.R = self.ring()
            return self.R

    def polynomial_system(self, P=None, K=None, **kwds):
        """
        Return a polynomial system for self and P,K

        INPUT:
            P -- plaintext (default: random)
            K -- key (default: random)
            eliminate_keys -- do not introduce new variables
                               for the key schedule
            eliminate_state -- do not introduce new variables
                                for the diffusion layer
            eliminate_all -- eliminate_state and eliminate_keys

        OUTPUT:
            MPolynomialSystem, dictionary of solutions
        """

        if not P:
            P = self.random_element()
        if not K:
            K = self.random_element()

        C = self(P,K)

        Bs = self.Bs
        s = self.s
        R = self.ring()
        self.R = R

        add  = self.key_addition_polynomials(self.vars("X",1), P, self.vars("K",0))
        add += self.field_polynomials(self.vars("X",1))
        rounds = [mq.MPolynomialRoundSystem(R,add)]

        for i in range(1,self.Nr+1):
            Xi = self.vars("X",i)
            Yi = self.vars("Y",i)
            Zi = self.vars("Z",i)
            Ki = self.vars("K",i)

            # sbox
            sbox = self.sbox_polynomials(Xi, Yi)
            sbox += self.field_polynomials(Yi)

            if kwds.get("lc",False) is True:
                sbox.append(Xi[2] + Yi[0])

            # diffusion layer
            if kwds.get("eliminate_state",False) or kwds.get("eliminate_all",False):
                lin = []
                Zi = self.diffusion_variable_substitution(Yi)
            else:
                lin  = self.diffusion_polynomials(Zi, Yi)
                lin += self.field_polynomials(Zi)

            # key schedule equations
            if kwds.get("eliminate_keys",False) or kwds.get("eliminate_all",False):
                key = []
                Ki = self.key_schedule_variable_substitution(self.vars("K",0), i)
            else:
                key = self.key_schedule_polynomials(self.vars("K",0),Ki,i)
                key += self.field_polynomials(Ki)

            # key addition equations
            if i < self.Nr:
                add = self.key_addition_polynomials(self.vars("X",i+1), Zi, Ki)
                add += self.field_polynomials(self.vars("X",i+1))
            else:
                add = self.key_addition_polynomials(C, Zi, Ki)

            rounds.append(mq.MPolynomialRoundSystem(R, sbox + lin + add + key))

        solution = dict(zip(self.vars("K",0),K))

        F = mq.MPolynomialSystem(R, rounds)
        return F, solution

    def sbox_polynomials(self, X, Y):
        """
        Return a list of polynomials describing the sbox/substitution
        layer for the input variables X and the output variables Y.

        INPUT:
            X -- list of input variables of length self.Bs
            Y -- list of output variables of length self.Bs
        """
        s = self.s
        return sum([ self.single_sbox_polynomials(X[j:j+s], Y[j:j+s])
                     for j in range(0,self.Bs,s)],[])

    def single_sbox_polynomials(self,x,y):
        """
        Return a list of polynomials describing one sbox for the
        input variables x and output variables y.

        INPUT:
            x -- list of length 3 with add-able elements
            y -- list of length 3 with add-able elements
        """
        x1,x2,x3 = x[0],x[1],x[2]
        y1,y2,y3 = y[0],y[1],y[2]

        l = [ x1*x2 + y1 + x3 + x2 + x1 + 1,
              x1*x3 + y2 + x2 + 1,
              x1*y1 + y2 + x2 + 1,
              x1*y2 + y2 + y1 + x3,
              x2*x3 + y3 + y2 + y1 + x2 + x1 + 1,
              x2*y1 + y3 + y2 + y1 + x2 + x1 + 1,
              x2*y2 + x1*y3 + x1,
              x2*y3 + x1*y3 + y1 + x3 + x2 + 1,
              x3*y1 + x1*y3 + y3 + y1,
              x3*y2 + y3 + y1 + x3 + x1,
              x3*y3 + x1*y3 + y2 + x2 + x1 + 1,
              y1*y2 + y3 + x1,
              y1*y3 + y3 + y2 + x2 + x1 + 1,
              y2*y3 + y3 + y2 + y1 + x3 + x1
              ]

##         l = [          	
##             y3 + x1*x2 + x1*x3 + x2*x3 + x2 + x3 + 1,
##             y2 + x1*x3 + x2 + 1,
##             y1 + x1*x2 + x1 + x2 + x3 + 1
##             ]

        return l

    def diffusion_variable_substitution(self, y):
        """
        Return a list of length self.Bs with linear combinations of
        the elements in the list y (of length self.Bs) matching the
        diffusion layer of CTC.

        INPUT:
            y -- list of length self.Bs
        """
        Bs = self.Bs
        z = [0]*Bs
        for j in xrange(1,Bs):
            z[ (j*1987+257) % Bs ] = y[ j ]  + y[ (j+137) % Bs ]
        z[ 257 % Bs ] = y[ 0 ]
        return z

    def diffusion_polynomials(self,z,y):
        """
        The diffusion part D of the cipher is defined as follows:

         $Z_{i,(257 mod Bs)} = Y_{i,0} for all i = 1 ... Nr$
         $Z_{i,j*1987+257 mod Bs} = Y_{i,j} + Y_{i,j+137 mod Bs}$ for $j != 0$ and all $i$

         INPUT:
             z -- output variables
             y -- input variables
         
        """
        Bs = self.Bs
        
        l = [ z[ (j*1987+257) % Bs ] + y[ j ]  + y[ (j+137) % Bs ] for j in range(1,Bs)]
        l += [ z[ 257 % Bs ] + y[ 0 ] ]
        return l

    def key_addition_polynomials(self,X,Z,K):
        """
        Return key addition polynomials.

          $X_{i+1,j} = Z_{i,j} + K_{i,j} for all i = 0 ... Nr$

        INPUT:
            X -- output variables
            Z -- input state variables
            K -- key variables
        """
        return [ self.R(X[j]) + self.R(Z[j]) + self.R(K[j]) for j in xrange(self.Bs)]

    def key_schedule_variable_substitution(self, K0, i):
        """
        """
        Bs = self.Bs
        return [K0[ (j+i) % Bs] for j in xrange(Bs) ]

    def key_schedule_polynomials(self,K0,Ki,i):
        """
        Return key schedule polynomials for round $i$.
        
        'There is no S-Boxes in the key schedule and the derived key in
        round i, K_i is obtained from he secret key K_0, by a very
        simple permutation of wires:

         $K_{i,j} = K_{0,(j+i % Bs)}$' 


        INPUT:
            K0 -- variables K_0
            Ki -- variables K_i
            i -- round
        """
        Bs = self.Bs
        
        return [ Ki[j] + K0[ (j+i) % Bs] for j in xrange(Bs) ]

    def field_polynomials(self, X):
        """
        Return list x_i^2 + x_i for all x_i in X.

        INPUT:
            X -- list
        """
        return [x**2 + x for x in X]

    def sbox(self):
        """
        Return SBox object.
        """
        return self._sbox

    def __call__(self,P,K):
        """
        encrypt plaintext $p$ with key $k$.

        INPUT:
            P -- plaintext
            K -- key
        """

        k = K

        def key_schedule(i):
            K = [0]*self.Bs
            for j in range(self.Bs):
                K[j] = k[int((j+i))%self.Bs]

            return K
        
        Z = [ self._base(e) for e in P ]
        for i in range(self.Nr):
            K = key_schedule(i)
            X = self.add( Z, K )
            Y = self.substitute( X )
            Z = self.permute(Y)

        K = key_schedule(self.Nr)
        X = [ Z[j] + K[j] for j in range(self.Bs) ]
        
        return X

    def add(self, X, Y):
        """
        Return list of pairwise sums of elements in X and Y.

        INPUT:
            X -- list
            Y -- list
        """
        return [ X[j] + Y[j] for j in range(self.Bs) ]

    def substitute(self, X):
        s = self.s
        sbox = self.sbox()
        return sum([ sbox(X[j:j+s]) for j in range(0,self.Bs,s) ],[])

    def permute(self, Y):
        Bs = self.Bs

        Z = [0]*self.Bs

        Z[ 257 % Bs ] = Y[ 0 ]

        for j in range(1,Bs):
            Z[ (j*1987+257) % Bs ] = Y[ j ]  + Y[ (j+137) % Bs ]

        return Z

def ctc_big_system(self, samples=1, K=None):
    P = self.ring(n=samples)

    if K is None:
        K = self.random_element()
    l = []
    j = []
    
    for sample in xrange(samples):
        sr = CTC(B=self.B, Nr=self.Nr, postfix=str(sample))
        F,s = sr.polynomial_system(K=K)
        j.append(F)
        l.append([P(str(e)) for e in F.gens() ])

    solution = dict(zip(map(lambda x: P(str(x)),self.vars("K",0)),K))
    
    return mq.MPolynomialSystem(P,l), solution, j

def ctc_dc_system(self, P=None, K=None, **kwds):
    """
    Return an equation system for P,K such that two plaintexts P and P' are
    encrypted with the same key K. P and P' are related by the difference
    [1,0,...,0].
    
    INPUT:
        P -- plaintext (default: None)
        K -- key (default: None)
        kwds -- passed on to CTC.polynomial_system
    """
    R = self.ring(n=2)

    if K is None:
        K = self.random_element()
    l = []
    j = []

    diff = [1,0,0] + [0,0,0]*(self.B-1)

    if P is None:
        plaintext1 = self.random_element()
    else:
        plaintext1 = P

    plaintext2 = [plaintext1[i] + diff[i] for i in range(len(plaintext1))]

    ctc = CTC(B=self.B, Nr=self.Nr, postfix=str(0))
    F,s = ctc.polynomial_system(P=plaintext1, K=K, **kwds)
    j.append(F)
    l.append([R(str(e)) for e in F.gens() ])

    ctc = CTC(B=self.B, Nr=self.Nr, postfix=str(1))
    F,s = ctc.polynomial_system(P=plaintext2, K=K, **kwds)
    j.append(F)
    l.append([R(str(e)) for e in F.gens() ])

    if kwds.get("dc_level",0) >= 1:
        l.append([R(self.varformatstr("X0")%(1,i)) + R(self.varformatstr("X1")%(1,i)) + diff[i] for i in range(self.Bs)])
    if kwds.get("dc_level",0) >= 2:
        l.append([R(self.varformatstr("Y0")%(1,i)) + R(self.varformatstr("Y1")%(1,i)) + diff[i] for i in range(0,self.Bs)])
    if kwds.get("dc_level",0) >= 3:
        diff = self.permute(diff)
        l.append([R(self.varformatstr("X0")%(2,i)) + R(self.varformatstr("X1")%(2,i)) + diff[i] for i in range(0,self.Bs)])

    solution = dict(zip(map(lambda x: R(str(x)),self.vars("K",0)),K))
    
    return mq.MPolynomialSystem(R,l), solution, j

def ctc_dc(self, P=None, K=None, timeout=10, **kwds):
    R = self.ring(n=2)

    l = []

    if K is None:
        K = self.random_element()

    # chosen difference
    diff = [1,0,0] + [0,0,0]*(self.B-1)

    # given plaintext
    if P is None:
        plaintext1 = self.random_element()
    else:
        plaintext1 = P

    # chosen related plaintext
    plaintext2 = [plaintext1[i] + diff[i] for i in xrange(len(plaintext1))]

    # create first polynomial system
    ctc = CTC(B=self.B, Nr=self.Nr, postfix=str(0))
    F,s = ctc.polynomial_system(P=plaintext1, K=K, **kwds)
    l.append([R(str(e)) for e in F.gens() ])

    # create second polynomial system
    ctc = CTC(B=self.B, Nr=self.Nr, postfix=str(1))
    F,s = ctc.polynomial_system(P=plaintext2, K=K, **kwds)
    l.append([R(str(e)) for e in F.gens() ])

    # allways true due to chosen differential
    l.append([R(self.varformatstr("X0")%(1,i)) + R(self.varformatstr("X1")%(1,i)) + diff[i] for i in range(self.Bs)])

    L = list(l) # copy
    
    solution = dict(zip(map(lambda x: R(str(x)),self.vars("K",0)),K))


    differentials = []
    S = self.sbox()
    M = S.difference_distribution_matrix()
    for i in srange(M.nrows()):
        if M[1,i]:
            differentials.append( S.to_bits(i) + [0,0,0]*(self.B-1) )

    singular.option("redTail")
    singular.option("redSB")

    for D in differentials:
        l = list(L)
        l.append([R(self.varformatstr("Y0")%(1,i)) + R(self.varformatstr("Y1")%(1,i)) + D[i] for i in range(0,self.Bs)])
        F = mq.MPolynomialSystem(R,l)

        #rt = cputime()
        #rb = F.ideal().reduced_basis()
        #rt = cputime(rt)
        rt = 0.0
        try:
            alarm(timeout) # arm alarm
            gt = singular.cputime()
            #gb = Ideal(rb).groebner_basis()
            gb = F.ideal().groebner_basis()
            gt = singular.cputime(gt)
            alarm(0) # disable alarm again
            if gb != [1]:
                break
            print gb, rt, gt
        except TypeError, KeyboardInterrupt:
            print "candidate found"
            break

    k0 = R("K0000")
    k1 = R("K0001")
    k2 = R("K0002")

    d0,d1,d2 = D[:3]
    p0,p1,p2 = plaintext1[:3]
    p0bar,p1bar,p2bar = plaintext2[:3]

    S0  = lambda x1,x2,x3: x1*x2 + x3 + x2 + x1 + 1
    S1  = lambda x1,x2,x3: x1*x3 + x2 + 1
    S2  = lambda x1,x2,x3: x2*x3 + x1*x3 + x1*x2 + x3 + x2 + 1

    Fbar = [ d0 + S0(p0+k0,p1+k1,p2+k2) + S0(p0bar+k0,p1bar+k1,p2bar+k2),
             d1 + S1(p0+k0,p1+k1,p2+k2) + S1(p0bar+k0,p1bar+k1,p2bar+k2),
             d2 + S2(p0+k0,p1+k1,p2+k2) + S2(p0bar+k0,p1bar+k1,p2bar+k2)]

    return Fbar,K[:3]
    


def ctc_dc2(Nr=2, K=None, timeout=10, **kwds):
    self = CTC(20,Nr=Nr)
    R = self.ring(n=2)

    if K is None:
        K = self.random_element()

    solution = dict(zip(map(lambda x: R(str(x)),self.vars("K",0)),K))

    singular.option("redTail")
    singular.option("redSB")

    DX1 = [0,0,0]*5 + [1,0,0] + [0,0,0]*(self.B-5-1)

    while True:
        try:
            l = []
            plaintext1 = self.random_element()
            plaintext2 = [plaintext1[i] + DX1[i] for i in xrange(len(plaintext1))]

            # create first polynomial system
            ctc = CTC(B=self.B, Nr=self.Nr, postfix=str(0))
            F,s = ctc.polynomial_system(P=plaintext1, K=K, **kwds)
            l.append([R(str(e)) for e in F.gens() ])

            # create second polynomial system
            ctc = CTC(B=self.B, Nr=self.Nr, postfix=str(1))
            F,s = ctc.polynomial_system(P=plaintext2, K=K, **kwds)
            l.append([R(str(e)) for e in F.gens() ])

            # allways true due to chosen differential
            l.append([R(self.varformatstr("X0")%(1,i)) + R(self.varformatstr("X1")%(1,i)) + DX1[i] for i in range(self.Bs)])

            # output differential, probability 1/4
            DY1 = [0,0,0]*5 + [0,0,1] + [0,0,0]*(self.B-5-1)
            l.append([R(self.varformatstr("Y0")%(1,i)) + R(self.varformatstr("Y1")%(1,i)) + DY1[i] for i in range(0,self.Bs)])

            # true, given the last differential holds
            DX2 = [0,0,0]*5 + [0,1,0] + [0,0,0]*(self.B-5-1)
            l.append([R(self.varformatstr("X0")%(2,i)) + R(self.varformatstr("X1")%(2,i)) + DX2[i] for i in range(self.Bs)])

            # true, with probability 1/4
            DY2 = [0,0,0]*5 + [0,1,0] + [0,0,0]*(self.B-5-1)
            l.append([R(self.varformatstr("Y0")%(2,i)) + R(self.varformatstr("Y1")%(2,i)) + DY2[i] for i in range(self.Bs)])

            F = mq.MPolynomialSystem(R,l)
            alarm(timeout)
            t = singular.cputime()
            gb = F.groebner_basis()
            t = singular.cputime(t)
            if gb != [1]:
                break
            print gb, t
        except TypeError:
            print "candidate found"
            break

    p0,p1,p2 = plaintext1[5*3:5*3+3]
    p0bar,p1bar,p2bar = plaintext2[5*3:5*3+3]
    d0,d1,d2 = DY1[5*3:5*3+3]
    k0,k1,k2 = R(str(self.varformatstr("K")%(0,15))), R(str(self.varformatstr("K")%(0,16))),R((self.varformatstr("K")%(0,17)))

    S0  = lambda x1,x2,x3: x1*x2 + x3 + x2 + x1 + 1
    S1  = lambda x1,x2,x3: x1*x3 + x2 + 1
    S2  = lambda x1,x2,x3: x2*x3 + x1*x3 + x1*x2 + x3 + x2 + 1

    Fbar = [ d0 + S0(p0+k0,p1+k1,p2+k2) + S0(p0bar+k0,p1bar+k1,p2bar+k2),
             d1 + S1(p0+k0,p1+k1,p2+k2) + S1(p0bar+k0,p1bar+k1,p2bar+k2),
             d2 + S2(p0+k0,p1+k1,p2+k2) + S2(p0bar+k0,p1bar+k1,p2bar+k2)]

    return Fbar,K[5*3:5*3+3]
    
