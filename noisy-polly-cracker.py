# -*- coding: utf-8 -*-
"""
Reference implementation of Symmetric Polly Cracker with Noise for d=1, b=2.
"""

import itertools

class SPCN:
    def __init__(self, secpar, mu=1):
        self.secpar = secpar
        q, alpha, n = SPCN.parameters(secpar, mu)

        self.mu = mu
        self.q = q
        self.K = GF(self.q)

        self.n = n
        self.alpha = alpha

        self.P = PolynomialRing(self.K, self.n, 'x')
        self.gb = self.gen_gb()

    @staticmethod
    def parameters(secpar, mu):
        secpar = ZZ(secpar)
        mu = ZZ(mu)
        N = var('N',domain='positive')

        delta = 2**(1.8/(secpar + 110))
        alpha = ( (secpar**(0.5-mu))/log(secpar,2)**2 ).n()
        q = random_prime(lbound=secpar**(1 + mu), n=2*secpar**(1 + mu))
        k = sqrt(2*log(2**20)).n()
        m = sqrt(N*log(q)/log(delta))

        est_security_q = (1/q < 2**(-2*sqrt(N*log(q,2)*log(delta,2))))
        est_security_practice = 1/alpha <= delta**m * q**(N/m)
        est_security_theory = alpha*q > 2*sqrt(N)
        est_correctness = k*alpha*q < q**(1/(mu))

        def estimate(mu, N):
           c = bool(est_correctness)
           sp = bool(est_security_practice.subs(N=N))
           st = bool(est_security_theory.subs(N=N))
           sq = bool(est_security_q.subs(N=N))
           return c,sq,sp,st

        for N in range(1,10000):
            c,sq,sp,st = estimate(mu,N)
            if c and sq and sp:
                for n in range(ceil(sqrt(N)),N):
                    if (alpha*q)**n > 2**secpar:
                        c,sq,sp,st = estimate(mu, binomial(n+2,2))
                        return q, alpha, n

    @staticmethod
    def mod(x,p):
        #Sage normalises between [0,p), but we want (-p/2,p/2]
        x = int(x) % p
        if x > p//2:
            return x - p
        else:
            return x

    def chi(self):
        return self.K(round(gauss(0,self.q*self.alpha)))

    def gen_gb(self):
        monomials = list(MonomialIterator(self.P, (0,1,2)))

        gb = []
        n, X = self.P.ngens(), self.P.gens()
        K, P = self.K, self.P

        for i in range(n):
            f = P.gen(i) + K.random_element()
            gb.append(f)
        gb = Ideal(gb)
        return gb

    def __repr__(self):
        s_str = "\n                       ".join(str(f) for f in self.gb.gens())
        return """Symmetric Polly Cracker with Noise
                field: %s
         secpar, n, m: %d, %d, %d
           secret key: %s
"""%(self.K,self.secpar,self.P.ngens(),self.mu,s_str)

    def sample(self, noise=True, ideal=None):
        P, n = self.P, self.P.ngens()
        f = P.random_element(degree=2,terms=binomial(n + 2, 2))
        if ideal is None:
            G = self.gb
        else:
            G = ideal

        f -= f.reduce(G)
        if noise:
            f += 2*self.chi()
        return f

    def encrypt(self, m):
        f = self.sample()
        return f + m

    def decrypt(self, c, ideal=None):
        if ideal is None:
            G = self.gb
        else:
            G = ideal
        return SPCN.mod(c.reduce(G),self.q) % 2

    @staticmethod
    def add(c0,c1):
        return c0 + c1

    @staticmethod
    def mul(c0,c1, T=None):
        c = c0*c1
        if T is not None:
            return SPCN.reencrypt(c, T)
        else:
            return c

    def reencryption_map(self, G):
        T = dict()
        for m in MonomialIterator(self.P,(0,1,2)):
            if G is self.gb:
                M = m
            else:
                M = m.reduce(G)
            for j in range(ceil(log_b(self.q,2))):
                F = self.sample() + 2**j * M
                T[2**j * m] = F
        return T

    @staticmethod
    def reencrypt(f, T):
        P = parent(f)
        q = P.base_ring().order()

        F = 0
        for c,m in f:
            c = int(c)
            M = 0
            for j in range(ceil(log_b(q,2))):
                if 2**j & c:
                    M += T[2**j * m]
            F += M
        return F

class MonomialIterator(SageObject):
    def __init__(self, P, degrees):
        from sage.rings.polynomial.pbori import BooleanPolynomialRing

        self.P = P
        try:
            _ = iter(degrees)
        except TypeError:
            degrees = [degrees]

        self.degree = degrees
        self.it = []

        for degree in degrees:
            if isinstance(P, BooleanPolynomialRing):
                if degree > P.ngens():
                    raise ValueError("Degree must be <= number of variables.")
                self.it.append( Permutations([1 for _ in range(degree)] \
                                                 + [0 for _ in range(P.ngens()-degree)]) )
            else:
                self.it.append( IntegerVectors(degree,P.ngens()) )

    def __iter__(self):
        for exp in itertools.chain(*self.it):
            yield prod(self.P.gen(i)**exp[i] for i in range(self.P.ngens()))

        raise StopIteration


def print_table():

    def print_instance(secpar, mu):
        spcn = SPCN(secpar, mu)
        logq = log(spcn.q,2.0)
        sk = logq*spcn.n
        N = binomial(spcn.n + 2, spcn.n)
        enc = N*logq
        pk = 2*N*logq * enc

        print r"%3d & %1d & %2d & %4d & %6.2f & %6.2f & %6.2f & %6.2f & %6.2f\\"%(spcn.secpar, spcn.mu, spcn.n, N, log(spcn.alpha,2.0), logq, log(sk,2.0), log(enc,2.0), log(pk,2.0))

    print r"\hline"
    for k in range(1,6):
        print_instance(40, k)
    print r"\hline"
    for k in range(1,6):
        print_instance(80, k)
    print r"\hline"

    for k in range(1,6):
        print_instance(128, k)
    print r"\hline"
