"""A rough quick implementation of

    Marten van Dijkm Craig Gentry, Shai Halevi, Vinod Vaikuntanathan: Fully homomorphic encryption
    over the integers. http://eprint.iacr.org/2009/616.pdf
"""

class IHE:
    def __init__(self, l, rho=None, rhoprime=None, eta=None, gamma=None, tau=None):
        self.l = l
        if rho is None:
            rho = l
        self.rho = rho
        if rhoprime is None:
            rhoprime = 2*l
        self.rhoprime = rhoprime
        if eta is None:
            eta = l**2
        self.eta = eta
        if gamma is None:
            gamma = l**5
        self.gamma = gamma
        self.tau = self.gamma + self.l if tau is None else tau
        
        p = ZZ.random_element(2**(self.eta-1),2**self.eta)
        if p%2 == 0:
            p += 1
        self.p = p

        while True:
            x = []
            for i in xrange(self.tau+1):
                x.append( self.sample() )
                assert( IHE.mod(2*x[-1],p)%2 ==0 )
            x.sort(reverse=True)
            assert(x[0] > x[1])
            if x[0]%2 == 1 and IHE.mod(x[0],p)%2 == 0:
                break

        self.x = x
        self.pk = self.x

    key_gen = __init__

    def sample(self, gamma=None, rho=None, p=None):
        if gamma is None:
            gamma = self.gamma
        if rho is None:
            rho = self.rho
        if p is None:
            p = self.p
        q = ZZ.random_element(0, ceil(2**gamma/p))
        r = ZZ.random_element(-(2**rho) + 1, 2**rho)
        return p*q + r


    @staticmethod
    def mod(x,p):
        #Sage normalises between [0,p), but we need (-p/2,p/2]
        x = x % p
        if x > p//2:
            return x - p
        else:
            return x

    def encrypt(self, m):
        S = range(1,self.tau+1)
        shuffle(S)
        S = S[:10] # need to do something proper here
        
        r = ZZ.random_element(-(2**self.rhoprime)+1,2**self.rhoprime)

        c = m%2 + 2*r + 2*sum(self.x[i] for i in S)
        c = IHE.mod(c, self.x[0])
        return c

    def decrypt(self, c):
        c = IHE.mod(c, self.p)
        return c%2



    

