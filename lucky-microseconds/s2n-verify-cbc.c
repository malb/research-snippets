#include "s2n/api/s2n.h"
#include "s2n/tls/s2n_record.h"
#include "s2n/utils/s2n_random.h"

#define TRIALS (1UL<<10)

#include <string.h>
#include <sys/time.h>
#include <unistd.h>

inline static uint64_t rdtsc(){
    unsigned int bot, top;
    __asm__ __volatile__ ("rdtsc" : "=a" (bot), "=d" (top));
    return ((uint64_t) top << 32) | bot;
}


int main(int argc, char *argv[]) {
  struct s2n_connection *conn;
  uint8_t mac_key[] = "sample mac key";

  if (argc != 3){
    fprintf(stderr, "Please provide number of bytes and either 'sha1', 'sha256' or 'md5'.\n");
    exit(-1);
  }
  const long length  = atol(argv[1]);
  s2n_hmac_algorithm mac_alg;
  if (strcmp(argv[2],"sha1")==0)
    mac_alg = S2N_HMAC_SHA1;
  else if (strcmp(argv[2],"sha256")==0)
    mac_alg = S2N_HMAC_SHA256;
  else if (strcmp(argv[2],"md5")==0)
    mac_alg = S2N_HMAC_MD5;
  else {
    fprintf(stderr, "Please either 'sha1', 'sha256' or 'md5', as second parameter.\n");
    exit(-1);
  }

  uint8_t random_data[length];
  struct s2n_hmac_state check_mac;
  uint64_t times[256];

  s2n_init();
  conn = s2n_connection_new(S2N_SERVER);
  conn->actual_protocol_version = S2N_TLS12;

  struct s2n_blob decrypted = { .data = random_data, .size = sizeof(random_data) };
  s2n_get_urandom_data(&decrypted);


  for(uint16_t padding_length = 0; padding_length<256; padding_length++) {
    times[padding_length] = 0;
  }

  for(uint32_t i=0; i<TRIALS; i++) {
    for(uint16_t padding_length = 0; padding_length<256; padding_length++) {
      s2n_hmac_init(&check_mac, mac_alg, mac_key, sizeof(mac_key));
      decrypted.data[decrypted.size-1] = (uint8_t)padding_length;
#if 1
      uint64_t t = rdtsc();
      s2n_verify_cbc(conn, &check_mac, &decrypted);
#else
      /* hard code the relevant steps to confirm where time difference comes from */
      uint8_t padding_length = decrypted.data[decrypted.size - 1];

      int payload_and_padding_size = decrypted.size - 32;
      int payload_length = payload_and_padding_size - padding_length - 1;
      if (payload_length < 0) {
        payload_length = 0;
      }

      /* Update the MAC */
      struct s2n_hmac_state copy;
      s2n_hmac_update(&check_mac, decrypted.data, payload_length);
      s2n_hmac_copy(&copy, &check_mac);

      /* Check the MAC */
      uint8_t check_digest[S2N_MAX_DIGEST_LEN];
      s2n_hmac_digest(&check_mac, check_digest, 32);
      uint64_t t = rdtsc();
      s2n_hmac_update(&copy, decrypted.data + payload_length + 32,
                      decrypted.size - payload_length - 32 - 1);
#endif
      times[padding_length] += rdtsc() - t;
      s2n_hmac_reset(&check_mac);
    }
  }
  double min = 1000000000000.0;
  double max = 0;
  for(uint16_t padding_length = 0; padding_length<256; padding_length++) {
    if (times[padding_length]/(double)TRIALS > max)
      max = times[padding_length]/(double)TRIALS;
    else if (times[padding_length]/(double)TRIALS < min)
      min = times[padding_length]/(double)TRIALS;
  }
  printf("min: %f, max: %f\n",min,max);

  for(uint16_t padding_length = 0; padding_length<256; padding_length++) {
    printf("padding_length: 0x%02x, time: %8.2f\n",
           padding_length, times[padding_length]/(double)TRIALS);
  }
  s2n_connection_free(conn);
  s2n_cleanup();
  return 0;
}
