/**
   How to run:

   $time taskset -c 0-15 parallel --gnu -j 500 --delay 0.1 \
         ./s2n-time-random-delay -p 100 -t 17 -d 10000 -n ::: `seq -w 0 2047`

   - `time` overall time
   - `tastset -c 0-15` to restrict to 16 cores
   - `parallel --gnu -j 500 --delay 0.1` run 500 experiments in parallel,
                                         start then with a delay of 0.1 seconds
                                         of each other
   - `./s2n-time-random-delay -p 100 -t 17 -d 10000 -n` run for padding_length 100,
                                                        run 2^17 trials, max delay 10000,
                                                        with prefix -n
   - `::: `seq -w 0 2047`` run 2048 experiments
 */

#include "s2n/api/s2n.h"
#include "s2n/tls/s2n_record.h"
#include "s2n/utils/s2n_random.h"

#define DEFAULT_NTRIALS (20)
#define DEFAULT_PADDING_LENGTH 0x00
#define DEFAULT_DELAY 1000
#define DEFAULT_POSTIFX "00"
#define DEFAULT_MOD 3300

#include <math.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>
#include <float.h>

static inline void print_help_and_exit(const char *name) {
  printf("####################################################################\n");
  printf(" %s\n",name);
  printf("####################################################################\n");
  printf("-t   log of the number of trials to do > 1 (default: %d)\n", DEFAULT_NTRIALS);
  printf("-p   padding value to test (default: 0x%02x)\n", DEFAULT_PADDING_LENGTH);
  printf("-d   maximum delay added (default: %d)", DEFAULT_DELAY);
  printf("-m   mod (default: %d)", DEFAULT_MOD);
  printf("-n   filename postfix (default: \"00\")");
  abort();
}

struct _cmdline_params_struct{
  int32_t ntrials;
  uint8_t padding_length;
  uint32_t delay;
  uint32_t mod;
  const char *postfix;
};

typedef struct _cmdline_params_struct cmdline_params_t[1];


static inline void parse_cmdline(cmdline_params_t params, int argc,
                                 char *argv[], const char *name) {
  params->ntrials   =  DEFAULT_NTRIALS;
  params->padding_length = DEFAULT_PADDING_LENGTH;
  params->delay = DEFAULT_DELAY;
  params->postfix = DEFAULT_POSTIFX;
  params->mod = DEFAULT_MOD;

  int c;
  while ((c = getopt(argc, argv, "t:p:d:n:m:")) != -1) {
    switch(c) {
    case 'p':
      params->padding_length = (uint8_t)atoi(optarg);
      break;
    case 't':
      params->ntrials = atoi(optarg);
      break;
    case 'd':
      params->delay = atoi(optarg);
      break;
    case 'n':
      params->postfix = optarg;
      break;
    case 'm':
      params->mod = atoi(optarg);
      break;
    case ':':  /* without operand */
      print_help_and_exit(name);
    case '?':
      print_help_and_exit(name);
    }
  }

  if (params->ntrials<1)
    print_help_and_exit(name);
}


static inline uint64_t rdtsc(){
    unsigned int bot, top;
    __asm__ __volatile__ ("rdtsc" : "=a" (bot), "=d" (top));
    return ((uint64_t) top << 32) | bot;
}

static inline void print_progress(int32_t count, int32_t total,
                                  double acc_vrfy, double acc_wipe,
                                  double acc_rand, double acc_slep,
                                  double acc_uslp,
                                  int32_t acc_count) {

  printf("\rdone: %6.2f%%, log(#samples): %6.2f, μ_vrfy: %8.3f, μ_wipe: %8.3f, μ_rand: %8.3f, μ_slep: %8.3f, μ_uslep: %8.3f",
         100*((double)count)/((double)total),
         log2(count),
         acc_vrfy/acc_count, acc_wipe/acc_count, acc_rand/acc_count,
         acc_slep/acc_count, acc_uslp/acc_count);
  fflush(0);
}

int main(int argc, char *argv[]) {
  s2n_init();
  const uint8_t mac_key[] = "sample mac key";

  cmdline_params_t params;
  parse_cmdline(params, argc, argv, "Random Delay Test");

  printf("padding length: 0x%02x, #trials: 1<<%d, max delay: %d, mod %d, postfix: %s\n",
         params->padding_length, params->ntrials, params->delay, params->mod, params->postfix);

  /* We are hardcoding SHA-256 here */
  const long length  = 80+13;
  const s2n_hmac_algorithm mac_alg = S2N_HMAC_SHA256;

  uint8_t random_data[length];
  struct s2n_hmac_state check_mac;

  struct s2n_connection *conn = s2n_connection_new(S2N_SERVER);
  conn->actual_protocol_version = S2N_TLS12;

  struct s2n_blob decrypted = { .data = random_data, .size = sizeof(random_data) };
  s2n_get_urandom_data(&decrypted);

  char fn[256];
  snprintf(fn, 256, "0x%02x-%d-%d-%d-%s.txt",
           params->padding_length, params->ntrials, params->delay, params->mod, params->postfix);
  FILE *fh = fopen(fn,"w");

  double acc_vrfy = 0;
  double acc_wipe = 0;
  double acc_rand = 0;
  double acc_slep = 0;
  double acc_uslp = 0;
  int32_t acc_count = 0;

  const uint64_t ntrials = (1ULL)<<params->ntrials;
  for(size_t i=0; i<ntrials; i++) {
    s2n_hmac_init(&check_mac, mac_alg, mac_key, sizeof(mac_key));
    decrypted.data[decrypted.size-1] = params->padding_length;

    if(i%((1ULL)<<10) == 0)
      print_progress(i+1, ntrials, acc_vrfy, acc_wipe, acc_rand, acc_slep, acc_uslp, acc_count);

    const uint64_t t_start = rdtsc();
    s2n_verify_cbc(conn, &check_mac, &decrypted);
    const uint64_t t0 = rdtsc();
    s2n_stuffer_wipe(&conn->in);
    const uint64_t t1 = rdtsc();
    /* int delay = s2n_public_random(1000 + 10000000); */
    int delay = s2n_public_random(params->delay);
    const uint64_t t2 = rdtsc();
    sleep(delay / 1000000);
    const uint64_t t3 = rdtsc();
    usleep(delay % 1000000);
    const uint64_t t4 = rdtsc();

    const uint64_t t_vrfy = t0 - t_start;
    const uint64_t t_wipe = t1 - t_start;
    const uint64_t t_rand = t2 - t_start;
    const uint64_t t_slep = t3 - t_start;
    const uint64_t t_uslp = t4 - t_start;
    const uint64_t t_totl = t_uslp;

    if (t_totl < (params->delay * params->mod)/2) {
      acc_vrfy += (t_vrfy % params->mod);
      acc_wipe += (t_wipe % params->mod);
      acc_rand += (t_rand % params->mod);
      acc_slep += (t_slep % params->mod);
      acc_uslp += (t_uslp % params->mod);
      acc_count  += 1;
    }

    fprintf(fh, "%lu %lu %lu %lu %lu %lu %lu\n",
            t_totl, t_vrfy, t_wipe, t_rand, t_slep, t_uslp, t_totl % params->mod);

    s2n_hmac_reset(&check_mac);
  }
  print_progress(ntrials, ntrials, acc_vrfy, acc_wipe, acc_rand, acc_slep, acc_uslp, acc_count);
  printf("\n");
  fprintf(fh, "\n");
  fclose(fh);
  s2n_connection_free(conn);
  s2n_cleanup();
  return 0;
}
