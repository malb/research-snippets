import os
from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

SAGE_ROOT  = os.environ['SAGE_ROOT']
SAGE_LOCAL = SAGE_ROOT + '/local/'
SAGE_DEVEL = SAGE_ROOT + '/devel/'

setup(
    cmdclass = {'build_ext': build_ext},
    ext_modules = [Extension("metamarkov", 
                             sources = ["metamarkov.pyx"],
                             depends = ["small_present.c"],
                             extra_compile_args = ['-std=c99', '-D_XOPEN_SOURCE=600'],
                             include_dirs = ['%s/include/'%SAGE_LOCAL,
                                             '%s/sage/'%SAGE_DEVEL,
                                             '%s/sage/sage/ext'%SAGE_DEVEL, 
                                             '%s/sage/c_lib/include'%SAGE_DEVEL],
                             libraries = ['csage', 'gmp', 'mpfr']),
                   Extension("analyse", 
                             sources = ["analyse.pyx"],
                             extra_compile_args = ['-std=c99', '-D_XOPEN_SOURCE=600'],
                             include_dirs = ['%s/include/'%SAGE_LOCAL,
                                             '%s/sage/'%SAGE_DEVEL,
                                             '%s/sage/sage/ext'%SAGE_DEVEL, 
                                             '%s/sage/c_lib/include'%SAGE_DEVEL],
                             libraries = ['csage', 'gmp', 'mpfr'])]
)
