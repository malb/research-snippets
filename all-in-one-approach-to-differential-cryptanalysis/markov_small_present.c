#include "small_present.h"
#include <getopt.h>
#include <sys/types.h>
#include <unistd.h>

/************************************************
 * Main
 ************************************************/

#define DEFAULT_MODULUS 2251799813685119ULL
#define DEFAULT_DIFFERENCE 0x7ULL

void print_help() {
  printf("Markov for SMALLPRESENT-[4]\n");
  printf("\n");
  printf("Supported options:\n");
  printf("  -r   number of rounds (mandatory)\n");
  printf("  -d   input difference (default: 0x%016llu)\n",DEFAULT_DIFFERENCE);
  printf("  -m   number of trials (default: 1)\n");
  printf("  -h   this help message\n");
  printf("  -p   modulus (default: %llu\n",DEFAULT_MODULUS);
  printf("  -w   dump markov model mod p\n");
  printf("\n");
}

int main(int argc, char *argv[]) {
  diff_t d_i = 0;
  int nr = 0;
  int m = 1;
  uint64_t modulus = 0;
  int dump = 0;
  
  FILE *fh = NULL;
  char fn[255];

  char *msg = "";
  int c;
  int errflg = 0;

  int *ranks;

  /* config */
  while ((c = getopt(argc, argv, "r:d:s:hm:p:w")) != -1) {
    switch(c) {
    case 'm':
      m = atoi(optarg);
      break;
    case 'h':
      print_help();
      exit(0);
    case 'r': // number of rounds
      nr = atoi(optarg);
      break;
    case 'p': // modulus
      modulus = atol(optarg);
      break;
    case 'w': // dump?
      dump = 1;
      break;
    case 'd': //
      d_i = (diff_t)atoll(optarg);
      break;
    case 's': //message prepended to output
      msg = optarg;
      break;
    case ':':  /*without operand */
      fprintf(stderr,
              "Option -%c requires an operand\n", optopt);
      errflg++;
      break;
    case '?':
      fprintf(stderr,
              "Unrecognized option: -%c\n", optopt);
      errflg++;
    }
  }

  if(modulus == 0)
    modulus = DEFAULT_MODULUS;
  if(nr == 0) {
    print_help();
    exit(-1);
  }
  if(d_i == 0)
    d_i = DEFAULT_DIFFERENCE;

  assert(errflg == 0);
  assert(modulus < 1ULL<<(64 - (3*4+1)));

  init(4);

  diff_t *markov;
  long double *markov_log_p, *markov_log_q;
  long double *markov_log_1_minus_p, *markov_log_1_minus_q;

  unsigned int seed = (unsigned int)time(NULL) ^ (1048573 * (unsigned int)getpid());

  printf("Markov for SMALLPRESENT-[4] | nr: %2d | d_i 0x%04x | seed: 0x%08x\n",nr,d_i,seed);

  markov_log_p         = (long double*)calloc(1<<16,sizeof(long double));
  markov_log_1_minus_p = (long double*)calloc(1<<16,sizeof(long double));

  markov_log_q         = (long double*)calloc(1<<16,sizeof(long double));
  markov_log_1_minus_q = (long double*)calloc(1<<16,sizeof(long double));

  if (nr <= 5 || dump) {
    markov               = run_markov(4, nr, d_i, modulus);    

    if(dump) {
      sprintf(fn,"present4-%d-%llu.dat",nr,modulus);
      FILE *fh = fopen(fn,"w");
      fwrite(&modulus,sizeof(uint64_t), 1, fh);
      fwrite(markov,sizeof(uint64_t),1ULL<<32,fh);
      fclose(fh);
      exit(0);
    }

    markov_postprocess(4, nr, markov, markov_log_p, markov_log_1_minus_p);

    diff_t *tmp = (diff_t*)calloc(sizeof(uint64_t),(1<<16));
    printf("%zu\n",markov[10]);
    round4(tmp, markov, modulus);
    printf("%zu\n",markov[10]);
    round4_wrong_dec(tmp, markov, modulus);
    printf("%zu\n",markov[10]);
    markov_postprocess(4, nr+2, markov, markov_log_q, markov_log_1_minus_q);


    free(markov);
    free(tmp);

  } else {
    sprintf(fn,"markov_log_p-%02d-%04x.dat",nr,d_i);
    fh = fopen(fn, "r");
    if (fh == NULL) {
      printf("file '%s' needed but not found",fn);
      exit(1);
    }
    fread(markov_log_p, sizeof(long double), 1<<16, fh);
    fclose(fh);

    sprintf(fn,"markov_log_1_minus_p-%02d-%04x.dat",nr,d_i);
    fh = fopen(fn, "r");
    if (fh == NULL) {
      printf("file '%s' needed but not found",fn);
      exit(1);
    }
    fread(markov_log_1_minus_p, sizeof(long double), 1<<16, fh);
    fclose(fh);

    sprintf(fn,"markov_log_q-%02d-%04x.dat",nr,d_i);
    fh = fopen(fn, "r");
    if (fh == NULL) {
      printf("file '%s' needed but not found",fn);
      exit(1);
    }
    fread(markov_log_q, sizeof(long double), 1<<16, fh);
    fclose(fh);

    sprintf(fn,"markov_log_1_minus_q-%02d-%04x.dat",nr,d_i);
    fh = fopen(fn, "r");
    if (fh == NULL) {
      printf("file '%s' needed but not found",fn);
      exit(1);
    }
    fread(markov_log_1_minus_q, sizeof(long double), 1<<16, fh);
    fclose(fh);

  }
  ranks = (int *)calloc(sizeof(int),m);

  for (int i=0; i<m; i++) {
    ranks[i] = run_attack(4, nr+1, d_i, 
                          markov_log_p, markov_log_1_minus_p, 
                          markov_log_q, markov_log_1_minus_q, 
                          seed+i);
  };
  printf("\n");
  for (int i=0; i<m; i++) {
    printf("r: %5d\n",ranks[i]);
  }
  free(ranks);
  free(markov_log_p);
  free(markov_log_1_minus_p);
  free(markov_log_q);
  free(markov_log_1_minus_q);
  return 0;
}


