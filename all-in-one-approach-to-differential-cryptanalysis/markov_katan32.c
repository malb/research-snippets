#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <time.h>
#include <sys/time.h>
#include <math.h>
#include <assert.h>
#include <unistd.h>
#include <getopt.h>

/* wall clock */
/* see "Software Optimization for High Performance Computing" p. 135 */
double walltime( double t0 )
{
  double mic, time;
  double mega = 0.000001;
  struct timeval tp;
  static long base_sec = 0;
  static long base_usec = 0;

  (void) gettimeofday(&tp,NULL);
  if (base_sec == 0)
    {
      base_sec = tp.tv_sec;
      base_usec = tp.tv_usec;
    }

  time = (double) (tp.tv_sec - base_sec);
  mic = (double) (tp.tv_usec - base_usec);
  time = (time + mic * mega) - t0;
  return(time);
}

static inline unsigned int log2_floor(unsigned int n){
  unsigned int i;
  for(i=0;(1ULL<<i)<=n;i++){}
  return i;
}

typedef unsigned int uint;


#define Y1 18
#define Y2  7
#define Y3 12
#define Y4 10
#define Y5  8
#define Y6  3

#define X1 (12+19)
#define X2 ( 7+19)
#define X3 ( 8+19)
#define X4 ( 5+19)
#define X5 ( 3+19)



#define MASQUE 0x7fffffffU
#define MASQUE_FA ((1U<<X3)^(1U<<X4))
#define MASQUE_FB ((1U<<Y3)^(1U<<Y4)^(1U<<Y5)^(1U<<Y6))

#define MASQUE_L1 (~((1U<<19)-1))
#define MASQUE_L2 ((1U<<19)-1)

int IR[]={1,1,1,1,1,1,1,0,0,0,1,1,0,1,0,1,0,1,0,1,1,1,1,0,1,1,0,0,1,1,0,0,1,0,1,0,0,1,0,0,0,1,0,0,0,1,1,0,0,0,1,1,1,1,0,0,0,0,1,0,0,0,0,1,0,1,0,0,0,0,0,1,1,1,1,1,0,0,1,1,1,1,1,1,0,1,0,1,0,0,0,1,0,1,0,1,0,0,1,1,0,0,0,0,1,1,0,0,1,1,1,0,1,1,1,1,1,0,1,1,1,0,1,0,0,1,0,1,0,1,1,0,1,0,0,1,1,1,0,0,1,1,0,1,1,0,0,0,1,0,1,1,1,0,1,1,0,1,1,1,1,0,0,1,0,1,1,0,1,1,0,1,0,1,1,1,0,0,1,0,0,1,0,0,1,1,0,1,0,0,0,1,1,1,0,0,0,1,0,0,1,1,1,1,0,1,0,0,0,0,1,1,1,0,1,0,1,1,0,0,0,0,0,1,0,1,1,0,0,1,0,0,0,0,0,0,1,1,0,1,1,1,0,0,0,0,0,0,0,1,0,0,1,0};


void dc_to_str(uint a) {
	int i;
	for (i=0;i<19;i++) {
          if(a&(1U<<i))
            printf("1");
          else printf("0");
        }
        printf(" ");
	for (i=19;i<19+13;i++) {
          if(a&(1U<<i)) 
            printf("1");
          else printf("0");
        }
}


uint linear_part(const uint in, const int round) {
  /*
    L1(13), L2 (19), counting from right!
    
    fa = L1[X1] ^ L1[X2] ^ (L1[X3] & L1[X4]) ^ (L1[X5] & IR[i])  ^ k[2*i];
    fb = L2[Y1] ^ L2[Y2] ^ (L2[Y3] & L2[Y4]) ^ (L2[Y5] & L2[Y6]) ^ k[2*i+1];

    for(j=12;j>0;--j)
      L1[j] = L1[j-1];
    for(j=18;j>0;--j)
      L2[j] = L2[j-1];
    L1[0] = fb;
    L2[0] = fa;
  */
  
  uint result = ((in<<1)&MASQUE_L2)  | ((in&MASQUE_L1)<<1);

  result ^= ((in>>X1)&1);
  result ^= ((in>>X2)&1);
  if (IR[round]==1)
    result ^= ((in>>X5)&1);
  result ^= ((in>>Y1)&1)<<19;
  result ^= ((in>>Y2)&1)<<19;
  return result;
}

#define UPDATE(a,b,c) (a + b*c)%modulus

void encryption(int nrounds, uint di, unsigned int modulus, char *msg, char *fn) {
  uint *DI, *DO, *tmp;
  double wt;
  long i;

  DI = calloc(1ULL<<32,sizeof(uint));
  DO = calloc(1ULL<<32,sizeof(uint));
  
  /* TODO: make a choice here */
  DI[di] = 1;

  for (int nr=0; nr<nrounds; nr++) {
    wt = walltime(0.0);
    printf("%s --", msg);
    printf(" Nr: %3d,",nr);

    for (i=1;i<(1ULL<<32);i++) {
      if (DI[i]) {
        uint p = DI[i];
        uint lp = linear_part(i, nr);

        if (!(i&MASQUE_FA) && !(i&MASQUE_FB)) { //one possible
          DO[lp] = UPDATE(DO[lp], 4,p);

        } else if ((i&MASQUE_FA) && !(i&MASQUE_FB)) { //two possiblites
          DO[lp] = UPDATE(DO[lp], 2,p);

          lp = lp ^ 1;
          DO[lp] = UPDATE(DO[lp], 2,p);

        } else if (!(i&MASQUE_FA) && (i&MASQUE_FB)) { //two possiblites
          DO[lp] = UPDATE(DO[lp], 2,p);

          lp = lp^(1<<19);
          DO[lp] = UPDATE(DO[lp], 2,p);

        } else if ((i&MASQUE_FA) && (i&MASQUE_FB)) { //four possiblities
          DO[lp] = UPDATE(DO[lp], 1,p);

          lp = lp^(1<<19);
          DO[lp] = UPDATE(DO[lp], 1,p);

          lp = lp^1;
          DO[lp] = UPDATE(DO[lp], 1,p);

          lp = lp^(1<<19);
          DO[lp] = UPDATE(DO[lp], 1,p);
        }
      }
      DI[i] = 0;
    }
    printf(" Wall time: %8.2f sec",walltime(wt));
    printf("\n");
    tmp = DI;
    DI = DO;
    DO = tmp;
  }

  free(DO);

  /* for(i=0; i<64; i++) { */
  /*   printf("%2d, %10d\n",i, DI[i]); */
  /* } */

  FILE *fh = fopen(fn,"w");
  fwrite(&modulus,sizeof(unsigned int), 1, fh);
  fwrite(DI,sizeof(uint),1ULL<<32,fh);
  fclose(fh);

  free(DI);

}


int main(int argc, char *argv[]) {
  int nrounds = 0;
  uint dp = 268871808;
  unsigned int modulus = 148459189;
  char *msg = "";
  char *fn = "test.dat";
  int c;
  int errflg = 0;

  /* config */
  while ((c = getopt(argc, argv, "r:m:d:s:f:")) != -1) {
    switch(c) {
    case 'r': // number of rounds
      nrounds = atoi(optarg);
      break;
    case 'm': //
      modulus = (unsigned int)atoi(optarg);
      break;
    case 'd': //
      dp = (uint)atol(optarg);
      break;
    case 's': //message prepended to output
      msg = optarg;
      break;
    case 'f': //message prepended to output
      fn = optarg;
      break;
    case ':':  /*without operand */
      fprintf(stderr,
              "Option -%c requires an operand\n", optopt);
      errflg++;
      break;
    case '?':
      fprintf(stderr,
              "Unrecognized option: -%c\n", optopt);
      errflg++;
    }
  }
  assert(errflg == 0);
  assert(modulus <= 1<<28);

  printf("KATAN32 -- nr: %3d, d_i: 0x%08x, prime: %10d, file: %s\n",nrounds,dp,modulus,fn);
  encryption(nrounds, dp, modulus, msg, fn);
  return 0;
}
