#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <assert.h>
#include <getopt.h>

#define ONES 0xFFFFFFFFFFFFFFFFULL

#define X1_32  12
#define X2_32  7
#define X3_32  8
#define X4_32  5
#define X5_32  3
#define Y1_32  18
#define Y2_32  7
#define Y3_32  12
#define Y4_32  10
#define Y5_32  8
#define Y6_32  3

#define X1_48  18
#define X2_48  12
#define X3_48  15
#define X4_48  7
#define X5_48  6
#define Y1_48  28
#define Y2_48  19
#define Y3_48  21
#define Y4_48  13
#define Y5_48  15
#define Y6_48  6

#define X1_64  24
#define X2_64  15
#define X3_64  20
#define X4_64  11
#define X5_64  9
#define Y1_64  38
#define Y2_64  25
#define Y3_64  33
#define Y4_64  21
#define Y5_64  14
#define Y6_64  9


// IR constants, either 1 for all slices, are 0 for all slices
const uint64_t IR[254] = {
  ONES,ONES,ONES,ONES,ONES,ONES,ONES,0,0,0, // 0-9 
  ONES,ONES,0,ONES,0,ONES,0,ONES,0,ONES,
  ONES,ONES,ONES,0,ONES,ONES,0,0,ONES,ONES,
  0,0,ONES,0,ONES,0,0,ONES,0,0,
  0,ONES,0,0,0,ONES,ONES,0,0,0,
  ONES,ONES,ONES,ONES,0,0,0,0,ONES,0,
  0,0,0,ONES,0,ONES,0,0,0,0, // 60-69
  0,ONES,ONES,ONES,ONES,ONES,0,0,ONES,ONES,
  ONES,ONES,ONES,ONES,0,ONES,0,ONES,0,0,
  0,ONES,0,ONES,0,ONES,0,0,ONES,ONES,
  0,0,0,0,ONES,ONES,0,0,ONES,ONES,
  ONES,0,ONES,ONES,ONES,ONES,ONES,0,ONES,ONES,
  ONES,0,ONES,0,0,ONES,0,ONES,0,ONES, // 120-129
  ONES,0,ONES,0,0,ONES,ONES,ONES,0,0,
  ONES,ONES,0,ONES,ONES,0,0,0,ONES,0,
  ONES,ONES,ONES,0,ONES,ONES,0,ONES,ONES,ONES,
  ONES,0,0,ONES,0,ONES,ONES,0,ONES,ONES,
  0,ONES,0,ONES,ONES,ONES,0,0,ONES,0,
  0,ONES,0,0,ONES,ONES,0,ONES,0,0, // 180-189
  0,ONES,ONES,ONES,0,0,0,ONES,0,0,
  ONES,ONES,ONES,ONES,0,ONES,0,0,0,0,
  ONES,ONES,ONES,0,ONES,0,ONES,ONES,0,0,
  0,0,0,ONES,0,ONES,ONES,0,0,ONES,
  0,0,0,0,0,0,ONES,ONES,0,ONES,
  ONES,ONES,0,0,0,0,0,0,0,ONES, // 240-249
  0,0,ONES,0,
};


void katan32_keyschedule(uint64_t *k, uint64_t const key[80], int rounds) {
  int i;
  for(i=0;i<80;++i)
    k[i]=key[i];
  for(i=80;i<2*rounds;++i)
    k[i]=k[i-80] ^ k[i-61] ^ k[i-50] ^ k[i-13] ;
}

void katan32_encrypt( const uint64_t plain[32], uint64_t cipher[32], const uint64_t *k, int rounds ) {

  uint64_t L1[13], L2[19], fa, fb;
  int i,j;

  for(i=0;i<19;++i) 
    L2[i] = plain[i];
  for(i=0;i<13;++i) 
    L1[i] = plain[i+19];

  for(i=0;i<rounds;++i) {
    
    fa = L1[X1_32] ^ L1[X2_32] ^ (L1[X3_32] & L1[X4_32]) ^ (L1[X5_32] & IR[i])     ^ k[2*i];
    fb = L2[Y1_32] ^ L2[Y2_32] ^ (L2[Y3_32] & L2[Y4_32]) ^ (L2[Y5_32] & L2[Y6_32]) ^ k[2*i+1];

    for(j=12;j>0;--j)
      L1[j] = L1[j-1];
    for(j=18;j>0;--j)
      L2[j] = L2[j-1];
    L1[0] = fb;
    L2[0] = fa;
  }

  for(i=0;i<19;++i) 
    cipher[i] = L2[i];
  for(i=0;i<13;++i) 
    cipher[i+19] = L1[i];

}


void katan32_decrypt_partial( const uint64_t cipher[32], uint64_t plain[32], const uint64_t *k, int rounds, int R) {

  uint64_t L1[13], L2[19], fa, fb;
  int i,j,z;
  
  for(i=0;i<19;++i) 
    L2[i] = cipher[i];
  for(i=0;i<13;++i) 
    L1[i] = cipher[i+19];

  for(z=R-1,i=rounds-1;i>=0,z>=0;--i,--z) {

    fb = L1[0];    
    fa = L2[0];
    for(j=0;j<12;++j)
      L1[j] = L1[j+1];
    for(j=0;j<18;++j)
      L2[j] = L2[j+1];
    
    L1[X1_32] = fa ^ L1[X2_32] ^ (L1[X3_32] & L1[X4_32]) ^ (L1[X5_32] & IR[i])     ^ k[2*z];
    L2[Y1_32] = fb ^ L2[Y2_32] ^ (L2[Y3_32] & L2[Y4_32]) ^ (L2[Y5_32] & L2[Y6_32]) ^ k[2*z+1];
  }
  
  for(i=0;i<19;++i) 
    plain[i] = L2[i];
  for(i=0;i<13;++i) 
    plain[i+19] = L1[i];
  
}

void katan32_decrypt( const uint64_t cipher[32], uint64_t plain[32], const uint64_t *k, int rounds ) {

  uint64_t L1[13], L2[19], fa, fb;
  int i,j;
  
  for(i=0;i<19;++i) 
    L2[i] = cipher[i];
  for(i=0;i<13;++i) 
    L1[i] = cipher[i+19];

  for(i=rounds-1;i>=0;--i) {

    fb = L1[0];    
    fa = L2[0];
    for(j=0;j<12;++j)
      L1[j] = L1[j+1];
    for(j=0;j<18;++j)
      L2[j] = L2[j+1];
    
    L1[X1_32] = fa ^ L1[X2_32] ^ (L1[X3_32] & L1[X4_32]) ^ (L1[X5_32] & IR[i])     ^ k[2*i];
    L2[Y1_32] = fb ^ L2[Y2_32] ^ (L2[Y3_32] & L2[Y4_32]) ^ (L2[Y5_32] & L2[Y6_32]) ^ k[2*i+1];
  }
  
  for(i=0;i<19;++i) 
    plain[i] = L2[i];
  for(i=0;i<13;++i) 
    plain[i+19] = L1[i];
  
}


void test_values() {

  uint64_t key[80], k[2*254];
  uint64_t plain[64], cipher[64];
  int i;

  for(i=0;i<80;++i)   key[i]=ONES;
  for(i=0;i<32;++i)   plain[i]=0;
  katan32_keyschedule(k, key, 254);
  katan32_encrypt( plain, cipher, k, 254 );
  printf("\nkatan32_encrypt(key=11..11, plain=00.00) = ");
  //  for(i=0;i<32;++i)   printf("%llu",cipher[i]&1);
  for(i=31;i>=0;i--)   printf("%llu",cipher[i]&1);
  katan32_decrypt( cipher, plain, k, 254 );
  for(i=0;i<32;++i) {
    if ( plain[i] ) {
      printf("\ndecryption error\n");
      return;
    }
  }
  printf("\ndecryption okay\n");

  for(i=0;i<80;++i)   key[i]=0;
  for(i=0;i<32;++i)   plain[i]=ONES;
  katan32_keyschedule(k, key, 254);
  katan32_encrypt( plain, cipher, k, 254 );
  printf("\nkatan32_encrypt(key=00..00, plain=11.11) = ");
  //  for(i=0;i<32;++i)   printf("%llu",cipher[i]&1);
  for(i=31;i>=0;i--)   printf("%llu",cipher[i]&1);
  katan32_decrypt( cipher, plain, k, 254 );
  for(i=0;i<32;++i) {
    if ( plain[i] != ONES ) {
      printf("\ndecryption error\n\n");
      return;
    }
  }
  printf("\ndecryption okay\n\n");  
  
}

static inline void transp_6432(uint64_t dst[32], uint32_t src[64]) {
  int i, j;
  for (i = 0; i < 32; i++) {
    dst[i] = 0;
    for (j = 0; j < 64; j++) {
      dst[i] ^= ((src[j] >> i) & 1UL) << j;
    }
  }
}

static inline void transp_3264(uint32_t dst[64], uint64_t src[32]) {
  int i, j;
  for (i = 0; i < 64; i++) {
    dst[i] = 0;
    for (j = 0; j < 32; j++) {
      dst[i] ^= ((src[j] >> i) & 1UL) << j;
    }
  }
}

void check_transpose(uint64_t dst[32], uint32_t src[64]) {
  int i,j;
  for(i=0; i<32; i++) {
    for (j=0; j<64; j++) {
      assert( ((src[3] & (1ULL<<13))>>13) ==  ((dst[13] & (1ULL<<3))>>3) );
    }
  }
}


struct E {
  uint32_t diff;
  long double log_p;
  long double log_1_minus_p;

};


struct E lookup[16] = {
  {0x00000008U, -29.5189307, -1.87538726500102e-9 },
  {0x00aa2100U, -30.0635624, -1.28570243558865e-9 },
  {0x00a22100U, -30.0635624, -1.28570243558865e-9 },
  {0x008a2100U, -30.0635624, -1.28570243558865e-9 },
  {0x00822100U, -30.0635624, -1.28570243558865e-9 },
  {0x002a2100U, -30.0635624, -1.28570243558865e-9 },
  {0x00222100U, -30.0635624, -1.28570243558865e-9 },
  {0x000a2100U, -30.0635624, -1.28570243558865e-9 },
  {0x00022100U, -30.0635624, -1.28570243558865e-9 },
  {0x00400088U, -30.2781842, -1.10798241951992e-9 },
  {0x00000088U, -30.2781842, -1.10798241951992e-9 },
  {0x01c80180U, -30.4524095, -9.81942001922776e-10},
  {0x01880180U, -30.4524095, -9.81942001922776e-10},
  {0x01480180U, -30.4524095, -9.81942001922776e-10},
  {0x01080180U, -30.4524095, -9.81942001922776e-10},
  {0x08aa2105U, -30.5986147, -8.87306536257093e-10},  
};

long double compute_prob(int counters[16]) {
  uint64_t const n = (1ULL<<31);
  int k;
  long double log_prob = 0.0;

  long double log_q = -32.0;
  long double log_1_minus_q = -3.35903615006e-10;

  for (int i=1; i<16; i++) {
    k = counters[i];
    log_prob += (lookup[i].log_p - log_q)*k + (lookup[i].log_1_minus_p - log_1_minus_q)*(n-k);
  }
  return log_prob;
}

long double compute_counters(uint64_t const right_key[80], uint64_t const key_guess[32], int const rounds, int const r) {
  uint64_t dp = 0x1006a880;
  uint64_t k[2*rounds];
  katan32_keyschedule(k, right_key, rounds);

  int counters[16];
  for(int c=0; c<16; c++) {
    counters[c] = 0;
  }
  
  uint32_t p0[64], p1[64], c0[64], c1[64];
  uint64_t p0t[32], p1t[32], c0t[32], c1t[32];

#pragma omp parallel for private(p0,p1,c0,c1,p0t,p1t,c0t,c1t) shared(counters)
  for(long int i=0; i<(1ULL<<32); i+=64) {
    if (i&(1ULL)<<7)
      continue;

    for(uint64_t j=0; j<64; j++) {
      p0[j] = i ^ j;
      p1[j] = i ^ j ^ dp;
    } 
    transp_6432(p0t,p0);
    transp_6432(p1t,p1);

    katan32_encrypt(p0t, c0t, k, rounds); 
    katan32_encrypt(p1t, c1t, k, rounds); 

    katan32_decrypt_partial(c0t, p0t, key_guess, rounds, r);
    katan32_decrypt_partial(c1t, p1t, key_guess, rounds, r);

    transp_3264(p0,p0t);
    transp_3264(p1,p1t);

    for(int j=0; j<64; j++) {
      uint32_t diff = (p0[j] ^ p1[j]);
      for(int c=0; c<16; c++) {
        if (diff == lookup[c].diff) {
          counters[c]++;
        }
      }
    }

    if(i%(1<<28) == 0) {
      printf("0x%08x: ",i);
      for(int c=0; c<16; c++) {
        printf("%d ",counters[c]);
      }
      printf("\n");
    }
  }
  return compute_prob(counters);
};

void randomise(uint64_t *k,int length) {
  for(int i = 0; i<length; i++) {
    if(random() < RAND_MAX/2) {
      k[i] = 0;
    } else {
      k[i] = ONES;
    }    
  }
}

#define DEFAULT_R 16
#define DEFAULT_S 17
#define DEFAULT_N 0

void print_help() {
  printf("Markov for KATAN32\n");
  printf("\n");
  printf("Supported options:\n");
  printf("  -r   number of rounds for backward guessing (default: %d)\n",DEFAULT_R);
  printf("  -n   number of random key guess samples (default: %d)\n",DEFAULT_N);
  printf("  -s   random seed (default: %d)\n",DEFAULT_S);
  printf("  -h   this help message\n");
  printf("\n");
}



int main (int argc, char **argv) {
  int r = DEFAULT_R;
  int s = DEFAULT_S;
  int n = DEFAULT_N;

  int const nr = 71;
  int c;
  int errflg = 0;

  uint64_t right_key[80];
  uint64_t k[2*254];
  uint64_t key_guess[64];

  /* config */
  while ((c = getopt(argc, argv, "r:n:s:h")) != -1) {
    switch(c) {
    case 'r':
      r = atoi(optarg);
      assert(r>=0);
      break;
    case 'n': // number of rounds
      n = atoi(optarg);
      assert(n>=0);
      break;
    case 's':
      s = atoi(optarg);      
      break;
    case 'h':
      print_help();
      exit(0);
    case ':':  /*without operand */
      fprintf(stderr,
              "Option -%c requires an operand\n", optopt);
      errflg++;
      break;
    case '?':
      fprintf(stderr,
              "Unrecognized option: -%c\n", optopt);
      errflg++;
    }
  }


  srandom(s);
  randomise(right_key,80);
  printf("seed: %d, n: %d, r: %d, right key: ",s,n,r);
  for(int i=0; i<80; i++) {
    if (right_key[i])
      printf("1");
    else
      printf("0");
  }
  printf("\n");

  katan32_keyschedule(k,right_key, 254);
  long double right_key_prob = compute_counters(right_key, k+(2*nr), nr+r, r);
  printf("right key prob: %llf\n",right_key_prob);

  int rank = 0;


  for(int i=0; i<n; i++) {
    randomise(key_guess, 2*r);
    long double prob = compute_counters(right_key, key_guess, nr+r, r);
    if (prob >= right_key_prob)
      rank++;
    printf("%5d: prob: %llf, right key rank: %5d\n",i,prob,rank);
  }
}



